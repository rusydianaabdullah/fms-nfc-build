@echo off

cd /d "C:\fms-nfc-build"

npx pm2 start pm2.json
npx pm2 save

echo "Success running PM2"

pause