import os
import sys
import time
from schedulers import post_cctv, post_do, post_tare, get_do_temp, get_others, post_logs
from ws import websocket_service

if __name__ == "__main__":
    sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), "src")))
    # ws
    websocket_service.run()
    # scheduler
    minute = 1
    sleeptime = 60 * minute
    halfday = 60 * 60 * 12
    i = 1
    while True:
        print('running jobs count: ', i)
        if i%halfday == 0:
            try:
                print('--- getting others ---')
                get_others.run()
            except Exception as e:
                print(f'--- error {str(e)} ---')
                pass
        elif i%2 == 0:
            try:
                print('--- post do ---')
                post_do.run()
                print('--- getting do temp ---')
                get_do_temp.run()
            except Exception as e:
                print(f'--- error {str(e)} ---')
                pass
        else:
            try:
                print('--- post logs ---')
                post_logs.run()
                print('--- post tare ---')
                post_tare.run()
                print('--- post cctv ---')
                post_cctv.run()
            except Exception as e:
                print(f'--- error {str(e)} ---')
                pass

        i = i + 1
        time.sleep(sleeptime)