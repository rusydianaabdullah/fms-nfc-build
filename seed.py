import sys
import os


if len(sys.argv) < 2:
    exit()


def init():
    import json
    from utils.hash import Hash
    import db.tables
    from db.database import Session
    from app.models import Permission
    from app.models import Role
    from app.models import User
    db = Session()

    ####### PERMISSIONS #######
    p = open('./seeders/permissions.json')
    mod = {"browse": True, "read": True, "create": True, "update": True, "delete": True, "restore": True}
    permissions = json.load(p)
    perms = []
    for v in permissions:
        try:
            app = v.get('app')
            name = v.get('name')
            if (db.query(Permission.name).filter(Permission.app == app).filter(Permission.name == name).first() == None):
                db.add(Permission( app=app, name=name ))
                db.commit()
                print(f'commit {name}')
            else: print(f'permission exist: {name}')
            perms.append({ "app": app, "name": name, "detail": mod })
        except:
            print('exist')

    try:
        if (db.query(Role).filter(Role.id == 1).first()):
            db.query(Role).filter(Role.id == 1).update({'app':'user', 'name':"superuser", 'permissions':perms, 'deleted_at': None, 'deleted_by': None})
            db.commit()
            print('role permission updated')
        else:
            role_su = Role(app='user', name="superuser", permissions=perms)
            db.add(role_su)
            db.commit()
            print('role permission added')
    except:
        print('role permission fiailed')

    ####### USER #######
    try:
        check = db.query(User.id).filter(User.id == 1).first()
        if (check):
            db.query(User).filter(User.id == 1).update({
                "name":"superuser",
                "username":"superuser",
                "email":"superuser@mail.com",
                "password":Hash.bcrypt("password123"),
                "role_id":1,
            })
            db.commit()
        else:
            admin = User(
                name="superuser",
                username="superuser",
                email="superuser@mail.com",
                password=Hash.bcrypt("password123"),
                role_id=1,
            )
            db.add(admin)
            db.commit()
            print('superuser added')

        check2 = db.query(User.id).filter(User.id == 2).first()
        if (check2):
            db.query(User).filter(User.id == 2).update({
                "name":"admin",
                "username":"admin",
                "email":"admin@mail.com",
                "password":Hash.bcrypt("root"),
                "role_id":1,
            })
            db.commit()
        else:
            admin = User(
                name="admin",
                username="admin",
                email="admin@mail.com",
                password=Hash.bcrypt("root"),
                role_id=1,
            )
            db.add(admin)
            db.commit()
            print('superuser added')
    except:
        print('superuser update failed')

if __name__ == "__main__":
    sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), "src")))

    if sys.argv[1] == "init":
        init()
    elif sys.argv[1] == "drop-all":
        from db import tables

        tables.drop_all()
