import os
import cv2
import base64
import time
from datetime import datetime
from dotenv import load_dotenv, dotenv_values
load_dotenv()
config  = dotenv_values(".env")

parent_folder = 'src/cctv'
try:    rtsp_url = config['RTSP_URL']
except: rtsp_url = ""

if rtsp_url:
  if not os.path.isdir(parent_folder): os.mkdir(parent_folder)
  while True:
    cap = cv2.VideoCapture(rtsp_url)
    ret,frame = cap.read()
    if  not  ret:
      break
    ####### JPG AS BASE64 #######
    retval, buffer = cv2.imencode('.jpg', frame)
    jpg_as_text = base64.b64encode(buffer)
    cctv = jpg_as_text.decode('utf-8')
    f = open(f'{parent_folder}/capture.txt', "w")
    f.write(cctv)
    f.close()
    x = datetime.now()
    now = x.strftime("%Y-%m-%d %H:%M:%S")
    f = open(f'{parent_folder}/last_update.txt', "w")
    f.write(now)
    f.close()
    print('cctv gen base64', now)
    cap.release()
    time.sleep(5)
