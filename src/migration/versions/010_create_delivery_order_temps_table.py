"""Create Delivery Order Temps Table

Revision ID: 010
Revises: 009
Create Date: 2024-04-24 14:52:48.005507

"""
from typing import Sequence, Union

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision: str = '010'
down_revision: Union[str, None] = '009'
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    op.create_table(
        'delivery_order_temps',
        sa.Column("id_", sa.BigInteger, primary_key=True, index=True),
        sa.Column("id", sa.BigInteger, unique=True, nullable=True),
        sa.Column('flag_sync', sa.Boolean, default=False),
        sa.Column('type', sa.Integer, nullable=True),
        sa.Column('code', sa.String, nullable=True),
        sa.Column('truck_code', sa.String, nullable=True),
        sa.Column('owner_code', sa.String, nullable=True),
        sa.Column('pit_code', sa.String, nullable=True),
        sa.Column('seam_code', sa.String, nullable=True),
        sa.Column('vendor_code', sa.String, nullable=True),
        sa.Column('driver', sa.String, nullable=True),

        sa.Column('datetime', sa.BigInteger, nullable=True),
        sa.Column('date', sa.Date, nullable=True),
        sa.Column('shift', sa.String, nullable=True),
        sa.Column('rit', sa.Integer, nullable=True),
        sa.Column('user', sa.String, nullable=True),
        sa.Column('wc', sa.String, nullable=True),
        sa.Column('loc', sa.String, nullable=True),
        sa.Column('bruto', sa.Double, nullable=True),
        sa.Column('net', sa.Double, nullable=True),
        sa.Column('rfid', sa.String, nullable=True),
        sa.Column('abnormal', sa.Text, nullable=True),

        sa.Column('created_by', sa.String, nullable=True),
        sa.Column('updated_by', sa.String, nullable=True),
        sa.Column('deleted_by', sa.String, nullable=True),
        sa.Column('created_at', sa.DateTime(timezone=True), nullable=True),
        sa.Column('updated_at', sa.DateTime(timezone=True), nullable=True),
        sa.Column('deleted_at', sa.DateTime(timezone=True), nullable=True),
    )
    pass


def downgrade() -> None:
    op.drop_table('delivery_order_temps')
    pass
