"""add reprint do

Revision ID: 006
Revises: 005
Create Date: 2024-04-22 17:27:50.874521

"""
from typing import Sequence, Union

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision: str = '006'
down_revision: Union[str, None] = '005'
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    op.add_column('delivery_orders', sa.Column('reprint', sa.Integer, default=0))    
    pass


def downgrade() -> None:
    op.drop_column('delivery_orders', column_name="reprint")
    pass