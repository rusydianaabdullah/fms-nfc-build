"""add tare max day to nfc cfg

Revision ID: 022
Revises: 021
Create Date: 2024-12-09 11:32:28.310519

"""
from typing import Sequence, Union

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision: str = '022'
down_revision: Union[str, None] = '021'
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    op.add_column('nfc_config', sa.Column('tare_max_day', sa.Integer, server_default='5', nullable=False))
    pass


def downgrade() -> None:
    op.drop_column('nfc_config', column_name="tare_max_day")
    pass
