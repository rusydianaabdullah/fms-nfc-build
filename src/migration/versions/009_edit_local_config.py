"""009_edit_local_config

Revision ID: 009
Create Date: 2024-04-24 09:25:16.892591

"""
from typing import Sequence, Union

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision: str = '009'
down_revision: Union[str, None] = '008'
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    op.drop_table('local_config')
    op.create_table(
        'local_config',
        sa.Column('id', sa.BigInteger, primary_key=True, index=True),
        sa.Column('type', sa.String, nullable=True),
        sa.Column('loc_code', sa.String, nullable=True),
        sa.Column('wc_code', sa.String, nullable=True),
        sa.Column('wc_code_1', sa.String, nullable=True),
        sa.Column('pit_code', sa.String, nullable=True),
        sa.Column('hauling', sa.String, nullable=True),
        sa.Column('owner_code', sa.String, nullable=True),
        sa.Column('destination', sa.String, nullable=True),
        sa.Column('prefix', sa.String, nullable=True),

        sa.Column('created_at', sa.DateTime(timezone=True), nullable=True),
        sa.Column('updated_at', sa.DateTime(timezone=True), nullable=True),
        sa.Column('deleted_at', sa.DateTime(timezone=True), nullable=True),
    )


def downgrade() -> None:
    op.drop_table('local_config')
