"""014_add_id_server_to_do

Revision ID: 014
Revises: 013
Create Date: 2024-04-28 01:22:14.973390

"""
from typing import Sequence, Union

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision: str = '014'
down_revision: Union[str, None] = '013'
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    op.add_column('delivery_orders', sa.Column('id_server', sa.BigInteger, nullable=True))
    pass


def downgrade() -> None:
    op.drop_column('delivery_orders', column_name="id_server")
    pass