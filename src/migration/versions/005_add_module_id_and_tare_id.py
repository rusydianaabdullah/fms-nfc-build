"""add module id and tare id

Revision ID: 005
Revises: 004
Create Date: 2024-04-22 17:27:07.252701

"""
from typing import Sequence, Union

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision: str = '005'
down_revision: Union[str, None] = '004'
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    op.drop_table('cctv')
    op.create_table(
        "cctv",
        sa.Column("id", sa.BigInteger, primary_key=True, index=True),
        sa.Column('flag_sync', sa.Boolean, default=False),
        sa.Column("module", sa.String, nullable=True, default='transaction/delivery-orders'),
        sa.Column("name", sa.String, nullable=True),
        sa.Column("base64", sa.Text, nullable=True),
        sa.Column("filename", sa.String, nullable=True),
        sa.Column("module_id", sa.BigInteger, nullable=True),
        sa.Column("tare_id", sa.BigInteger, nullable=True),
        sa.Column("description", sa.String, nullable=True),
        sa.Column("created_at", sa.DateTime(timezone=True), nullable=True),
        sa.Column("updated_at", sa.DateTime(timezone=True), nullable=True),
        sa.Column('deleted_at', sa.DateTime(timezone=True), nullable=True),
        # sa.ForeignKeyConstraint(["role_id"], ["auth.roles.id"], name="role_user_id_fkey"),
    )
    pass


def downgrade() -> None:
    op.drop_column('cctv', column_name="module_id")
    op.drop_column('cctv', column_name="tare_id")
    pass