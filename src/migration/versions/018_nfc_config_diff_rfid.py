"""018_nfc_config_diff_rfid

Revision ID: 018
Revises: 017
Create Date: 2024-06-05 02:36:39.362527

"""
from typing import Sequence, Union

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision: str = '018'
down_revision: Union[str, None] = '017'
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    op.add_column('nfc_config', sa.Column('diff_rfid', sa.Boolean, default=False))
    pass


def downgrade() -> None:
    op.drop_column('nfc_config', column_name="diff_rfid")
    pass