"""011_truck_tare_update

Revision ID: 011
Create Date: 2024-04-25 09:25:16.892591

"""
from typing import Sequence, Union

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision: str = '011'
down_revision: Union[str, None] = '010'
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    op.drop_table('truck_tares')
    op.create_table(
        "truck_tares",
        sa.Column("id_", sa.BigInteger, primary_key=True, index=True),
        sa.Column("id", sa.BigInteger, unique=False, nullable=True),
        sa.Column('flag_sync', sa.Boolean, default=False),
        sa.Column("truck_id", sa.BigInteger, nullable=True),
        sa.Column("wc_id", sa.BigInteger, nullable=True),
        sa.Column("truck_code", sa.String, nullable=True),
        sa.Column("wc_code", sa.String, nullable=True),
        sa.Column("value", sa.Double, nullable=True),
        sa.Column("date", sa.BigInteger, nullable=True),
        sa.Column("created_at", sa.DateTime(timezone=True), nullable=True),
        sa.Column("updated_at", sa.DateTime(timezone=True), nullable=True),
        sa.Column('deleted_at', sa.DateTime(timezone=True), nullable=True),
        # sa.ForeignKeyConstraint(["role_id"], ["auth.roles.id"], name="role_user_id_fkey"),
    )


def downgrade() -> None:
    op.drop_table('truck_tares')
