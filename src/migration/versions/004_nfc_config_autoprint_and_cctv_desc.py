"""004_local_config_and_cctv_desc

Revision ID: 004
Create Date: 2024-04-21 09:25:16.892591

"""
from typing import Sequence, Union

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision: str = '004'
down_revision: Union[str, None] = '003'
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    op.drop_table('nfc_config')
    op.create_table(
        'nfc_config',
        sa.Column('id', sa.BigInteger, primary_key=True, index=True),
        sa.Column('type', sa.String, nullable=True, default='do'),
        sa.Column('truck_code', sa.String, nullable=True),
        sa.Column('nik', sa.String, nullable=True),
        sa.Column('min_weight', sa.Integer, nullable=True),
        sa.Column('auto_print', sa.Boolean, default=False),

        sa.Column('created_at', sa.DateTime(timezone=True), nullable=True),
        sa.Column('updated_at', sa.DateTime(timezone=True), nullable=True),
        sa.Column('deleted_at', sa.DateTime(timezone=True), nullable=True),
        # schema='auth'
    )

    op.create_table(
        'local_config',
        sa.Column('id', sa.BigInteger, primary_key=True, index=True),
        sa.Column('type', sa.String, nullable=True),
        sa.Column('loc_code', sa.String, nullable=True),
        sa.Column('loc_type', sa.String, nullable=True),
        sa.Column('wc_code', sa.String, nullable=True),
        sa.Column('wc_code1', sa.String, nullable=True),
        sa.Column('pit_code', sa.String, nullable=True),
        sa.Column('hauling', sa.String, nullable=True),
        sa.Column('owner_code', sa.String, nullable=True),
        sa.Column('prefix', sa.String, nullable=True),

        sa.Column('created_at', sa.DateTime(timezone=True), nullable=True),
        sa.Column('updated_at', sa.DateTime(timezone=True), nullable=True),
        sa.Column('deleted_at', sa.DateTime(timezone=True), nullable=True),
    )


def downgrade() -> None:
    op.drop_table('nfc_config')
    op.drop_table('local_config')
