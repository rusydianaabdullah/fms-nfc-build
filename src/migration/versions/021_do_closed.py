"""021_do_closed

Revision ID: 021
Revises: 020
Create Date: 2024-11-05 20:22:14.973390

"""
from typing import Sequence, Union

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision: str = '021'
down_revision: Union[str, None] = '020'
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    op.create_table(
        'do_closed',
        sa.Column('id', sa.BigInteger, primary_key=True, index=True),
        sa.Column('date', sa.Date, default=False),
        sa.Column('code', sa.String, nullable=True)
    )


def downgrade() -> None:
    op.drop_table('do_closed')