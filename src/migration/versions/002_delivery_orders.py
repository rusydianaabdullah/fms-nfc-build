"""delivery_orders

Revision ID: 002
Create Date: 2023-12-07 09:25:16.892591

"""
from typing import Sequence, Union

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision: str = '002'
down_revision: Union[str, None] = '001'
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    # op.drop_table('delivery_orders')
    # op.drop_table('cctv')
    # op.drop_table('weighing_centers')
    # op.drop_table('trucks')
    # op.drop_table('truck_tares')

    op.create_table(
        'delivery_orders',
        sa.Column('id', sa.BigInteger, primary_key=True, index=True),
        sa.Column('flag_sync', sa.Boolean, default=False),
        sa.Column('type', sa.Integer, nullable=True),
        sa.Column('code', sa.String, nullable=True),
        sa.Column('truck_code', sa.String, nullable=True),
        sa.Column('owner_code', sa.String, nullable=True),
        sa.Column('pit_code', sa.String, nullable=True),
        sa.Column('seam_code', sa.String, nullable=True),
        sa.Column('vendor_code', sa.String, nullable=True),
        sa.Column('driver', sa.String, nullable=True),

        sa.Column('datetime_0', sa.BigInteger, nullable=True),
        sa.Column('date_0', sa.Date, nullable=True),
        sa.Column('shift_0', sa.String, nullable=True),
        sa.Column('rit_0', sa.Integer, nullable=True),
        sa.Column('user_0', sa.String, nullable=True),
        sa.Column('wc_0', sa.String, nullable=True),
        sa.Column('loc_0', sa.String, nullable=True),
        sa.Column('bruto_0', sa.Double, nullable=True),
        sa.Column('net_0', sa.Double, nullable=True),
        sa.Column('rfid_0', sa.String, nullable=True),
        sa.Column('nfc_0', sa.String, nullable=True),
        sa.Column('abnormal_0', sa.Text, nullable=True),

        sa.Column('datetime_1', sa.BigInteger, nullable=True),
        sa.Column('date_1', sa.Date, nullable=True),
        sa.Column('shift_1', sa.String, nullable=True),
        sa.Column('rit_1', sa.Integer, nullable=True),
        sa.Column('user_1', sa.String, nullable=True),
        sa.Column('wc_1', sa.String, nullable=True),
        sa.Column('loc_1', sa.String, nullable=True),
        sa.Column('bruto_1', sa.Double, nullable=True),
        sa.Column('net_1', sa.Double, nullable=True),
        sa.Column('rfid_1', sa.String, nullable=True),
        sa.Column('nfc_1', sa.String, nullable=True),
        sa.Column('abnormal_1', sa.Text, nullable=True),

        sa.Column('created_by', sa.String, nullable=True),
        sa.Column('updated_by', sa.String, nullable=True),
        sa.Column('deleted_by', sa.String, nullable=True),
        sa.Column('created_at', sa.DateTime(timezone=True), nullable=True),
        sa.Column('updated_at', sa.DateTime(timezone=True), nullable=True),
        sa.Column('deleted_at', sa.DateTime(timezone=True), nullable=True),
    )

    op.create_table(
        "cctv",
        sa.Column("id", sa.BigInteger, primary_key=True, index=True),
        sa.Column('flag_sync', sa.Boolean, default=False),
        sa.Column("module", sa.String, nullable=True, default='transaction/delivery-orders'),
        sa.Column("name", sa.String, nullable=True),
        sa.Column("base64", sa.Text, nullable=True),
        sa.Column("filename", sa.String, nullable=True),
        sa.Column("created_at", sa.DateTime(timezone=True), nullable=True),
        sa.Column("updated_at", sa.DateTime(timezone=True), nullable=True),
        sa.Column('deleted_at', sa.DateTime(timezone=True), nullable=True),
        # sa.ForeignKeyConstraint(["role_id"], ["auth.roles.id"], name="role_user_id_fkey"),
    )

    op.create_table(
        "weighing_centers",
        sa.Column("id_", sa.BigInteger, primary_key=True, index=True),
        sa.Column("id", sa.BigInteger, unique=True),
        sa.Column('flag_sync', sa.Boolean, default=False),
        sa.Column('code', sa.String, default=False),
        sa.Column('prefix', sa.String, default=False),
        sa.Column("created_at", sa.DateTime(timezone=True), nullable=True),
        sa.Column("updated_at", sa.DateTime(timezone=True), nullable=True),
        sa.Column('deleted_at', sa.DateTime(timezone=True), nullable=True),
        # sa.ForeignKeyConstraint(["role_id"], ["auth.roles.id"], name="role_user_id_fkey"),
    )

    op.create_table(
        "trucks",
        sa.Column("id_", sa.BigInteger, primary_key=True, index=True),
        sa.Column("id", sa.BigInteger, unique=True),
        sa.Column('flag_sync', sa.Boolean, default=False),
        sa.Column("code", sa.String, nullable=True),
        sa.Column("code_1", sa.String, nullable=True),
        sa.Column("name", sa.String, nullable=True),
        sa.Column("type", sa.String, nullable=True),
        sa.Column("brand", sa.String, nullable=True),
        sa.Column("company_code", sa.String, nullable=True),
        sa.Column("gps_code", sa.String, nullable=True),
        sa.Column("rfid_code", sa.String, nullable=True),
        sa.Column("status", sa.String, nullable=True),
        sa.Column("created_at", sa.DateTime(timezone=True), nullable=True),
        sa.Column("updated_at", sa.DateTime(timezone=True), nullable=True),
        sa.Column('deleted_at', sa.DateTime(timezone=True), nullable=True),
        # sa.ForeignKeyConstraint(["role_id"], ["auth.roles.id"], name="role_user_id_fkey"),
    )

    op.create_table(
        "truck_tares",
        sa.Column("id_", sa.BigInteger, primary_key=True, index=True),
        sa.Column("id", sa.BigInteger, unique=True, nullable=True),
        sa.Column('flag_sync', sa.Boolean, default=False),
        sa.Column("truck_id", sa.BigInteger, nullable=True),
        sa.Column("wc_id", sa.BigInteger, nullable=True),
        sa.Column("truck_code", sa.String, nullable=True),
        sa.Column("wc_code", sa.String, nullable=True),
        sa.Column("value", sa.Double, nullable=True),
        sa.Column("date", sa.BigInteger, nullable=True),
        sa.Column("created_at", sa.DateTime(timezone=True), nullable=True),
        sa.Column("updated_at", sa.DateTime(timezone=True), nullable=True),
        sa.Column('deleted_at', sa.DateTime(timezone=True), nullable=True),
        # sa.ForeignKeyConstraint(["role_id"], ["auth.roles.id"], name="role_user_id_fkey"),
    )


def downgrade() -> None:
    op.drop_table('delivery_orders')
    op.drop_table('cctv')
    op.drop_table('weighing_centers')
    op.drop_table('trucks')
    op.drop_table('truck_tares')
