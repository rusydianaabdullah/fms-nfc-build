"""Add min truck tare column to nfc config

Revision ID: 020
Revises: 019
Create Date: 2024-09-06 14:32:50.152228

"""
from typing import Sequence, Union

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision: str = '020'
down_revision: Union[str, None] = '019'
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:    
    op.add_column('nfc_config', sa.Column('min_truck_tare', sa.Integer, default=0, nullable=True, comment='truk weight in KG'))
    pass


def downgrade() -> None:
    op.drop_column('nfc_config', column_name="min_truck_tare")
    pass
