"""auth-role-permission

Revision ID: 001
Create Date: 2023-11-23 09:25:16.892591

"""
from typing import Sequence, Union

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision: str = '001'
down_revision: Union[str, None] = None
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    # op.drop_table('users', schema='auth')

    op.create_table(
        'permissions',
        sa.Column('id', sa.BigInteger, primary_key=True, index=True),
        sa.Column('app', sa.String, nullable=True),
        sa.Column('name', sa.String, unique=True, nullable=False),
        sa.Column('created_by', sa.String, nullable=True),
        sa.Column('updated_by', sa.String, nullable=True),
        sa.Column('deleted_by', sa.String, nullable=True),
        sa.Column('created_at', sa.DateTime(timezone=True), nullable=True),
        sa.Column('updated_at', sa.DateTime(timezone=True), nullable=True),
        sa.Column('deleted_at', sa.DateTime(timezone=True), nullable=True),
        # schema='auth'
    )

    op.create_table(
        'roles',
        sa.Column('id', sa.BigInteger, primary_key=True, index=True),
        sa.Column('app', sa.String, nullable=True),
        sa.Column('name', sa.String, unique=True, nullable=False),
        sa.Column('permissions', sa.JSON, nullable=True),
        sa.Column('created_by', sa.String, nullable=True),
        sa.Column('updated_by', sa.String, nullable=True),
        sa.Column('deleted_by', sa.String, nullable=True),
        sa.Column('created_at', sa.DateTime(timezone=True), nullable=True),
        sa.Column('updated_at', sa.DateTime(timezone=True), nullable=True),
        sa.Column('deleted_at', sa.DateTime(timezone=True), nullable=True),
        # schema='auth'
    )

    op.create_table(
        "master_files",
        sa.Column("id", sa.BigInteger, primary_key=True, index=True),
        sa.Column("app", sa.String, nullable=True),
        sa.Column("name", sa.String, nullable=True),
        sa.Column("description", sa.Text, nullable=True),
        sa.Column("filename", sa.String, nullable=True),
        sa.Column("path", sa.String, nullable=True),
        sa.Column("module", sa.String, nullable=True),
        sa.Column("reference_code", sa.String, nullable=True),
        sa.Column("reference_id", sa.BigInteger, nullable=True),

        sa.Column("created_by", sa.String, nullable=True),
        sa.Column("updated_by", sa.String, nullable=True),
        sa.Column("deleted_by", sa.String, nullable=True),
        sa.Column("created_at", sa.DateTime(timezone=True), nullable=True),
        sa.Column("updated_at", sa.DateTime(timezone=True), nullable=True),
        sa.Column("deleted_at", sa.DateTime(timezone=True), nullable=True),
        # sa.ForeignKeyConstraint(["role_id"], ["roles.id"], name="role_user_id_fkey"),
        # schema="auth"
    )

    op.create_table(
        'users',
        sa.Column('id', sa.BigInteger, primary_key=True, index=True),
        sa.Column('_id', sa.String, unique=True),
        sa.Column('username', sa.String, unique=True, nullable=False),
        sa.Column('email', sa.String, unique=True, nullable=False),
        sa.Column('password', sa.String, nullable=False),
        sa.Column('phone', sa.String, nullable=True),
        sa.Column('name', sa.String, nullable=False),

        sa.Column('role_id', sa.BigInteger, nullable=True),
        sa.Column('role_name', sa.String, nullable=True),

        sa.Column('created_by', sa.String, nullable=True),
        sa.Column('updated_by', sa.String, nullable=True),
        sa.Column('deleted_by', sa.String, nullable=True),
        sa.Column('created_at', sa.DateTime(timezone=True), nullable=True),
        sa.Column('updated_at', sa.DateTime(timezone=True), nullable=True),
        sa.Column('deleted_at', sa.DateTime(timezone=True), nullable=True),
        sa.ForeignKeyConstraint(['role_id'], ['roles.id'], name='role_user_id_fkey')
        # schema='auth'
    )

    op.create_table(
        "notifications",
        sa.Column("id", sa.BigInteger, primary_key=True, index=True),
        sa.Column("username", sa.String, nullable=True),
        sa.Column("user_id", sa.BigInteger, nullable=True),
        sa.Column("is_read", sa.Boolean, default=False),
        sa.Column("app", sa.String, nullable=True),
        sa.Column("title", sa.String, nullable=True),
        sa.Column("description", sa.Text, nullable=True),
        sa.Column("path", sa.String, nullable=True),
        sa.Column("type", sa.String, nullable=True),
        sa.Column("color", sa.String, nullable=True),
        sa.Column("icon", sa.String, nullable=True),

        sa.Column("created_by", sa.String, nullable=True),
        sa.Column("updated_by", sa.String, nullable=True),
        sa.Column("deleted_by", sa.String, nullable=True),
        sa.Column("created_at", sa.DateTime(timezone=True), nullable=True),
        sa.Column("updated_at", sa.DateTime(timezone=True), nullable=True),
        sa.Column("deleted_at", sa.DateTime(timezone=True), nullable=True),
        sa.ForeignKeyConstraint(["user_id"], ["users.id"], name="auth_notifications_user_id_fkey"),
        # schema="auth"
    )


def downgrade() -> None:
    op.drop_table('users', schema='auth')
    op.drop_table('roles', schema='auth')
    op.drop_table('permissions', schema='auth')
