"""add truck name to delivery orders temps

Revision ID: 013
Revises: 012
Create Date: 2024-04-27 16:54:14.973390

"""
from typing import Sequence, Union

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision: str = '013'
down_revision: Union[str, None] = '012'
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    op.add_column('delivery_order_temps', sa.Column('no_lambung', sa.String, default=0))
    pass


def downgrade() -> None:
    op.drop_column('delivery_order_temps', column_name="no_lambung")
    pass