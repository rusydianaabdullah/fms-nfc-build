"""016_update_do_temp_nfc_cfg

Revision ID: 016
Revises: 015
Create Date: 2024-05-21 20:22:14.973390

"""
from typing import Sequence, Union

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision: str = '016'
down_revision: Union[str, None] = '015'
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    op.add_column('delivery_order_temps', sa.Column('loc_1', sa.String, nullable=True))
    op.add_column('nfc_config', sa.Column('lastlogin_user', sa.String, nullable=True))
    pass


def downgrade() -> None:
    op.drop_column('delivery_order_temps', column_name="loc_1")
    op.drop_column('nfc_config', column_name="lastlogin_user")
    pass