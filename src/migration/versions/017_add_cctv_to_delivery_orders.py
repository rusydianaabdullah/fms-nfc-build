"""Add cctv to delivery orders

Revision ID: 017
Revises: 016
Create Date: 2024-06-05 02:36:39.362527

"""
from typing import Sequence, Union

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision: str = '017'
down_revision: Union[str, None] = '016'
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    op.add_column('delivery_orders', sa.Column('cctv_0', sa.Text, nullable=True))
    op.add_column('delivery_orders', sa.Column('cctv_1', sa.Text, nullable=True))
    pass


def downgrade() -> None:
    op.drop_column('delivery_orders', column_name="cctv_0")
    op.drop_column('delivery_orders', column_name="cctv_1")
    pass