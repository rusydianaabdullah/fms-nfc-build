"""015_error_logs

Revision ID: 015
Revises: 014
Create Date: 2024-05-08 20:22:14.973390

"""
from typing import Sequence, Union

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision: str = '015'
down_revision: Union[str, None] = '014'
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    op.create_table(
        'error_logs',
        sa.Column('id', sa.BigInteger, primary_key=True, index=True),
        sa.Column('flag_sync', sa.Boolean, default=False),
        sa.Column('location', sa.String, nullable=True),
        sa.Column('module', sa.String, nullable=True),
        sa.Column('module_id', sa.BigInteger, nullable=True),
        sa.Column('name', sa.String, nullable=True),
        sa.Column('remark', sa.Text, nullable=True),
        sa.Column('created_at', sa.DateTime(timezone=True), nullable=True),
        sa.Column('updated_at', sa.DateTime(timezone=True), nullable=True),
        sa.Column('deleted_at', sa.DateTime(timezone=True), nullable=True),
    )


def downgrade() -> None:
    op.drop_table('error_logs')