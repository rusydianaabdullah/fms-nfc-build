"""add truck name to delivery orders

Revision ID: 012
Revises: 011
Create Date: 2024-04-27 16:06:11.515264

"""
from typing import Sequence, Union

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision: str = '012'
down_revision: Union[str, None] = '011'
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    op.add_column('delivery_orders', sa.Column('no_lambung', sa.String, default=0))
    pass


def downgrade() -> None:
    op.drop_column('delivery_orders', column_name="no_lambung")
    pass