"""Add thermal print to nfc config

Revision ID: 019
Revises: 018
Create Date: 2024-09-03 14:56:36.418767

"""
from typing import Sequence, Union

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision: str = '019'
down_revision: Union[str, None] = '018'
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:    
    op.add_column('nfc_config', sa.Column('thermal_print', sa.Boolean, default=False))
    pass


def downgrade() -> None:
    op.drop_column('nfc_config', column_name="thermal_print")
    pass
