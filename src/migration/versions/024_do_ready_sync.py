"""do_ready_sync

Revision ID: 024
Revises: 026
Create Date: 2025-01-06 15:20:08.760091

"""
from typing import Sequence, Union

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision: str = '024'
down_revision: Union[str, None] = '023'
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    op.add_column('delivery_orders', sa.Column('ready_sync', sa.Boolean, default=False))
    pass


def downgrade() -> None:
    op.drop_column('delivery_orders', column_name="ready_sync")
    pass
