"""create locations table

Revision ID: 008
Revises: 007
Create Date: 2024-04-22 23:52:28.579501

"""
from typing import Sequence, Union

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision: str = '008'
down_revision: Union[str, None] = '007'
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    op.create_table(
        'locations',
        sa.Column('id', sa.BigInteger, primary_key=True, index=True),
        sa.Column('code', sa.String, nullable=True),
        sa.Column('name', sa.String, nullable=True),
        sa.Column('company_code', sa.String, nullable=True),
        sa.Column('status', sa.Integer, nullable=True),
        sa.Column('type', sa.Integer, nullable=True),
        sa.Column('parent_id', sa.BigInteger, nullable=True),
        sa.Column('flag_show', sa.String, nullable=True),
        sa.Column('area', sa.Text, nullable=True),
        sa.Column('remark', sa.Text, nullable=True),
        sa.Column('created_at', sa.DateTime(timezone=True), nullable=True),
        sa.Column('updated_at', sa.DateTime(timezone=True), nullable=True),
        sa.Column('deleted_at', sa.DateTime(timezone=True), nullable=True),
    )
    pass

    op.create_table(
        'companies',
        sa.Column('id', sa.BigInteger, primary_key=True, index=True),
        sa.Column('code', sa.String, nullable=True),
        sa.Column('name', sa.String, nullable=True),
        sa.Column('type', sa.String, nullable=True),
        sa.Column('parent_id', sa.BigInteger, nullable=True),
        sa.Column('ownership', sa.String, nullable=True),
        sa.Column('block', sa.String, nullable=True),
        sa.Column('address', sa.Text, nullable=True),
        sa.Column('remark', sa.Text, nullable=True),
        sa.Column('created_at', sa.DateTime(timezone=True), nullable=True),
        sa.Column('updated_at', sa.DateTime(timezone=True), nullable=True),
        sa.Column('deleted_at', sa.DateTime(timezone=True), nullable=True),
    )
    pass

def downgrade() -> None:
    op.drop_table('locations')
    pass
