import datetime
from pydantic import BaseModel
from typing import List, Optional
from typing_extensions import Annotated
from fastapi.param_functions import Form

class Schema(BaseModel):
  flag_sync: Optional[bool] = None
  module: Optional[str] = 'transaction/delivery-orders'
  name: Optional[str] = None
  description: Optional[str] = None
  base64: Optional[str] = None
  filename: Optional[str] = None

class Display(Schema):
  id: int

class DisplayRead(Display):
  created_by: Optional[str] = None
  created_at: Optional[datetime.datetime] = None
  updated_by: Optional[str] = None
  updated_at: Optional[datetime.datetime] = None
  deleted_by: Optional[str] = None
  deleted_at: Optional[datetime.datetime] = None
