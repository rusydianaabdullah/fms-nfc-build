from os import getenv
import requests
from fastapi.encoders import jsonable_encoder
from config import statics
from sqlalchemy.sql import text
from sqlalchemy.orm import Session
from sqlalchemy.exc import IntegrityError
from utils.hash import Hash
from utils.exceptions import BadRequest400, NotFound404
from utils.std_repo import BrowseQueries, BulkId, browse, get_by_id, soft_delete
from utils.cctv import run_cctv
from utils.helper import date
from utils.logs import ErrorLog
from .schema import Schema
from .model import Cctv as BaseModel
from app.nfc.model import DeliveryOrder
from app.local_config.model import LocalConfig

class CctvRepo:
  def create(db: Session, name: str, filename: str = None, module: str = 'transaction/delivery-orders', tare_id: int = None):
    local_config    = db.query(LocalConfig).first()
    TYPE = local_config.type if local_config else getenv('TYPE')
    LOC_CODE = local_config.loc_code if local_config else getenv('LOC_CODE')
        
    base64 = run_cctv()
    data = BaseModel()
    filenm          = filename if filename else name
    filenm         += '_'
    filenm         += LOC_CODE
    # filenm         += local_config.wc_code if local_config.wc_code else local_config.loc_code
    filenm         += '_'
    filenm         += TYPE
    # filenm         += str(date.timemillis())
    data.module     = module
    data.name       = name
    data.filename   = f'{filenm}.jpg'
    data.base64     = base64
    data.flag_sync  = False
    data.description= f'{local_config.loc_code} {local_config.type}, {local_config.wc_code}'
    data.tare_id    = tare_id
    try:
      db.add(data)
      if module == 'transaction/delivery-orders':
        field = 'cctv_0' if TYPE == 'out' else 'cctv_1'
        db.query(DeliveryOrder).filter(DeliveryOrder.code == name).update({field: base64})
      db.commit()
      db.refresh(data)
      return data
    except Exception as e:
      db.rollback()
      raise BadRequest400(str(e))


  def update(id: int, request: Schema, db: Session, cred: str):
    try:
      if db.query(BaseModel.id).filter(BaseModel.id == id).first():
        updates = {'updated_by': cred}
        for index in BaseModel.fillable:
          if hasattr(request, index):
            updates.update({index: getattr(request, index)})
        data = db.query(BaseModel).filter(BaseModel.id == id).update(updates)
        db.commit()
        data = db.query(BaseModel).filter(BaseModel.id == id).first()
        return data
      else:
        raise NotFound404(None, id)
    except IntegrityError as e:
      db.rollback()
      raise BadRequest400(str(e))


  def cctv_post(db: Session, token: str, limit: int = None):
    try:
      data = db.query(BaseModel).filter(BaseModel.base64 != None).filter(BaseModel.module == 'master/truck-tares').filter(BaseModel.deleted_at == None).filter(BaseModel.flag_sync == False)
      data = data.order_by(BaseModel.id)
      if limit:
        data = data.limit(limit)
      data = data.all()
      success = 0
      failed  = 0
      jsons = jsonable_encoder(data)
      for row in jsons:
        base64 = row['base64']
        if base64:
          row['base64'] = f'data:image/png;base64,{base64}'
          # return row
          r = ''
          if row['module'] == 'master/truck-tares':
            if row['module_id']:
              r = CctvRepo.post_each(db, row, token)
          else:
            r = CctvRepo.post_each(db, row, token)
          if r == 'success': success = success + 1
          if r == 'failed': failed = failed + 1
        # else: failed = failed + 1
      return {'message': f'success: {success}, failed: {failed}'}
    except Exception as e:
      return {'message': f'failed: {str(e)}'}

  def post_each(db: Session, row: dict, token: str):
    try:
      url     = statics['cctv']
      headers = {"Authorization": f"Bearer {token}"}
      request = requests.post(url, headers=headers, json=row)
      if request.status_code == 200:
        db.query(BaseModel).filter(BaseModel.id == row['id']).update({BaseModel.flag_sync: True})
        db.commit()
        return 'success'
      else:
        print('error: ', request.content)
        ErrorLog.create(db, str(request.content), 'cctv_post')
        return 'failed'
    except Exception as e:
      ErrorLog.create(db, str(e), 'cctv_post')
      print(e)
      return 'failed'



  def get_index(browse_queries: BrowseQueries, db: Session):
    data = browse(browse_queries=browse_queries, db=db, model=BaseModel)
    return data


  def get_id(id: int, db: Session):
    data = get_by_id(id=id, db=db, model=BaseModel)
    return data


  def delete(request: BulkId, id: int, db: Session, cred: str):
    return soft_delete(request=request, id=id, db=db, model=BaseModel, cred=cred, type = 'delete')


  def restore(request: BulkId, id: int, db: Session, cred: str):
    return soft_delete(request=request, id=id, db=db, model=BaseModel, cred=cred, type = 'restore')
