import uuid

from db.database import Base
from sqlalchemy import Boolean, Column, DateTime, Date, Double, func
from sqlalchemy.sql.sqltypes import BigInteger, String, Integer, Text
from sqlalchemy.orm import relationship

class Cctv(Base):
  __tablename__ = 'cctv'

  id = Column(BigInteger, primary_key=True, index=True)
  flag_sync = Column(Boolean, default=False)
  module = Column(String, nullable=True, default='transaction/delivery-orders')
  name = Column(String, nullable=True)
  description = Column(String, nullable=True)
  base64 = Column(Text, nullable=True)
  filename = Column(String, nullable=True)
  module_id = Column(BigInteger, nullable=True)
  tare_id = Column(BigInteger, nullable=True)

  created_at = Column(DateTime, default=func.now())
  updated_at = Column(DateTime, default=func.now(), onupdate=func.now())
  deleted_at = Column(DateTime, nullable=True)

  fillable = [
    'module',
    'name',
    'base64',
    'filename',
    'description'
  ]
