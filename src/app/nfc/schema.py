import datetime
from pydantic import BaseModel
from typing import List, Optional
from typing_extensions import Annotated
from fastapi.param_functions import Form

class NfcReq:
  def __init__(
    self,
    *,
    type: Annotated[str,Form()] = 'read / write',
    uid: Annotated[str,Form()] = 'xxxxxxxxx',
    device_id: Annotated[str,Form()] = 'xxxxxxxxx',
    status: Annotated[str,Form()] = 'success / failed',
    do: Annotated[str,Form()] = None,
    truck: Annotated[str,Form()] = None,
    data1: Annotated[str,Form()] = '3576014403910003',
    data2: Annotated[str,Form()] = 'CK_CK-OHT773_CK-',
    data3: Annotated[str,Form()] = '0SP05A2401081053',
    data4: Annotated[str,Form()] = '0102794254827548',
    data5: Annotated[str,Form()] = '                ',
    data6: Annotated[str,Form()] = '                ',
    data7: Annotated[str,Form()] = '                ',
    data8: Annotated[str,Form()] = '                ',
    data9: Annotated[str,Form()] = '                ',
    data10: Annotated[str,Form()] = '                ',
    data11: Annotated[str,Form()] = '                ',
    data12: Annotated[str,Form()] = '                ',
  ):
    self.type = type
    self.uid = uid
    self.device_id = device_id
    self.status = status
    self.do = do
    self.truck = truck
    self.data1 = data1
    self.data2 = data2
    self.data3 = data3
    self.data4 = data4
    self.data5 = data5
    self.data6 = data6
    self.data7 = data7
    self.data8 = data8
    self.data9 = data9
    self.data10 = data10
    self.data11 = data11
    self.data12 = data12

class Nfc(BaseModel):
  type: Optional[str] = 'read'
  uid: Optional[str] = ''
  device_id: Optional[str] = ''
  status: Optional[str] = ''
  data1: Optional[str] = ''
  write1: Optional[str] = 'false'
  data2: Optional[str] = ''
  write2: Optional[str] = 'false'
  data3: Optional[str] = ''
  write3: Optional[str] = 'false'
  data4: Optional[str] = ''
  write4: Optional[str] = 'false'
  data5: Optional[str] = ''
  write5: Optional[str] = 'false'
  data6: Optional[str] = ''
  write6: Optional[str] = 'false'
  data7: Optional[str] = ''
  write7: Optional[str] = 'false'
  data8: Optional[str] = ''
  write8: Optional[str] = 'false'
  data9: Optional[str] = ''
  write9: Optional[str] = 'false'
  data10: Optional[str] = ''
  write10: Optional[str] = 'false'
  data11: Optional[str] = ''
  write11: Optional[str] = 'false'
  data12: Optional[str] = ''
  write12: Optional[str] = 'false'


class Schema(BaseModel):
  flag_sync: Optional[bool] = None
  type: Optional[int] = None
  code: Optional[str] = None
  truck_code: Optional[str] = None
  no_lambung: Optional[str] = None
  owner_code: Optional[str] = None
  pit_code: Optional[str] = None
  seam_code: Optional[str] = None
  datetime_0: Optional[int] = None
  date_0: Optional[datetime.date] = None
  rit_0: Optional[int] = None
  shift_0: Optional[str] = None
  user_0: Optional[str] = None
  wc_0: Optional[str] = None
  loc_0: Optional[str] = None
  bruto_0: Optional[float] = None
  net_0: Optional[float] = None
  rfid_0: Optional[str] = None
  nfc_0: Optional[str] = None
  abnormal_0: Optional[str] = None
  datetime_1: Optional[int] = None
  date_1: Optional[datetime.date] = None
  rit_1: Optional[int] = None
  shift_1: Optional[str] = None
  user_1: Optional[str] = None
  wc_1: Optional[str] = None
  loc_1: Optional[str] = None
  bruto_1: Optional[float] = None
  net_1: Optional[float] = None
  rfid_1: Optional[str] = None
  nfc_1: Optional[str] = None
  abnormal_1: Optional[str] = None
  reprint: Optional[int] = None


class Display(Schema):
  id: int

class DisplayRead(Display):
  created_by: Optional[str] = None
  created_at: Optional[datetime.datetime] = None
  updated_by: Optional[str] = None
  updated_at: Optional[datetime.datetime] = None
  deleted_by: Optional[str] = None
  deleted_at: Optional[datetime.datetime] = None

class NfcCfgReq:
  def __init__(
    self,
    *,
    type: Annotated[str,Form()] = 'reg',
    truck_code: Annotated[str,Form()] = 'CK_CK-OHT773_000',
    nik: Annotated[str,Form()] = '3576014403910003',
    min_weight: Annotated[int,Form()] = None,
    min_truck_tare: Annotated[int,Form()] = None,
    auto_print: Annotated[bool,Form()] = False,
    thermal_print: Annotated[bool,Form()] = False,
    tare_max_day: Annotated[int,Form()] = None,
  ):
    self.type = type
    self.truck_code = truck_code
    self.nik = nik
    self.min_weight = min_weight
    self.min_truck_tare = min_truck_tare
    self.auto_print = auto_print
    self.thermal_print = thermal_print
    self.tare_max_day = tare_max_day