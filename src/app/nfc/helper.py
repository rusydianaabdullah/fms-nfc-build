import requests
import json
from config import getenv
from sqlalchemy import column, text
from sqlalchemy.orm import Session
from app.auth import oauth2
from utils.exceptions import BadRequest400
from utils.helper import date
# from utils.weighing import run_weighing
from utils.mqtt import send_mqtt
from app.weighing.model import DeliveryOrderTemp
from .model import DeliveryOrder as BaseModel, NfcConfig
from .schema import NfcReq
from ..truck.model import TruckTare, Truck, WeighingCenter
from ..local_config.model import LocalConfig

class NfcHelper:
  def gen_token():
    expires_delta = date.timedelta(30)
    return oauth2.create_access_token(data = {'sub': 'system'}, expires_delta=expires_delta)

  def check_minweight(cfg: NfcConfig, weighing: int = None):
    # weighing = run_weighing(False)
    if weighing == None:
      raise BadRequest400(f'berat tidak terdeteksi')
    elif weighing <= 5000:
      raise BadRequest400(f'berat <= 5000')
    elif cfg:
      if weighing and cfg.min_weight > weighing:
        raise BadRequest400(f'berat < {cfg.min_weight}')

  def get_tare(truck_code: str, local_cfg: LocalConfig, db: Session):
    WC_CODE = local_cfg.wc_code
    tare = 0
    try:
      t = db.query(TruckTare.value).filter(TruckTare.truck_code == truck_code).filter(TruckTare.wc_code == WC_CODE).order_by(TruckTare.id_.desc()).value(column('value'))
      tare = int(t) if int(t) > 0 else 0
    except:
      pass
    return tare

  def get_truck(truck_code, db: Session):
    truck = ''
    try:
      truck = db.query(Truck.name).filter(Truck.code == truck_code).value(column('name'))
      return truck
    except Exception as e:
      print(f'get_truck:{str(e)}')
      pass
    return truck

  def get_abnormal(lastdo: BaseModel, local_config: LocalConfig, rfid: str, db: Session):
    TYPE        = local_config.type
    LOC_CODE    = local_config.loc_code
    truck_code  = lastdo.truck_code
    code        = lastdo.code
    abnormal = ''
    try:
      if rfid == truck_code         : ''
      elif rfid.find('error') > -1  : abnormal += f'RFID Error: {rfid}'
      elif rfid == ''               : abnormal += 'RFID IS NOT DETECTED'
      elif rfid != truck_code       : abnormal += f'DIFF RFID: {rfid}'
      else                          : abnormal += f'RFID OTHERS: {rfid}'

      if TYPE == 'in':
        dotemp = db.query(DeliveryOrderTemp.loc_1).filter(DeliveryOrderTemp.code == code).first()
        if dotemp:
          loc_1 = dotemp.loc_1
          if loc_1 != LOC_CODE:
            if abnormal != '': abnormal += ' | '
            abnormal += f'DIFF LOCATION IN: {loc_1}'
    except Exception as e:
      print(f'NfcHelper get_abnormal: {str(e)}')
      pass
    if abnormal != '': send_mqtt(f'abnormal: {abnormal}')
    else: abnormal = '-'
    return abnormal

  def get_wc0(wc_0: str, db: Session):
    try:
      code = db.query(WeighingCenter.code).filter(WeighingCenter.prefix == wc_0).value(column('code'))
      if code:
        wc_0 = code
    except:
      pass
    return wc_0

  def gen_code(request: BaseModel, prefix: str, shiftdate: dict, db: Session):
    try:
      shf   = shiftdate.get('shift')
      dt    = shiftdate.get('date')
      ct    = db.query(BaseModel).filter(BaseModel.deleted_at == None).filter(BaseModel.date_0 == dt).filter(BaseModel.shift_0 == shf).count()
      count = int(ct)+1
      code_date = date.code_date()
      code = f'{prefix}{code_date}{shf}{count:03}'
      return {'code': code, 'rit': count}
    except Exception as e:
      raise BadRequest400(str(e))

  def parselastdata(request: NfcReq, type: str = None):
    try:
      driver      = request.data1
      vendortruck = request.data2
      dd  = request.data3           # 0SP05YYMMDDSXXXX
      ddt = request.data4           #
      dd2 = request.data5           #
      falsemessage = ''
      code        = '                '
      vendor_code = ''
      truck_code  = ''
      pit_code    = 'Sitarum'
      owner_code  = 'BMBBD'
      datetime_0  = 0
      date_0      = ''
      shift_0     = ''
      rit_0       = ''
      wc_0        = ''
      loc_0       = ''
      bruto_0     = ''
      net_0       = ''

      try:
        sp = vendortruck.split('_')
        if sp[0]: vendor_code = sp[0]
        if sp[1]: truck_code = sp[1]
      except:
        pass

      if (type == 'out'):                       # TYPE OUT
        closenfc = getenv('CLOSENFC', '0')
        if closenfc == '1':
          if dd[0:1] == '0':                        # ADA DATA YANG BELUM CLOSE
            falsemessage = 'DO belum close'

      elif (type == 'in'):                        # TYPE IN
        if dd[0:1] == '0':
          code  = dd[1:]

          # WC & LOC Origin
          wc    = code[0:5]
          wc_0  = wc
          loc_0 = 'SP0-5'
          if dd2.find('L02') > -1: loc_0 = 'SP0 DUMMY'
          elif dd2.find('L02') > -1: loc_0 = 'SP0 DUMMY'
          elif dd2.find('L03') > -1: loc_0 = 'SP0 DUMMY 2'
          elif dd2.find('CP1') > -1: loc_0 = 'CP1'
          elif dd2.find('CP2') > -1: loc_0 = 'CP2'
          elif dd2.find('CP8') > -1: loc_0 = 'CP8'
          elif wc.find('CP') > -1: loc_0 = wc[0:3]

          # PIT & OWNER CODE
          if dd2.find('P02') > -1: pit_code = 'BMR'
          if dd2.find('C02') > -1: owner_code = 'BMR'

          datetime_0 = date.decodeminute(year=code[5:7], time=ddt[0:6])
          date_0 = f'20{code[5:7]}-{code[7:9]}-{code[9:11]}'
          shift_0 = code[11:12]
          rit_0 = int(code[12:])
          bruto_0 = int(ddt[6:11])
          net_0 = int(ddt[11:])
        else:
          falsemessage = 'DO tidak ditemukan'

      if falsemessage != '':
        return {
          'status': 'failed',
          'message': falsemessage
        }

      return {
        'status': 'success',
        'driver': driver,
        'pit_code': pit_code,
        'owner_code': owner_code,
        'vendor_code': vendor_code,
        'truck_code': truck_code,
        'code': code,
        'datetime_0': datetime_0,
        'date_0': date_0,
        'shift_0': shift_0,
        'rit_0': rit_0,
        'wc_0': wc_0,
        'loc_0': loc_0,
        'bruto_0': bruto_0,
        'net_0': net_0,
        'nfc_0': '',
      }
    except Exception as e:
      return {
        'status': 'failed',
        'message': str(e)
      }

  def check_existing_do_local(code: str, db: Session):
    try:
      sqlquery = f"""select code from do_closed where code = '{code}'"""
      data = db.execute(text(sqlquery)).first()
      if data: return True
      return False
    except:
      return False

  def check_existing_do_realtime(code: str):
    try:
      token     = NfcHelper.gen_token()
      headers   = {"Authorization": f"Bearer {token}"}
      endpoint  = f"https://fms-api2.bmbcoal.com/v1/transaction/delivery-orders?limit=1&where=status:1&where=code:{code}"
      res       = requests.get(endpoint, headers=headers)
      lists     = json.loads(res.content)
      if lists: return True
      return False
    except:
      return False

  def genNfcCode3(owner_code: str, loc_code: str):
    pit   = 'P01'                                     # 3 [0:3]
    owner = 'C01'                                   # 6 [3:6]
    if owner_code == 'BMR':
      owner = 'C02'
      pit   = 'P02'
    loc = 'L01'                                     # 9 [6:9]
    if loc_code == 'SP0-5': loc = 'L01'
    elif loc_code == 'SP0 DUMMY': loc = 'L02'
    elif loc_code == 'SP0 DUMMY 2': loc = 'L03'
    else: loc = loc_code                            # CP1-CP8
    nfccode3 = f'{pit}{owner}{loc}XXXXXXX'
    return nfccode3
