import json
import requests
import aiohttp
from fastapi.encoders import jsonable_encoder
from config import getenv, statics
from sqlalchemy import func
from sqlalchemy.sql import text
from sqlalchemy.orm import Session, joinedload
from sqlalchemy.exc import IntegrityError
from starlette.background import BackgroundTasks

from utils.exceptions import BadRequest400, NotFound404
from utils.std_repo import BrowseQueries, BulkId, browse, get_by_id, soft_delete
from utils.helper import date
from utils.mqtt import send_mqtt, open_gate, close_gate
from utils.rfid import run_rfid
from utils.cctv import run_cctv
from utils.weighing import run_weighing

# from app.truck.model import Truck
from .schema import Schema, NfcReq, NfcCfgReq
from .model import DeliveryOrder as BaseModel, NfcConfig
from .helper import NfcHelper
from .transform import NfcTransform
from ..cctv.repo import CctvRepo
from ..cctv.model import Cctv
from ..truck.repo import TruckRepo
from ..local_config.model import LocalConfig
from utils.logs import ErrorLog

class NfcRepo:
  ####### CONFIG #######
  def get_nfc_config(db: Session, weighing = False, cctv = False, rfid = False, opengate = False, closegate = False, loc = False):
    try:
      if weighing: return run_weighing()
      elif cctv: return {'cctv': run_cctv(False), 'last_update': run_cctv(False, True), 'rfid': run_rfid(db, False)}
      elif rfid: return run_rfid(db, True)
      elif opengate: return open_gate()
      elif closegate: return close_gate()
      elif loc: return NfcRepo.get_loc(db)
      else:
        data = db.query(NfcConfig).first()
        if data: return data
        else:                        # HANDLE FIRST TIME DATA
          data = NfcConfig(
            type = 'reg',
            min_weight = 0,
            auto_print = True,
            thermal_print = False,
            diff_rfid = True,
            tare_max_day = 5,
            min_truck_tare = 10000
          )
          db.add(data)
          db.commit()
          db.refresh(data)
          return data
    except Exception as e:
      raise BadRequest400(e)

  def nfc_config(request: NfcCfgReq, db: Session):
    try:
      data = db.query(NfcConfig).first()
      if data:
        db.query(NfcConfig).update({
          NfcConfig.type: request.type,
          NfcConfig.min_weight: request.min_weight,
          NfcConfig.auto_print: request.auto_print,
          NfcConfig.thermal_print: request.thermal_print,
          NfcConfig.min_truck_tare: request.min_truck_tare,
          NfcConfig.tare_max_day: request.tare_max_day
        })
      else:
        data = NfcConfig(
          type = request.type,
          min_weight = request.min_weight,
          auto_print = request.auto_print,
          thermal_print = request.thermal_print,
          min_truck_tare = request.min_truck_tare,
          tare_max_day = request.tare_max_day
        )
        db.add(data)
      db.commit()
      db.refresh(data)
      return data
    except Exception as e:
      raise BadRequest400(e)

  def toggle_diff_rfid(db: Session):
    try:
      data = db.query(NfcConfig).first()
      if data:
        db.query(NfcConfig).update({NfcConfig.diff_rfid: True})
      else:
        data = NfcConfig(diff_rfid = True)
        db.add(data)
      db.commit()
      return data
    except Exception as e:
      raise BadRequest400(e)

  def get_loc(db: Session):
    try:
      local_cfg = db.query(LocalConfig).first()

      company = ['BMBBD', 'BMR']
      loc_0 = []
      loc_1 = []

      envCompany = getenv('COMPANY')
      if envCompany: company = envCompany.split(',')
      
      envLoc_0 = getenv('LOC_0')
      if envLoc_0: loc_0 = envLoc_0.split(',')

      envLoc_1 = getenv('LOC_1')
      if envLoc_1: loc_1 = envLoc_1.split(',')

      if local_cfg.hauling == '1':
        if len(loc_0) == 0: loc_0 = ['SP0-5', 'SP0 DUMMY']
        if len(loc_1) == 0: loc_1 = ['CP1', 'CP2', 'CP8']
      elif local_cfg.hauling == '2':
        if len(loc_0) == 0: loc_0 = ['CP1', 'CP2', 'CP8']
        if len(loc_1) == 0: loc_1 = ['PORT BMBBD', 'PORT TCT', 'JETTY R', 'SEI PUTING']

      return {'company': company, 'loc_0': loc_0, 'loc_1': loc_1}
    except Exception as e:
      raise BadRequest400(e)

  ####### NFC TAPPING #######
  def nfc(request: NfcReq, db: Session, bt: BackgroundTasks):
    check = db.query(NfcConfig).first()
    if check is None            : return NfcRepo.nfc_do(request, db, bt)
    elif check.type == 'cancel' : return NfcRepo.nfc_cancel(request, db)
    elif check.type == 'tare'   : return TruckRepo.nfc_tare(request, db)
    else                        : return NfcRepo.nfc_do(request, db, bt, check)

  def nfc_cancel(request: NfcReq, db: Session):
    try:
      if (request.type == 'write'):
        res = {
          'status': request.status,
          'message': 'Write failed!' if request.status == 'failed' else 'Write success!'
        }
        send_mqtt(res['message'])
        send_mqtt('nfc_cancel')
        return res
      else:
        code = '               '
        dd = request.data3
        if dd[0:1] == '0':                        # ADA DATA YANG BELUM CLOSE
          code = dd[1:]
        # SAVE LOGS CLOSE DO
        do = BaseModel(
          type        = 3,
          driver      = request.data1,
          truck_code  = request.data2,
          code        = f'{code}-CLOSE_DO',
          datetime_0  = date.timemillis(),
          flag_sync   = True
        )
        db.add(do)
        db.commit()
        response = {
          'status': 'success',
          'message': 'Proses close DO!',
        }
        res = NfcTransform.ask_write(request, response, f'1{code}')
        ErrorLog.create(db, code, 'nfc_do_cancel', request.data2)
        return res
    except Exception as e:
      msg = 'NfcRepo nfc cancel: ' + str(e)
      db.rollback()
      ErrorLog.create(db, msg, 'nfc_do_cancel', request.data2)
      return {'status': 'failed', 'message': msg}

  def nfc_do(request: NfcReq, db: Session, bt: BackgroundTasks, cfg: NfcConfig = None):
    try:
      if (request.type == 'write'):                             # AFTER NFC WRITE DATA
        res = NfcRepo.nfc_after_write(request=request, db=db, bt=bt)
        send_mqtt(res['message'])
        return res
      else:                                                     # AFTER NFC READ DATA
        local_cfg = db.query(LocalConfig).first()
        TYPE  = local_cfg.type if local_cfg else getenv('TYPE')
        nfc   = NfcHelper.parselastdata(request=request, type=TYPE)
        if nfc['status'] == 'failed': return nfc                    # IF DO NA OR NOT CLOSED YET

        res = { 'status': request.status, 'message': 'Read Data!' }
        if (TYPE == 'out'): res = NfcRepo.nfc_type_out(request=request, db=db, nfc=nfc, local_cfg=local_cfg, cfg=cfg)
        else: res = NfcRepo.nfc_type_in(request=request, db=db, nfc=nfc, local_cfg=local_cfg, cfg=cfg)
        send_mqtt(res['message'])
        return res
    except Exception as e:
      db.rollback()
      ErrorLog.create(db, str(e), 'nfc_do')
      return {'status': 'failed', 'message': str(e)}

  def nfc_type_out(request: NfcReq, db: Session, nfc: dict, local_cfg: LocalConfig = None, cfg: NfcConfig = None):
    truck_code = nfc['truck_code']
    try:
      do = BaseModel()
      docreate = True
      code  = ''
      # CHECK LAST TRUCK | VALIDATE MULTIPLE TAP
      try:
        validate_double = getenv('DOUBLENFC', '0')
        if validate_double == '1':
          lastquery = "select code from delivery_orders where datetime_0 is not null order by datetime_0 desc, id desc limit 1"
          lasttruck = db.execute(text(lastquery)).first()
          if lasttruck.code:
            separator = lasttruck.code.find('-')
            separator += 1
            do_truck = lasttruck.code[separator:]
            # print('do_truck', do_truck)
            # print('truck_code', truck_code)
            if do_truck == truck_code:
              docreate = False
              do = db.query(BaseModel).filter(BaseModel.datetime_0 != None).order_by(BaseModel.datetime_0.desc()).first()
              separator2 = do.code.find('-')
              code = do.code[:separator2]
      except: ''

      if docreate:
        LOC_CODE    = local_cfg.loc_code if local_cfg else getenv('LOC_CODE')
        WC_CODE     = local_cfg.wc_code if local_cfg else getenv('WC_CODE')
        HAULING     = int(local_cfg.hauling) if local_cfg else int(getenv('HAULING'))
        PIT_CODE    = local_cfg.pit_code if local_cfg else getenv('PIT_CODE')
        OWNER_CODE  = local_cfg.owner_code if local_cfg else getenv('OWNER_CODE')
        PREFIX      = local_cfg.prefix if local_cfg else getenv('PREFIX')
        DESTINATION = local_cfg.destination if local_cfg else ''

        CCTV        = run_cctv(False)
        RFID        = run_rfid(db, False)

        lastloginuser = cfg.lastlogin_user if cfg.lastlogin_user else 'system'
        shiftdate   = date.shift_date()
        shf         = shiftdate.get('shift')
        dt          = shiftdate.get('date')
        weighing    = 0
        tare        = 0

        if WC_CODE:
          weighing    = run_weighing()
          NfcHelper.check_minweight(cfg, weighing) # CHECK MINIMUM WEIGHTNYA
          tare        = NfcHelper.get_tare(nfc['truck_code'], local_cfg, db)

        code_rit      = NfcHelper.gen_code(request, PREFIX, shiftdate, db)
        truck_name    = NfcHelper.get_truck(truck_code, db)
        code          = code_rit['code']
        rit           = code_rit['rit']
        do = BaseModel(
          type        = HAULING,
          owner_code  = OWNER_CODE,
          pit_code    = PIT_CODE,
          code        = f'{code}-{truck_code}',
          vendor_code = nfc['vendor_code'],
          driver      = nfc['driver'],
          truck_code  = truck_code,
          no_lambung  = truck_name,
          datetime_0  = date.timemillis(),
          date_0      = dt,
          shift_0     = shf,
          rit_0       = rit,
          wc_0        = WC_CODE,
          loc_0       = LOC_CODE,
          loc_1       = DESTINATION,
          bruto_0     = weighing,
          net_0       = weighing - tare,
          nfc_0       = request.uid,
          rfid_0      = RFID,
          cctv_0      = CCTV,
          user_0      = lastloginuser,
          # deleted_at  = func.now() # NEW | Implement saat update NFC sudah dilaksanakan
        )
        abnormal: str = NfcHelper.get_abnormal(do, local_cfg, RFID, db)

        SKIP_DIFF_RFID = getenv('SKIP_DIFF_RFID', '0')

        if SKIP_DIFF_RFID == '0' and abnormal.find('RFID') > -1:                                 # DONT WRITE CARD! RFID VALIDATION !!!
          if cfg.diff_rfid:
            db.query(NfcConfig).update({NfcConfig.diff_rfid: False})
            db.commit()
          else:
            msg = f'{truck_code} {truck_name} {abnormal}'
            send_mqtt(f'popuprfid:{msg}')
            return { 'status': 'failed', 'message': msg }

        do.abnormal_0 = abnormal
        do.deleted_at = func.now()                                    # Prevent Double DO
        db.add(do)
        db.commit()
        db.refresh(do)

      if do.loc_1 == 'CP8': nfccode1 = f'0{code}'                     # HAPUS SAAT NFC SUDAH TERPASANG DI ALL CP
      else: nfccode1 = f'1{code}'

      minute = date.encodeminute(year=code[5:7])
      nfccode2 = f'{minute:06}{round(do.bruto_0):05}{round(do.net_0):05}'
      nfccode3 = NfcHelper.genNfcCode3(do.owner_code, do.loc_0)

      response = {
        'status': 'success',
        'message': f'{round(do.net_0)}Kg, mohon tunggu...',
        'do_id': do.id,
        'code': do.code
      }
      res = NfcTransform.ask_write(request, response, nfccode1, nfccode2, nfccode3)
      return res
    except Exception as e:
      db.rollback()
      msg = 'NfcRepo nfc out: ' + str(e)
      ErrorLog.create(db, msg, 'nfc_do_out')
      return {
        'status': 'failed',
        'message': 'Coba tap lagi' + msg
      }

  def nfc_type_in(request: NfcReq, db: Session, nfc: dict, local_cfg: LocalConfig = None, cfg: NfcConfig = None):
    LOC_CODE    = local_cfg.loc_code if local_cfg else getenv('LOC_CODE')
    WC_CODE     = local_cfg.wc_code if local_cfg else getenv('WC_CODE')
    HAULING     = int(local_cfg.hauling) if local_cfg else int(getenv('HAULING'))
    shiftdate   = date.shift_date()
    shf         = shiftdate.get('shift')
    dt          = shiftdate.get('date')
    weighing    = 0
    tare        = 0
    CCTV        = run_cctv(False)
    RFID        = run_rfid(db, False)

    lastloginuser = cfg.lastlogin_user if cfg.lastlogin_user else 'system'

    if WC_CODE:
      weighing  = run_weighing()
      NfcHelper.check_minweight(cfg, weighing) # CHECK MINIMUM WEIGHTNYA
      tare      = NfcHelper.get_tare(nfc['truck_code'], local_cfg, db)

    code = nfc['code']
    truck_code = nfc['truck_code']
    try:
      if NfcHelper.check_existing_do_local(f'{code}-{truck_code}', db):
        return { 'status': 'failed', 'message': 'DO Sudah Close!' }
      count       = db.query(BaseModel).filter(BaseModel.date_1 == dt).filter(BaseModel.shift_1 == shf).count()
      truck_name  = NfcHelper.get_truck(truck_code, db)
      do = BaseModel(
        type        = HAULING,
        code        =f'{code}-{truck_code}',
        driver      = nfc['driver'],
        pit_code    = nfc['pit_code'],
        owner_code  = nfc['owner_code'],
        vendor_code = nfc['vendor_code'],
        truck_code  = truck_code,
        no_lambung  = truck_name,
        datetime_0  = nfc['datetime_0'],
        date_0      = nfc['date_0'],
        shift_0     = nfc['shift_0'],
        rit_0       = nfc['rit_0'],
        wc_0        = nfc['wc_0'],
        loc_0       = nfc['loc_0'],
        bruto_0     = nfc['bruto_0'],
        net_0       = nfc['net_0'],

        datetime_1  = date.timemillis(),
        date_1      = dt,
        shift_1     = shf,
        rit_1       = count+1,
        wc_1        = WC_CODE,
        loc_1       = LOC_CODE,
        bruto_1     = weighing,
        net_1       = weighing - tare,
        nfc_1       = request.uid,
        rfid_1      = RFID,
        cctv_1      = CCTV,
        user_1      = lastloginuser,
        # deleted_at  = func.now() # NEW | Implement saat update NFC sudah dilaksanakan
      )
      abnormal: str = NfcHelper.get_abnormal(do, local_cfg, RFID, db)

      SKIP_DIFF_RFID = getenv('SKIP_DIFF_RFID', '0')

      if SKIP_DIFF_RFID == '0' and abnormal.find('RFID') > -1:  
        if cfg.diff_rfid:
          db.query(NfcConfig).update({NfcConfig.diff_rfid: False})
          db.commit()
        else:
          msg = f'{truck_code} {truck_name} {abnormal}'
          send_mqtt(f'popuprfid:{msg}')
          return { 'status': 'failed', 'message': msg }

      do.abnormal_1 = abnormal
      do.deleted_at = func.now()                                    # Prevent Double DO
      db.add(do)
      db.commit()
      db.refresh(do)
      nfccode1 = f'1{code}'
      response = {
        'status': 'success',
        'message': f'{round(do.net_1)}Kg, mohon tunggu...',
        'do_id': do.id,
        'code': do.code
      }
      res = NfcTransform.ask_write(request, response, nfccode1)
      return res
    except Exception as e:
      db.rollback()
      msg = 'NfcRepo nfc in: ' + str(e)
      ErrorLog.create(db, msg, 'nfc_do_in')
      return {
        'status': 'failed',
        'message': 'Coba tap lagi'
      }

  def nfc_after_write(request: NfcReq, db: Session, bt: BackgroundTasks):
    try:
      cfg = db.query(NfcConfig).first()
      print('do:', request.do)
      print('truck:', request.truck)
      if request.do and request.truck:                                    # NEW | Implement saat update NFC sudah dilaksanakan
        truck_code = ''
        try: truck_code = request.truck.split('_')[1]
        except: ''
        donfc = request.do[1:]
        do = f'{donfc}-{truck_code}'
        if (request.status == 'failed'):
          db.query(BaseModel).filter(BaseModel.code == do).update({BaseModel.deleted_at: func.now()})
          db.commit()
        else:
          last = db.query(BaseModel.id).filter(BaseModel.code == do).order_by(BaseModel.id.desc()).first()
          if last:
            id = last.id
            db.query(BaseModel).filter(BaseModel.id == id).update({BaseModel.deleted_at: None})
            db.commit()
            if cfg.auto_print:
              send_mqtt(f'print:{id}')
            open_gate()

            if (getenv('SYNC_AFTER_SAVE', '1') == '1'): bt.add_task(NfcRepo.do_post_realtime, db, id)
            send_mqtt(f'do:{id}')
          else:
            return {'status': 'failed', 'message': 'DO tidak ditemukan'}
      else:                                                               # OLD
        if (request.status == 'failed'):
          last = db.query(BaseModel.id).filter(BaseModel.deleted_at == None).order_by(BaseModel.id.desc()).first()
          db.query(BaseModel).filter(BaseModel.id == last.id).update({BaseModel.deleted_at: func.now()})
          db.commit()
        else:
          last: BaseModel = db.query(BaseModel.id, BaseModel.code).filter(BaseModel.deleted_at == None).order_by(BaseModel.id.desc()).first()
          if last.code.find('CLOSE_DO') > -1:
            return {'status': 'failed', 'message': 'Last DO merupakan Close DO'}
          id = last.id
          if cfg.auto_print:
            send_mqtt(f'print:{id}')
          open_gate()
          
          if (getenv('SYNC_AFTER_SAVE', '1') == '1'): bt.add_task(NfcRepo.do_post_realtime, db, id)
          send_mqtt(f'do:{id}')
      res = {
        'status': request.status,
        'message': 'Write failed!' if request.status == 'failed' else 'Write success!'
      }
      return res
    except Exception as e:
      db.rollback()
      ErrorLog.create(db, str(e), 'nfc_do')
      return {'status': 'failed', 'message': str(e)}


  ####### POSTING DO #######
  def do_post_realtime(db: Session, id):
    try:
      data = db.query(BaseModel).filter(BaseModel.id == id).first()
      token = NfcHelper.gen_token()
      print(f'nfc.do_post_realtime:{id}')
      NfcRepo.do_post_by_id(id, db, token)
    except:
      pass
    return 'do_post_realtime triggered'

  async def do_post(db: Session, token: str, limit = 10):
    # if limit == 20:
    #   limit = 10
    data = db.query(BaseModel).filter(BaseModel.deleted_at == None).filter(BaseModel.flag_sync == False).filter(BaseModel.ready_sync == True)
    data = data.order_by(BaseModel.id)
    if limit:
      data = data.limit(limit)
    data = data.all()
    success = 0
    failed  = 0
    if data:
      jsons = jsonable_encoder(data)
      for row in jsons:
        # r = await NfcRepo.do_post_each(db, row, token)
        # if r == 'success': success = success + 1
        # if r == 'failed': failed = failed + 
        r = NfcRepo.do_post_by_id(row['id'], db, token, 'bulk')
        if r == 'success': success = success + 1
        if r == 'failed': failed = failed + 1
        print(f'do_post:{row['id']}[{r}]')
      send_mqtt('scheduler:sync_do')
    return {'message': f'success: {success}, failed: {failed}'}

  async def do_post_each(db: Session, row: dict, token: str):
    try:
      url     = statics['do']
      headers = {"Authorization": f"Bearer {token}"}
      
      async with aiohttp.ClientSession() as sess:
        async with sess.post(url, headers=headers, json=row) as response:
          if response.status == 200:
            res = await response.json()

            try:
              if res['id']:
                db.query(BaseModel).filter(BaseModel.id == row['id']).update({
                  BaseModel.flag_sync: True,
                  BaseModel.id_server: res['id'],
                  BaseModel.cctv_0: '',                 # biar ga berat komputernya
                  BaseModel.cctv_1: ''
                })
              else:
                db.query(BaseModel).filter(BaseModel.id == row['id']).update({
                  BaseModel.flag_sync: True,
                  BaseModel.cctv_0: '',                 # biar ga berat komputernya
                  BaseModel.cctv_1: ''
                })
                id = row['id']
                print(f'nfc.do_post_each:{id} [success]')
            except Exception as async_ex:
              print(f'nfc.do_post_each: Async exception: {async_ex}')
              db.query(BaseModel).filter(BaseModel.id == row['id']).update({
                BaseModel.flag_sync: True,
                BaseModel.cctv_0: '',                 # biar ga berat komputernya
                BaseModel.cctv_1: ''
              })

            db.commit()
            return 'success'
          else:
            error_content = await response.text()
            ErrorLog.create(db, error_content, 'nfc_do_post_400', row['code'], row['id'])
            return 'failed'      
    except Exception as e:
      ErrorLog.create(db, str(e), 'nfc_do_post', row['code'], row['id'])
      return 'failed'
    
  def _do_post_each(db: Session, row: dict, token: str):
    try:
      url     = statics['do']
      headers = {"Authorization": f"Bearer {token}"}
      request = requests.post(url, headers=headers, json=row)
      if request.status_code == 200:
        res = json.loads(request.content)
        try:
          if res['id']:
            db.query(BaseModel).filter(BaseModel.id == row['id']).update({
              BaseModel.flag_sync: True,
              BaseModel.id_server: res['id'],
              BaseModel.cctv_0: '',                 # biar ga berat komputernya
              BaseModel.cctv_1: ''
            })
          else:
            db.query(BaseModel).filter(BaseModel.id == row['id']).update({
              BaseModel.flag_sync: True,
              BaseModel.cctv_0: '',                 # biar ga berat komputernya
              BaseModel.cctv_1: ''
            })
        except:
          db.query(BaseModel).filter(BaseModel.id == row['id']).update({
            BaseModel.flag_sync: True,
            BaseModel.cctv_0: '',                 # biar ga berat komputernya
            BaseModel.cctv_1: ''
          })
        db.commit()
        return 'success'
      else:
        ErrorLog.create(db, str(request.content), 'nfc_do_post_400', row['code'], row['id'])
        # print('error: ', request.content)
        return 'failed'
    except Exception as e:
      ErrorLog.create(db, str(e), 'nfc_do_post', row['code'], row['id'])
      return 'failed'

  def do_post_by_id(id: int, db: Session, token: str, mode: str = 'id'):
    try:
      data    = db.query(BaseModel).filter(BaseModel.id == id).first()
      row     = jsonable_encoder(data)
      url     = statics['do']
      headers = {"Authorization": f"Bearer {token}"}
      # request
      session = requests.Session()
      session.mount('https://', requests.adapters.HTTPAdapter(max_retries=3))
      request = session.post(url, headers=headers, json=row, verify=False)
      if request.status_code == 200:
        res = json.loads(request.content)
        try:
          if res['id']:
            db.query(BaseModel).filter(BaseModel.id == row['id']).update({
              BaseModel.flag_sync: True,
              BaseModel.id_server: res['id'],
              BaseModel.cctv_0: '',                 # biar ga berat komputernya
              BaseModel.cctv_1: ''
            })
          else:
            db.query(BaseModel).filter(BaseModel.id == row['id']).update({
              BaseModel.flag_sync: True,
              BaseModel.cctv_0: '',                 # biar ga berat komputernya
              BaseModel.cctv_1: ''
            })
        except:
            db.query(BaseModel).filter(BaseModel.id == row['id']).update({
              BaseModel.flag_sync: True,
              BaseModel.cctv_0: '',                 # biar ga berat komputernya
              BaseModel.cctv_1: ''
            })
        db.commit()
        print(f'do_post_by_id:{id}[success]')
        if mode == 'id': return {'message': 'success'}
        else: return 'success'
      else:
        ErrorLog.create(db, str(request.content), 'nfc_do_post_400', data.code, id)
        print(f'do_post_by_id:{id}[failed]')
        db.query(BaseModel).filter(BaseModel.id == row['id']).update({BaseModel.ready_sync: True})
        db.commit()
        if mode == 'id': return {'message': str(request.content)}
        else: return 'failed'
    except Exception as e:
      ex = str(e)
      msg = f'DOPostById failed: {ex}'
      print(f'do_post_by_id:{id}[failed>except]')
      ErrorLog.create(db, msg, 'nfc_do_post', None, id)
      db.query(BaseModel).filter(BaseModel.id == row['id']).update({BaseModel.ready_sync: True})
      db.commit()
      if mode == 'id': raise BadRequest400(msg)
      else: return 'failed'


  def sync_do(db: Session, id: int):
    token = NfcHelper.gen_token()
    print(f'sync_do:background={id}')
    return NfcRepo.do_post_by_id(id, db, token)

  ####### DEFAULT CRUD #######
  def create(request: Schema, db: Session, cred: str):
    data = BaseModel()
    for index in BaseModel.fillable:
      try: setattr(data, index, getattr(request, index))
      except: None
    setattr(data, 'created_by', cred)
    try:
      db.add(data)
      db.commit()
      db.refresh(data)
      return data
    except Exception as e:
      db.rollback()
      raise BadRequest400(str(e))

  def update(id: int, request: Schema, db: Session, cred: str):
    try:
      if db.query(BaseModel.id).filter(BaseModel.id == id).first():
        updates = {'updated_by': cred}
        for index in BaseModel.fillable:
          if hasattr(request, index):
            updates.update({index: getattr(request, index)})
        data = db.query(BaseModel).filter(BaseModel.id == id).update(updates)
        db.commit()
        data = db.query(BaseModel).filter(BaseModel.id == id).first()
        return data
      else:
        raise NotFound404(None, id)
    except IntegrityError as e:
      db.rollback()
      raise BadRequest400(str(e))

  def get_index(browse_queries: BrowseQueries, is_online: bool, db: Session, token: str):
    try:
      if is_online:
        # online data
        # local_cfg
        local_cfg = db.query(LocalConfig).first()
        if local_cfg.type == 'in': do_type = '1'
        else: do_type = '0'
        # end local_cfg
        url = statics['do'] + '?'
        for key, val in browse_queries:
          if val:
            if key in ['like', 'between']:
              for subVal in val:
                url += f'{key}={subVal}&'
            else: url += f'{key}={val}&'
        url = url[:-1]
        url += f'&where=type:{local_cfg.hauling}'
        url += f'&where=loc_{do_type}:{local_cfg.loc_code}'
        headers = {"Authorization": f"Bearer {token}"}
        request = requests.get(url, headers=headers)
        if request.status_code == 200:
          data = json.loads(request.content)
          for val in data['items']: val['flag_sync'] = True # agar tidak bisa di sync ulang
        else: raise BadRequest400('Error getting data')
        # end online data
      else:
        data = browse(browse_queries=browse_queries, db=db, model=BaseModel)
      return data
    except Exception as e:
      raise BadRequest400(str(e))

  def get_id(id: int, db: Session):
    data = (
      db.query(BaseModel)
      .options(joinedload(BaseModel.truck))
      .filter(BaseModel.id == id)
      .filter(BaseModel.deleted_at == None)
      .first()
    )
    return data

  def delete(request: BulkId, id: int, db: Session, cred: str):
    return soft_delete(request=request, id=id, db=db, model=BaseModel, cred=cred, type = 'delete')

  def restore(request: BulkId, id: int, db: Session, cred: str):
    return soft_delete(request=request, id=id, db=db, model=BaseModel, cred=cred, type = 'restore')

  def reprint(id: int, is_online: bool, db: Session, token: str):
    try:
      if is_online:
        # online data
        url = statics['do'] + f'/{id}'
        headers = {"Authorization": f"Bearer {token}"}
        request = requests.get(url, headers=headers)
        if request.status_code == 200:
          return json.loads(request.content)
        else: raise BadRequest400('Error getting data')
        # end online data
      else:
        delivery_order = db.query(BaseModel).filter(BaseModel.id == id).first()
        if delivery_order:
          reprint = delivery_order.reprint + 1 if delivery_order.reprint else 1
          # print(f'reprint:{reprint}')
          db.query(BaseModel).filter(BaseModel.id == id).update({'reprint': reprint})
          db.commit()
          return db.query(BaseModel).filter(BaseModel.id == id).first()
        else:
          raise NotFound404(None, id)
    except IntegrityError as e:
      db.rollback()
      raise BadRequest400(str(e))
    return data

