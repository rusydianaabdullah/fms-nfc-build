from fastapi import APIRouter, Query, Depends
from typing import Optional
from sqlalchemy.orm import Session
from starlette.background import BackgroundTasks

from db.database import get_db
from app.auth.oauth2 import get_cred, get_token, check_permission

from utils.std_repo import BrowseQueries, BulkId, browse_query
from .repo import NfcRepo as repo
from .schema import NfcReq, NfcCfgReq, Schema, Display, DisplayRead

path = 'do'
router = APIRouter(prefix=f'/v1/{path}', tags=[path], dependencies=[])
permission = False


# CONFIG
@router.get('/nfc-config')
def get_nfc_config(
        weighing: Optional[bool] = Query(False, title="Get weighing data"),
        cctv: Optional[bool] = Query(False, title="Get CCTV status"),
        rfid: Optional[bool] = Query(False, title="Get RFID data"),
        opengate: Optional[bool] = Query(False, title="Open get manually"),
        closegate: Optional[bool] = Query(False, title="Close get manually"),
        loc: Optional[bool] = Query(False, title="Geot Loc Out & In"),
        db: Session = Depends(get_db)
    ):
    return repo.get_nfc_config(db, weighing, cctv, rfid, opengate, closegate, loc)

@router.post('/nfc-config')
def nfc_config( request: NfcCfgReq = Depends(), db: Session = Depends(get_db) ):
    return repo.nfc_config(request, db)

@router.post('/nfc-config/toggle-diff-rfid')
def nfc_toggle_diff_rfid(db: Session = Depends(get_db) ):
    return repo.toggle_diff_rfid(db)


@router.post('/nfc')
def nfc_post(
        bt: BackgroundTasks,
        request: NfcReq = Depends(),
        db: Session = Depends(get_db)
    ):
    return repo.nfc(request, db, bt)


@router.post('/post')
async def do_post(db: Session = Depends(get_db), token = Depends(get_token), limit: Optional[int] = None):
    return await repo.do_post(db, token, limit)


@router.post('/post/{id}')
def do_post(id: int, db: Session = Depends(get_db), token = Depends(get_token)):
    return repo.do_post_by_id(id, db, token)


# BROWSE
@router.get('') # response_model=Union[List[Display], Page[Display]]
def browse(
        browse_queries: BrowseQueries = Depends(browse_query),
        is_online: Optional[bool] = False,
        db: Session = Depends(get_db),
        cred = Depends(get_cred),
        token = Depends(get_token)
    ):
    if permission: check_permission(cred=cred, db=db, module=f'{path}-browse')
    return repo.get_index(browse_queries, is_online, db, token)

# READ
@router.get('/{id}') #response_model=Display
def browse_id(
        id: int,
        reprint: Optional[bool] = False,
        is_online: Optional[bool] = False,
        db: Session = Depends(get_db),
        cred = Depends(get_cred),
        token = Depends(get_token)
    ):
    if permission: check_permission(cred=cred, db=db, module=f'{path}-read')
    if reprint: return repo.reprint(id, is_online, db, token)
    else: return repo.get_id(id, db)

# CREATE
# @router.post('', response_model=Display)
# def create(
#         request: Schema,
#         db: Session = Depends(get_db),
#         cred = Depends(get_cred)
#     ):
#     if permission: check_permission(cred=cred, db=db, module=f'{path}-create')
#     return repo.create(request, db, cred)

# UPDATE
# @router.put('/{id}', response_model=DisplayRead)
# def update(
#         id: int,
#         request: Schema,
#         db: Session = Depends(get_db),
#         cred = Depends(get_cred)
#     ):
#     if permission: check_permission(cred=cred, db=db, module=f'{path}-update')
#     return repo.update(id, request, db, cred)

# DELETE & RESTORE
@router.delete('/delete')
def delete(
        request: BulkId,
        id: Optional[int] = None,
        db: Session = Depends(get_db),
        cred = Depends(get_cred)
    ):
    if permission: check_permission(cred=cred, db=db, module=f'{path}-delete')
    return repo.delete(request, id, db, cred)

@router.delete('/restore')
def restore(
        request: BulkId,
        id: Optional[int] = None,
        db: Session = Depends(get_db),
        cred = Depends(get_cred)
    ):
    if permission: check_permission(cred=cred, db=db, module=f'{path}-restore')
    return repo.restore(request, id, db, cred)

