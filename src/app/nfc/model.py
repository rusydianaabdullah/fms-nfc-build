import uuid

from db.database import Base
from sqlalchemy import Boolean, Column, DateTime, Date, Double, func
from sqlalchemy.sql.sqltypes import BigInteger, String, Integer, Text
from sqlalchemy.orm import relationship
from app.truck.model import Truck

class NfcConfig(Base):
  __tablename__ = 'nfc_config'

  id = Column(BigInteger, primary_key=True, index=True)
  type = Column(String, nullable=True)
  truck_code = Column(String, nullable=True)
  nik = Column(String, nullable=True)
  min_weight = Column(Integer, nullable=True)
  min_truck_tare = Column(Integer, nullable=True)
  lastlogin_user = Column(String, nullable=True)
  auto_print = Column(Boolean, nullable=True)
  thermal_print = Column(Boolean, nullable=True)
  diff_rfid = Column(Boolean, default=False)
  tare_max_day = Column(Integer, default=5)
  created_at = Column(DateTime, default=func.now())
  updated_at = Column(DateTime, default=func.now(), onupdate=func.now())
  deleted_at = Column(DateTime, nullable=True)

class DeliveryOrder(Base):
  __tablename__ = 'delivery_orders'

  id = Column(BigInteger, primary_key=True, index=True)
  flag_sync = Column(Boolean, default=False)
  type = Column(Integer, nullable=True)
  code = Column(String, nullable=True)
  vendor_code = Column(String, nullable=True)
  driver = Column(String, nullable=True)
  truck_code = Column(String, nullable=True)
  no_lambung = Column(String, nullable=True)
  owner_code = Column(String, nullable=True)
  pit_code = Column(String, nullable=True)
  seam_code = Column(String, nullable=True)
  reprint = Column(Integer, nullable=True, default=0)

  datetime_0 = Column(BigInteger, nullable=True)
  date_0 = Column(Date, nullable=True)
  rit_0 = Column(Integer, nullable=True)
  shift_0 = Column(String, nullable=True)
  user_0 = Column(String, nullable=True)
  wc_0 = Column(String, nullable=True)
  loc_0 = Column(String, nullable=True)
  bruto_0 = Column(Double, nullable=True)
  net_0 = Column(Double, nullable=True)
  rfid_0 = Column(String, nullable=True)
  nfc_0 = Column(String, nullable=True)
  abnormal_0 = Column(Text, nullable=True)
  cctv_0 = Column(Text, nullable=True)

  datetime_1 = Column(BigInteger, nullable=True)
  date_1 = Column(Date, nullable=True)
  rit_1 = Column(Integer, nullable=True)
  shift_1 = Column(String, nullable=True)
  user_1 = Column(String, nullable=True)
  wc_1 = Column(String, nullable=True)
  loc_1 = Column(String, nullable=True)
  bruto_1 = Column(Double, nullable=True)
  net_1 = Column(Double, nullable=True)
  rfid_1 = Column(String, nullable=True)
  nfc_1 = Column(String, nullable=True)
  abnormal_1 = Column(Text, nullable=True)
  cctv_1 = Column(Text, nullable=True)

  id_server = Column(BigInteger, nullable=True)

  created_by = Column(String, nullable=True)
  updated_by = Column(String, nullable=True)
  deleted_by = Column(String, nullable=True)
  created_at = Column(DateTime, default=func.now())
  updated_at = Column(DateTime, default=func.now(), onupdate=func.now())
  deleted_at = Column(DateTime, nullable=True)

  ready_sync = Column(Boolean, default=False)

  fillable = [
    'flag_sync',
    'type',
    'code',
    'truck_code',
    'no_lambung',
    'owner_code',
    'pit_code',
    'seam_code',
    'datetime_0',
    'date_0',
    'rit_0',
    'shift_0',
    'user_0',
    'wc_0',
    'loc_0',
    'bruto_0',
    'net_0',
    'rfid_0',
    'nfc_0',
    'datetime_1',
    'date_1',
    'rit_1',
    'shift_1',
    'user_1',
    'wc_1',
    'loc_1',
    'bruto_1',
    'net_1',
    'rfid_1',
    'nfc_1',
    'ready_sync'
  ]

  def as_dict(self):
    return {c.name: getattr(self, c.name) for c in self.__table__.columns}

  truck = relationship('Truck', foreign_keys=[truck_code], primaryjoin='Truck.code == DeliveryOrder.truck_code')

