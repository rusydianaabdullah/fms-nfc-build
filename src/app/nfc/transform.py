from .schema import NfcReq

class NfcTransform:
  def ask_write(
    request: NfcReq,
    response: dict,
    code1: str = '                ',
    code2: str = '                ',
    code3: str = '                '
  ):
    res = response
    res.update({'uid': request.uid})
    res.update({'device_id': request.device_id})
    res.update({'data1': '                '})
    res.update({'write1': 'false'})
    res.update({'data2': '                '})
    res.update({'write2': 'false'})

    res.update({'data3': code1})
    if code1 != '                ': res.update({'write3': 'true'})
    else: res.update({'write3': 'false'})

    res.update({'data4': code2})
    if code2 != '                ': res.update({'write4': 'true'})
    else: res.update({'write4': 'false'})

    res.update({'data5': code3})
    if code3 != '                ': res.update({'write5': 'true'})
    else: res.update({'write5': 'false'})

    res.update({'data6': '                '})
    res.update({'write6': 'false'})
    res.update({'data7': '                '})
    res.update({'write7': 'false'})
    res.update({'data8': '                '})
    res.update({'write8': 'false'})
    res.update({'data9': '                '})
    res.update({'write9': 'false'})
    res.update({'data10': '                '})
    res.update({'write10': 'false'})
    res.update({'data11': '                '})
    res.update({'write11': 'false'})
    res.update({'data12': '                '})
    res.update({'write12': 'false'})
    return res
