
  # def update_write_do(db: Session, lastdo: BaseModel, bt: BackgroundTasks):
  #   try:
  #     if lastdo.code.find('CLOSE_DO') > -1: return 'failed'
  #     cfg = db.query(NfcConfig).first()
  #     local_cfg = db.query(LocalConfig).first()
  #     id    = lastdo.id
  #     TYPE  = local_cfg.type if local_cfg else getenv('TYPE')
  #     RFID  = run_rfid(False)
  #     CCTV  = run_cctv(False)

  #     abnormal      = NfcHelper.get_abnormal(lastdo, local_cfg, RFID, db)
  #     lastloginuser = cfg.lastlogin_user if cfg.lastlogin_user else 'system'
  #     if TYPE == 'out': db.query(BaseModel).filter(BaseModel.id == id).update({
  #       BaseModel.abnormal_0: abnormal,
  #       BaseModel.rfid_0: RFID,
  #       BaseModel.user_0: lastloginuser,
  #       BaseModel.cctv_0: CCTV,
  #       BaseModel.deleted_at: None
  #     })
  #     else: db.query(BaseModel).filter(BaseModel.id == id).update({
  #       BaseModel.abnormal_1: abnormal,
  #       BaseModel.rfid_1: RFID,
  #       BaseModel.user_1: lastloginuser,
  #       BaseModel.cctv_1: CCTV,
  #       BaseModel.deleted_at: None
  #     })
  #     db.commit()
  #     if cfg.auto_print:
  #       send_mqtt(f'print:{id}')
  #     # if RFID == None or RFID == '':
  #     #   bt.add_task(NfcRepo.read_rfid, db, lastdo, local_cfg)
  #     # else:
  #     bt.add_task(NfcRepo.do_post_realtime, db, id)
  #     return 'ok'
  #   except Exception as e:
  #     ErrorLog.create(db, str(e), 'nfc_do')
  #     pass

  # def nfc_register_truck(request: NfcReq, db: Session):
  #   try:
  #     if (request.type == 'write'):  # Setelah NFC Write Data
  #       return {
  #         'status': request.status,
  #         'message': 'Write failed!' if request.status == 'failed' else 'Write success!'
  #       }
  #     else: # PERINTAHKAN WRITING
  #       data = db.query(NfcConfig).first()
  #       if data:
  #         return {
  #           'status'    : 'success',
  #           'message'   : f'writing {data.truck_code}',
  #           'uid'       : request.uid,
  #           'device_id' : request.device_id,
  #           'data1'     : data.nik,
  #           'write1'    : 'true',
  #           'data2'     : data.truck_code,
  #           'write2'    : 'true',
  #           'data3'     : '                ',
  #           'write3'    : 'false',
  #           'data4'     : '                ',
  #           'write4'    : 'false',
  #           'data5'     : '                ',
  #           'write5'    : 'false',
  #           'data6'     : '                ',
  #           'write6'    : 'false',
  #           'data7'     : '                ',
  #           'write7'    : 'false',
  #           'data8'     : '                ',
  #           'write8'    : 'false',
  #           'data9'     : '                ',
  #           'write9'    : 'false',
  #           'data10'    : '                ',
  #           'write10'   : 'false',
  #           'data11'    : '                ',
  #           'write11'   : 'false',
  #           'data12'    : '                ',
  #           'write12'   : 'false',
  #         }
  #       else:
  #         raise BadRequest400('Config data is unavailable!')
  #   except Exception as e:
  #     raise BadRequest400(e)

  # def read_rfid(db: Session, lastdo: BaseModel, local_cfg: LocalConfig):
  #   try:
  #     id        = lastdo.id
  #     TYPE      = local_cfg.type if local_cfg else getenv('TYPE')
  #     RFID      = continuous_run_rfid()
  #     abnormal  = NfcHelper.get_abnormal(lastdo, local_cfg, RFID, db)
  #     if TYPE == 'out': db.query(BaseModel).filter(BaseModel.id == id).update({
  #       BaseModel.abnormal_0: abnormal,
  #       BaseModel.rfid_0: RFID
  #     })
  #     else: db.query(BaseModel).filter(BaseModel.id == id).update({
  #       BaseModel.abnormal_1: abnormal,
  #       BaseModel.rfid_1: RFID
  #     })
  #     db.commit()
  #     NfcRepo.do_post_realtime(db, id)
  #     return 'read_rfid triggered'
  #   except:
  #     pass

