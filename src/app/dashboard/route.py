from fastapi import APIRouter, Query, Depends
from sqlalchemy.orm import Session
from app.auth.oauth2 import get_cred, get_token

from utils.std_repo import BrowseQueries, browse_query
from db.database import get_db

from .repo import DashboardRepo as repo

path = 'dashboard'
router = APIRouter(prefix=f'/v1/{path}', tags=[path], dependencies=[])
permission = False

@router.get('/summary-trips')
def summary_trips(shift: str, date: str, wc_code: str = None, date_time: str = None, db: Session = Depends(get_db)):
    return repo.summary_trips(shift, date, wc_code, date_time, db)

@router.get('/ongoing-trips')
def ongoing_trips(browse_queries: BrowseQueries = Depends(browse_query), last_do_temp_update: bool = False, db: Session = Depends(get_db)):
    return repo.get_index_do_temps(browse_queries, last_do_temp_update, db)

@router.get('/sync-diff')
def sync_difference(shift: str, date: str, token = Depends(get_cred), db: Session = Depends(get_db)):
    return repo.sync_diff(shift, date, token, db)
