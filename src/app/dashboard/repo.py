from os import getenv, path
import requests
import json

from sqlalchemy.orm import Session
from sqlalchemy import func
from sqlalchemy.sql import case
from typing import List

from utils.exceptions import BadRequest400
from utils.std_repo import BrowseQueries, browse
from utils.helper import date as date_helper
from ..auth import oauth2

from ..nfc.model import DeliveryOrder, NfcConfig
from ..local_config.model import LocalConfig
from ..weighing.model import DeliveryOrderTemp

class DashboardRepo:
    def summary_trips(shift: str, date: str, wc_code: str, date_time: str, db: Session):
        try:               
            local_cfg = db.query(LocalConfig).first()
            TYPE = local_cfg.type if local_cfg else getenv('TYPE')     
            LOC_CODE = local_cfg.loc_code if local_cfg else getenv('LOC_CODE')     

            expires_delta   = date_helper.timedelta(30)
            token           = oauth2.create_access_token(data = {'sub': 'superuser'}, expires_delta=expires_delta)
            headers         = {"Authorization": f"Bearer {token}"}
            # endpoint        = f"https://fms-api2.bmbcoal.com/v1/dashboard/summary-trips?shift={shift}&date={date}&type={TYPE}&loc_code={LOC_CODE}"
            endpoint        = f"https://fms-api.bmbcoal.com/v1/dashboard/trip?ver=2&shift={shift}&date={date}&type={TYPE}&loc_code={LOC_CODE}"
            if wc_code: endpoint    += f"&wc_code={wc_code}"
            if date_time: endpoint  += f"&date_time={date_time}"
            
            res             = requests.get(endpoint, headers=headers, stream=True)
            res             = json.loads(res.content)        
            return res
        except Exception as e:
            raise BadRequest400(str(e))

    def get_index_do_temps(browse_queries: BrowseQueries, last_do_temp_update: bool, db: Session):
        if last_do_temp_update: 
            res = None
            parent_folder = 'src/cctv'
            file_name = 'last_do_temp_update.txt'
            file_path = path.join(parent_folder, file_name)
            if path.exists(parent_folder) and path.exists(file_path): 
                with open(file=file_path, mode="r", encoding='utf8') as f: res = f.read()
            data = res if res else '-'
        else: data = browse(browse_queries=browse_queries, db=db, model=DeliveryOrderTemp)
        return data

    # SYNC DIFFERENCE
    def sync_diff(shift: str, date: str, token: str, db: Session):
        local_cfg = db.query(LocalConfig).first()
        loc_code = local_cfg.loc_code if local_cfg else 'CP8'
        type_ = '_0' if local_cfg.type == 'out' else '_1'

        headers = {"Authorization": f"Bearer {token}"}
        url     = 'https://fms-api2.bmbcoal.com/v1/transaction/delivery-orders/codes?limit=0'        
        url    += f'&where=date{type_}:{date}'
        url    += f'&where=loc{type_}:{loc_code}'
        if shift != 'all':
            url    += f'&where=shift{type_}:{shift}'

        if local_cfg.wc_code:
            url+= f'&where=wc{type_}:{local_cfg.wc_code}'
        elif local_cfg.prefix:
            url+= f'&like=code:{local_cfg.prefix}'
            
        doapi   = requests.get(url, headers=headers)
        codes = []
        if doapi.status_code == 200:
            codes = json.loads(doapi.content)
        do_local = DashboardRepo.local_na(local_cfg, codes, shift, date, db)
        do_server = DashboardRepo.server_na(local_cfg, codes, shift, date, db)

        return {'do_local': do_local, 'do_server': do_server}

    def local_na(local_cfg: LocalConfig, codes: List, shift: str, date: str, db: Session):
        do_local = []
        if local_cfg.type == 'out':
            qry = db.query(DeliveryOrder.code, DeliveryOrder.truck_code).filter(
                    DeliveryOrder.date_0 == date
                ).filter(
                    DeliveryOrder.shift_0 == shift
                ).filter(
                    DeliveryOrder.code.notin_(codes)
                ).filter(
                    DeliveryOrder.deleted_at == None
                ).all()
        else:
            qry = db.query(DeliveryOrder.code, DeliveryOrder.truck_code).filter(
                    DeliveryOrder.date_1 == date
                ).filter(
                    DeliveryOrder.shift_1 == shift
                ).filter(
                    DeliveryOrder.code.notin_(codes)
                ).filter(
                    DeliveryOrder.deleted_at == None
                ).all()
        if qry:
            for v in qry:
                do_local.append({'code': v.code, 'truck_code': v.truck_code, 'no_lambung': ''})
        return do_local

    def server_na(local_cfg: LocalConfig, codes: List, shift: str, date: str, db: Session):
        do_server = []
        for code in codes:
            check = db.query(DeliveryOrder.id).filter(DeliveryOrder.code == code).filter(DeliveryOrder.deleted_at == None).first()
            if check:
                ''
            else:
                do_server.append({'code': code, 'truck_code': '', 'no_lambung': ''})
        return do_server
