from pydantic import BaseModel
from typing import List, Optional
import datetime

class Display(BaseModel):
  id: int
  flag_sync: Optional[bool] = False
  company_code: Optional[str] = None
  truck_code: Optional[str] = None
  value: Optional[float] = None
  date: Optional[int] = None
