import uuid

from db.database import Base
from sqlalchemy import Boolean, Column, DateTime, String, Text, func
from sqlalchemy.sql.sqltypes import BigInteger, String, Double
from sqlalchemy.orm import relationship

class Truck(Base):
  __tablename__ = 'trucks'

  id_ = Column(BigInteger, primary_key=True, index=True)
  id = Column(BigInteger, unique=True, nullable=True)
  flag_sync = Column(Boolean, default=False)
  code = Column(String, nullable=True)
  code_1 = Column(String, nullable=True)
  name = Column(String, nullable=True)
  company_code = Column(String, nullable=True)
  rfid_code = Column(String, nullable=True)
  status = Column(String, nullable=True)
  created_at = Column(DateTime, default=func.now())
  updated_at = Column(DateTime, default=func.now(), onupdate=func.now())
  deleted_at = Column(DateTime, nullable=True)


class TruckTare(Base):
  __tablename__ = 'truck_tares'

  id_ = Column(BigInteger, primary_key=True, index=True)
  id = Column(BigInteger, unique=False)
  flag_sync = Column(Boolean, default=False)
  truck_id = Column(BigInteger, nullable=True)
  wc_id = Column(BigInteger, nullable=True)
  truck_code = Column(String, nullable=True)
  wc_code = Column(String, nullable=True)
  value = Column(Double, nullable=True)
  date = Column(BigInteger, nullable=True)
  created_at = Column(DateTime, default=func.now())
  updated_at = Column(DateTime, default=func.now(), onupdate=func.now())
  deleted_at = Column(DateTime, nullable=True)


class WeighingCenter(Base):
  __tablename__ = 'weighing_centers'

  id_ = Column(BigInteger, primary_key=True, index=True)
  id = Column(BigInteger, unique=True)
  flag_sync = Column(Boolean, default=False)
  code = Column(String, nullable=True)
  prefix = Column(String, nullable=True)
  created_at = Column(DateTime, default=func.now())
  updated_at = Column(DateTime, default=func.now(), onupdate=func.now())
  deleted_at = Column(DateTime, nullable=True)
