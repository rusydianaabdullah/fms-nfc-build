from fastapi import APIRouter, Depends
from typing import Optional
from sqlalchemy.orm import Session

from db.database import get_db
from app.auth.oauth2 import get_cred, get_token

from utils.std_repo import BrowseQueries, BulkId, browse_query
from .repo import TruckRepo as repo
from .schema import Display

path = 'tares'
router = APIRouter(prefix=f'/v1/{path}', tags=[path], dependencies=[])

# BROWSE
@router.get('') # response_model=Union[List[Display], Page[Display]]
def browse(
        browse_queries: BrowseQueries = Depends(browse_query),
        db: Session = Depends(get_db),
        cred = Depends(get_cred)
    ):
    return repo.get_index(browse_queries, db)

@router.get('/truck')
def browse(
        browse_queries: BrowseQueries = Depends(browse_query),
        db: Session = Depends(get_db),
        cred = Depends(get_cred)
    ):
    return repo.get_index_truck(browse_queries, db)

@router.get('/truck_by_code')
def get_by_code(
        code: str = None,
        db: Session = Depends(get_db)
    ):
    return repo.get_by_code(code, db)

# SYNC
@router.post('/get')
def get_from_cloud(
        db: Session = Depends(get_db),
        cred = Depends(get_token)
    ):
    return repo.get_from_cloud(db, cred)

@router.post('/post')
def post_to_cloud( db: Session = Depends(get_db), token = Depends(get_token), limit: Optional[int] = None ):
    return repo.post_to_cloud(db, token, limit)

# DELETE & RESTORE
@router.delete('/delete')
def delete(
        request: BulkId,
        id: Optional[int] = None,
        db: Session = Depends(get_db),
        cred = Depends(get_cred)
    ):
    # check_permission(cred=cred, db=db, module=f'{path}-delete')
    return repo.delete(request, id, db, cred)

@router.delete('/restore')
def restore(
        request: BulkId,
        id: Optional[int] = None,
        db: Session = Depends(get_db),
        cred = Depends(get_cred)
    ):
    # check_permission(cred=cred, db=db, module=f'{path}-restore')
    return repo.restore(request, id, db, cred)
