import requests
import json
from fastapi.encoders import jsonable_encoder
from config import getenv, statics
from sqlalchemy import func, column, or_
from sqlalchemy.orm import Session
from utils.exceptions import BadRequest400
from utils.std_repo import BrowseQueries, BulkId, browse, soft_delete
from utils.mqtt import send_mqtt
# from utils.rfid import run_rfid
from utils.cctv import run_cctv
from utils.weighing import run_weighing
from utils.helper import date

from .model import Truck, TruckTare, WeighingCenter
from ..local_config.model import LocalConfig
from ..cctv.model import Cctv
from ..nfc.model import NfcConfig
from ..nfc.schema import NfcReq

class TruckRepo:
  def nfc_tare(request: NfcReq, db: Session):
    try:
      nfc_cfg = db.query(NfcConfig).first()
      if (request.type == 'write'):
        if (request.status == 'failed'):
          last = db.query(TruckTare.id_).filter(TruckTare.deleted_at == None).order_by(TruckTare.id_).limit(1).first()
          db.query(Cctv).filter(Cctv.tare_id == last.id_).update({Cctv.deleted_at: func.now()})
          db.commit()
        res = {
          'status': request.status,
          'message': 'Write failed!' if request.status == 'failed' else 'Write success!'
        }
        send_mqtt(res['message'])
        return res
      else:
        weigh = run_weighing()
        if int(weigh) > nfc_cfg.min_truck_tare:
          res = {
            'status': 'failed',
            'message': f'Berat > {nfc_cfg.min_truck_tare} Kg, Apakah anda yakin akan melakukan tera truk?'
          }
          send_mqtt(res['message'])
        else:
          res = TruckRepo.save_tare(request=request, db=db)
        return res
    except Exception as e:
      db.rollback()
      return {'status': 'failed', 'message': str(e)}

  def save_tare(request: NfcReq, db: Session):
    local_cfg = db.query(LocalConfig).first()
    if local_cfg:
      wc_code = local_cfg.wc_code
      vendortruck = request.data2
      # vendor_code = ''
      truck_code  = ''
      try:
        sp = vendortruck.split('_')
        # if sp[0]: vendor_code = sp[0]
        if sp[1]: truck_code = sp[1]
      except:
        pass
      truck = db.query(Truck.id).filter(Truck.code == truck_code).first()
      truck_id = truck.id
      weigh = run_weighing()
      tare = TruckTare(
        wc_code=wc_code,
        truck_code=truck_code,
        truck_id=truck_id,
        value=weigh,
        date=date.timemillis()
      )
      db.add(tare)
      db.commit()
      db.refresh(tare)
      TruckRepo.save_cctv(db, tare)
      return {
        'status': 'success',
        'message': f'{weigh}Kg | Tare'
      }
    else:
      return {
        'status': 'failed',
        'message': 'No local config!'
      }

  def save_cctv(db: Session, tare):
    truck_code = tare.truck_code
    filename = f'Tare {truck_code}'
    data = Cctv()
    data.module     = 'master/truck-tares'
    data.name       = filename
    data.filename   = f'{filename}.jpg'
    data.base64     = run_cctv()
    data.flag_sync  = False
    data.description= tare.wc_code
    data.tare_id    = tare.id_
    try:
      db.add(data)
      db.commit()
      db.refresh(data)
      return data
    except Exception as e:
      db.rollback()
      raise BadRequest400(str(e))

  ####### SYNC #######
  def get_from_cloud(db: Session, cred: str):
    local_cfg = db.query(LocalConfig).first()
    headers = {"Authorization": f"Bearer {cred}"}
    wcapi   = requests.get('https://fms-api.bmbcoal.com/v1/master/weighing-centers?limit=0&order=id:asc', headers=headers, stream=True)
    if wcapi.status_code == 200:
      wc      = TruckRepo.getWc(res=wcapi, db=db)
    truckapi= requests.get('https://fms-api.bmbcoal.com/v1/master/trucks?limit=0&like=status:1&order=id:asc', headers=headers, stream=True)
    if truckapi.status_code == 200:
      truck   = TruckRepo.getTruck(res=truckapi, db=db)
    wc1 = local_cfg.wc_code if local_cfg else getenv('WC_CODE')
    wc2 = local_cfg.wc_code_1 if local_cfg else getenv('WC_CODE1')
    tares   = None
    if wc1:
      wc_ids  = ''
      wcs     = db.query(WeighingCenter.id).filter(WeighingCenter.code.in_([wc1,wc2])).all()
      i = 0
      for v in wcs:
        wc_ids += f'{v.id}' if i == 0 else f',{v.id}'
        i += 1
      tareapi= requests.get(f'https://fms-api.bmbcoal.com/v1/master/truck-tares?limit=0&order=id:asc&in=wc_id:{wc_ids}', headers=headers, stream=True)
      if tareapi.status_code == 200:
        tares   = TruckRepo.getTare(res=tareapi, db=db)
    return {'wc': wc, 'truck': truck, 'tares': tares}

  def getWc(res, db: Session):
    lists   = json.loads(res.content)
    if lists['data'] is None:
      return lists
    insert  = 0
    update  = 0
    for v in lists['data']:
      try:
        check = db.query(WeighingCenter).filter(WeighingCenter.id == v['id']).first()
        if check:
          db.query(WeighingCenter).filter(WeighingCenter.id == v['id']).update({
            'id': v['id'],
            'code': v['code'],
            'prefix': v['division'],
            'flag_sync': True
          })
          update = update + 1
        else:
          row = WeighingCenter()
          row.id = v['id']
          row.code = v['code']
          row.prefix = v['division']
          row.flag_sync = True
          db.add(row)
          insert = insert + 1
        db.commit()
      except Exception as e:
        raise BadRequest400(str(e))
        # db.rollback()
        pass
    return {'insert': insert, 'update': update}

  def getTruck(res, db:Session):
    lists   = json.loads(res.content)
    if lists['data'] is None:
      return lists
    insert  = 0
    update  = 0
    ids = []
    for v in lists['data']:
      try:
        check = db.query(Truck).filter(Truck.id == v['id']).first()
        if check:
          db.query(Truck).filter(Truck.id == v['id']).update({
            'flag_sync': True,
            'code': v['code'],
            'code_1': v['code_1'],
            'name': v['name'],
            'company_code': v['company_code'],
            'rfid_code': v['rfid_code'],
            'status': v['status']
          })
          update = update + 1
          ids.append(check.id)
        else:
          row = Truck()
          row.id            = v['id']
          row.code          = v['code']
          row.code_1        = v['code_1']
          row.name          = v['name']
          row.company_code  = v['company_code']
          row.rfid_code     = v['rfid_code']
          row.status        = v['status']
          row.flag_sync = True
          db.add(row)
          insert = insert + 1
          ids.append(row.id)
        db.commit()
      except Exception as e:
        raise BadRequest400(str(e))
        # db.rollback()
        pass
    # Delete local truck where id is na
    db.query(Truck).filter(Truck.id.notin_(ids)).delete()
    db.commit()
    return {'insert': insert, 'update': update}

  def getTare(res, db:Session):
    lists   = json.loads(res.content)
    if lists['data'] is None:
      return lists

    # Delete local truck tare with value 0 and id server is null
    db.query(TruckTare).filter(or_(TruckTare.value == 0, TruckTare.value > 26000)).delete()

    insert  = 0
    update  = 0

    db.query(TruckTare).filter(TruckTare.flag_sync == True).delete()

    for v in lists['data']:
      try:
        check = db.query(TruckTare).filter(TruckTare.id == v['id']).first()
        truck_code = db.query(Truck.code).filter(Truck.id == v['truck_id']).value(column('code'))
        wc_code = db.query(WeighingCenter.code).filter(WeighingCenter.id == v['wc_id']).value(column('code'))
        if check:
          db.query(TruckTare).filter(TruckTare.id == v['id']).update({
            'flag_sync': True,
            'truck_id': v['truck_id'],
            'wc_id': v['wc_id'],
            'truck_code': truck_code,
            'wc_code': wc_code,
            'value': v['value'],
            'date': v['date']
          })
          update = update + 1
        else:
          row = TruckTare()
          row.id        = v['id']
          row.truck_id  = v['truck_id']
          row.wc_id     = v['wc_id']
          row.truck_code= truck_code
          row.wc_code   = wc_code
          row.value     = v['value']
          row.date      = v['date']
          row.flag_sync = True
          db.add(row)
          insert = insert + 1
        db.commit()
      except Exception as e:
        raise BadRequest400(str(e))
        # db.rollback()
        pass
    return {'insert': insert, 'update': update}

  def post_to_cloud(db: Session, token: str, limit = None):
    data = db.query(TruckTare).filter(TruckTare.deleted_at == None).filter(TruckTare.flag_sync == False)
    data = data.order_by(TruckTare.id_)
    if limit:
      data = data.limit(limit)
    data = data.all()
    success = 0
    failed  = 0
    jsons = jsonable_encoder(data)
    for row in jsons:
      r = TruckRepo.post_each(db, row, token)
      if r == 'success': success = success + 1
      if r == 'failed': failed = failed + 1
    return {'message': f'success: {success}, failed: {failed}'}

  def post_each(db: Session, row: dict, token: str):
    try:
      url     = statics['tare']
      headers = {"Authorization": f"Bearer {token}"}
      reqbody = {
        'wc_code': row['wc_code'],
        'truck_id': row['truck_id'],
        'value': int(row['value']),
        'date': row['date']
      }
      request = requests.post(url, headers=headers, json=reqbody)
      if request.status_code == 200 and request.content:
        data = json.loads(request.content)
        if data['data'] and data['data'][0]:
          res_id = data['data'][0]
          print(res_id)
          db.query(TruckTare).filter(TruckTare.id_ == row['id_']).update({TruckTare.id: res_id, TruckTare.flag_sync: True})
          db.query(Cctv).filter(Cctv.tare_id == row['id_']).update({Cctv.module_id: res_id})
          db.commit()
        return 'success'
      else:
        print('error: ', request.content)
        return 'failed'
    except Exception as e:
      # db.rollback()
      print(e)
      return 'failed'

  ####### DEFAULT #######
  def get_index_truck(browse_queries: BrowseQueries, db: Session):
    data = browse(browse_queries=browse_queries, db=db, model=Truck)
    return data

  def get_index(browse_queries: BrowseQueries, db: Session):
    data = browse(browse_queries=browse_queries, db=db, model=TruckTare)
    return data
  
  def get_by_code(code: str, db: Session):
    data = db.query(Truck.code, Truck.name, Truck.company_code).filter(Truck.code == code).first()
    return {'code': data.code, 'name': data.name, 'company_code': data.company_code}

  def delete(request: BulkId, id: int, db: Session, cred: str):
    return soft_delete(request=request, id=id, db=db, model=TruckTare, cred=cred, type = 'delete')


  def restore(request: BulkId, id: int, db: Session, cred: str):
    return soft_delete(request=request, id=id, db=db, model=TruckTare, cred=cred, type = 'restore')
