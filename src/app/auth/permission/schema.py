from pydantic import BaseModel
from typing import List, Optional
import datetime


class Schema(BaseModel):
  name: str
  app: Optional[str] = ''


class Display(BaseModel):
  id: int
  name: str
  app: Optional[str] = ''

class DisplayRead(Display):
  created_by: Optional[str]
  created_at: Optional[datetime.datetime]
  updated_by: Optional[str]
  updated_at: Optional[datetime.datetime]
  deleted_by: Optional[str]
  deleted_at: Optional[datetime.datetime]
