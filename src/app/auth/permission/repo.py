from sqlalchemy.orm import Session
from sqlalchemy.exc import IntegrityError
from utils.exceptions import BadRequest400, NotFound404
from utils.std_repo import BrowseQueries, BulkId, browse, get_by_id, soft_delete

from .schema import Schema
from .model import Permission as BaseModel

class PermissionRepo:
  def create(request: Schema, db: Session, cred: str):
    data = BaseModel()
    for index in BaseModel.fillable:
      try: setattr(data, index, getattr(request, index))
      except: None
    setattr(data, 'created_by', cred)
    try:
      db.add(data)
      db.commit()
      db.refresh(data)
      return data
    except Exception as e:
      db.rollback()
      raise BadRequest400(str(e))


  def update(id: int, request: Schema, db: Session, cred: str):
    try:
      if db.query(BaseModel.id).filter(BaseModel.id == id).first():
        updates = {'updated_by': cred}
        for index in BaseModel.fillable:
          if hasattr(request, index):
            updates.update({index: getattr(request, index)})
        data = db.query(BaseModel).filter(BaseModel.id == id).update(updates)
        db.commit()
        data = db.query(BaseModel).filter(BaseModel.id == id).first()
        return data
      else:
        raise NotFound404(None, id)
    except IntegrityError as e:
      db.rollback()
      raise BadRequest400(str(e))


  def get_index(browse_queries: BrowseQueries, db: Session):
    data = browse(browse_queries=browse_queries, db=db, model=BaseModel)
    return data


  def get_id(id: int, db: Session):
    data = get_by_id(id=id, db=db, model=BaseModel)
    return data


  def delete(request: BulkId, id: int, db: Session, cred: str):
    return soft_delete(request=request, id=id, db=db, model=BaseModel, cred=cred, type = 'delete')


  def restore(request: BulkId, id: int, db: Session, cred: str):
    return soft_delete(request=request, id=id, db=db, model=BaseModel, cred=cred, type = 'restore')
