from fastapi import APIRouter, Depends
from typing import Optional
from sqlalchemy.orm import Session

from db.database import get_db
from app.auth.oauth2 import get_cred, check_permission

from utils.std_repo import BrowseQueries, BulkId, browse_query
from .repo import PermissionRepo as repo
from .schema import Schema, Display, DisplayRead

path = 'auth/permissions'
router = APIRouter(prefix=f'/v1/{path}', tags=[path], dependencies=[])

# BROWSE
@router.get('')
def browse(
        browse_queries: BrowseQueries = Depends(browse_query),
        db: Session = Depends(get_db),
        cred = Depends(get_cred)
    ):
    check_permission(cred=cred, db=db, module=f'{path}-browse')
    return repo.get_index(browse_queries, db)

# READ
@router.get('/{id}', response_model=Display)
def browse_id(
        id: int,
        db: Session = Depends(get_db),
        cred = Depends(get_cred)
    ):
    check_permission(cred=cred, db=db, module=f'{path}-read')
    return repo.get_id(id, db)

# CREATE
@router.post('', response_model=Display)
def create(
        request: Schema,
        db: Session = Depends(get_db),
        cred = Depends(get_cred)
    ):
    check_permission(cred=cred, db=db, module=f'{path}-create')
    return repo.create(request, db, cred)

# UPDATE
@router.put('/{id}', response_model=DisplayRead)
def update(
        id: int,
        request: Schema,
        db: Session = Depends(get_db),
        cred = Depends(get_cred)
    ):
    check_permission(cred=cred, db=db, module=f'{path}-update')
    return repo.update(id, request, db, cred)

# DELETE & RESTORE
@router.delete('/delete')
def delete(
        request: BulkId,
        id: Optional[int] = None,
        db: Session = Depends(get_db),
        cred = Depends(get_cred)
    ):
    check_permission(cred=cred, db=db, module=f'{path}-delete')
    return repo.delete(request, id, db, cred)

@router.delete('/restore')
def restore(
        request: BulkId,
        id: Optional[int] = None,
        db: Session = Depends(get_db),
        cred = Depends(get_cred)
    ):
    check_permission(cred=cred, db=db, module=f'{path}-restore')
    return repo.restore(request, id, db, cred)
