import uuid

from db.database import Base
from sqlalchemy import Boolean, Column, DateTime, String, func
from sqlalchemy.sql.sqltypes import BigInteger, String
from sqlalchemy.orm import relationship

class Permission(Base):
  __tablename__ = 'permissions'

  id = Column(BigInteger, primary_key=True, index=True)
  app = Column(String, nullable=True)
  name = Column(String, unique=True)
  created_by = Column(String, nullable=True)
  updated_by = Column(String, nullable=True)
  deleted_by = Column(String, nullable=True)
  created_at = Column(DateTime, default=func.now())
  updated_at = Column(DateTime, default=func.now(), onupdate=func.now())
  deleted_at = Column(DateTime, nullable=True)

  fillable = [
    "app",
    "name"
  ]

  searchable = [
    "name",
    "created_by",
    "updated_by",
    "deleted_by"
  ]