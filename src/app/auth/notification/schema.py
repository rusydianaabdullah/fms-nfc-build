from pydantic import BaseModel, EmailStr
from typing import Optional, List
from datetime import datetime


class Schema(BaseModel):
  username: Optional[str]
  user_id: Optional[int]
  is_read: Optional[bool] = False
  app: Optional[str]
  title: Optional[str]
  description: Optional[str]
  path: Optional[str]
  type: Optional[str]
  color: Optional[str]
  icon: Optional[str]


class Display(BaseModel):
  id: int
  username: Optional[str]
  user_id: Optional[int]
  is_read: bool = False
  app: Optional[str]
  title: Optional[str]
  description: Optional[str]
  path: Optional[str]
  type: Optional[str]
  color: Optional[str]
  icon: Optional[str]
  created_by: Optional[str]
  created_at: Optional[datetime]
  updated_by: Optional[str]
  updated_at: Optional[datetime]
  deleted_by: Optional[str]
  deleted_at: Optional[datetime]

