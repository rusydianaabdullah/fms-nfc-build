from sqlalchemy import Boolean, Column, DateTime, ForeignKey, String, Text, func
from sqlalchemy.orm import relationship
from sqlalchemy.sql.sqltypes import BigInteger, Integer, String, Text
from db.database import Base

class Notification(Base):
  __tablename__ = "notifications"
  # __table_args__ = {"schema": "auth"}

  id = Column(BigInteger, primary_key=True, index=True)
  username = Column(String, nullable=True)
  user_id = Column(BigInteger, nullable=True)
  is_read = Column(Boolean, default=False)
  app = Column(String, nullable=True)
  title = Column(String, nullable=True)
  description = Column(Text, nullable=True)
  path = Column(String, nullable=True)
  type = Column(String, nullable=True)
  color = Column(String, nullable=True)
  icon = Column(String, nullable=True)

  # role = relationship("Role", back_populates="users") # primaryjoin="User.role_id == Role.id" # BelongsTo
  # users = relationship("User", back_populates="role", cascade="all, delete-orphan") # HasMany

  created_by = Column(String, nullable=True)
  updated_by = Column(String, nullable=True)
  deleted_by = Column(String, nullable=True)
  created_at = Column(DateTime, default=func.now())
  updated_at = Column(DateTime, default=func.now(), onupdate=func.now())
  deleted_at = Column(DateTime, nullable=True)

  fillable = [
    "username",
    "user_id",
    "is_read",
    "app",
    "title",
    "description",
    "path",
    "type",
    "color",
    "icon",
  ]
