import os
from fastapi.param_functions import Depends
from fastapi.security import OAuth2PasswordBearer, OAuth2
from fastapi.security import HTTPBearer
from typing import Optional
from datetime import datetime, timedelta
from jose import jwt
from jose.exceptions import JWTError
from sqlalchemy.orm import Session
from fastapi import HTTPException, status
from sqlalchemy import select

from utils.exceptions import Unauthorized401, Forbidden403
from app.auth.user.model import User
from app.auth.role.model import Role
from db.database import get_db
from app import schemas

# TOKEN_SCHEMA = os.environ.get('TOKEN_SCHEMA', 'password')
# if TOKEN_SCHEMA == 'token': oauth2_scheme = HTTPBearer()
# else:
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="v1/token")

SECRET_KEY = 'YmF0YWtndW1icmVuZw'
ALGORITHM = "HS256"
JWT_EXPIRED_MINUTE = 1000*30*24*60

def create_access_token(data: dict, expires_delta: Optional[timedelta] = None):
    to_encode = data.copy()
    if expires_delta:
        expire = datetime.utcnow() + expires_delta
    else:
        expire = datetime.utcnow() + timedelta(minutes=JWT_EXPIRED_MINUTE)
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(to_encode, SECRET_KEY, algorithm=ALGORITHM)
    return encoded_jwt


########## GET USERNAME FROM JWT ##########
def get_cred(creds = Depends(oauth2_scheme)):
    token = creds
    try:
        payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
        username: str = payload.get("sub")
        if username is None:
            raise Unauthorized401()
    except JWTError:
        raise Unauthorized401()
    return username

def get_token(creds = Depends(oauth2_scheme)):
    return creds

########## CHECK MODULE PERMISSION ##########
def check_permission(cred: str, db: Session = Depends(get_db), module: str = None):
    # forbidden = HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail="You don't have permission to do this", headers={"WWW-Authenticate": "Bearer"})
    if module == None:
        raise Forbidden403

    try:
        user = db.query(User.role_id).filter(User.username == cred).first()
        perms = db.query(Role.permissions).filter(Role.id == user.role_id).first()
        permissions = perms.permissions

        prefix = ''
        feat = ''
        mods = ['browse', 'read', 'create', 'update', 'delete', 'restore']
        for m in mods:
            if module.find(m) > -1:
                feat = m
                prefix = module.replace(f'-{m}' , '')

        for v in permissions:
            if v.get('name') == prefix:
                check = v.get('detail')     # print('check', check)
                result = check.get(feat)    # print('result', result)
                if result == True:
                    return True
                else:
                    raise Forbidden403
        # if result == False:
        raise Forbidden403
    except Exception as e:
        print(e)
        raise Forbidden403
        # raise HTTPException(
        #     status_code=status.HTTP_400_BAD_REQUEST,
        #     message=str(e),
        #     headers={"WWW-Authenticate": "Bearer"}
        # )
