import os
from typing import Union, Optional
from typing_extensions import Annotated
from fastapi.param_functions import Form
from fastapi import APIRouter
from fastapi.param_functions import Depends
# from fastapi.security.oauth2 import OAuth2PasswordRequestForm
from sqlalchemy.orm.session import Session
from db.database import get_db
from utils.hash import Hash
from app.auth.user.model import User
from app.nfc.model import NfcConfig
from app.auth import oauth2
from utils.exceptions import NotFound404
from utils.helper import date
from datetime import timedelta
# from pydantic import BaseModel

router = APIRouter(
  tags=['authentication']
)

JWT_EXPIRED_MINUTE = 1000*30*24*60


class LoginForm:
  def __init__(
    self,
    *,
    username: Annotated[str,Form()],
    password: Annotated[str,Form()],
    remember_me: Annotated[bool,Form()] = False,
  ):
    self.username = username
    self.password = password
    self.remember_me = remember_me


# OAuth2PasswordRequestForm
@router.post('/v1/token')
def get_token(request: LoginForm = Depends(), db: Session = Depends(get_db)):
  expiry = JWT_EXPIRED_MINUTE

  user = db.query(User).filter(User.username == request.username).first()
  if not user:
    raise NotFound404('Invalid credentials')
  if not Hash.verify(user.password, request.password):
    raise NotFound404('Incorrect password')

  expires_delta = timedelta(days=365)
  access_token = oauth2.create_access_token(data = {'sub': user.username}, expires_delta=expires_delta)
  expire = date.utcnow() + expires_delta
  milliseconds = date.timemillis_from_date(expire)

  # STORE LAST USERNAME TO FIELD NIK
  store_username(request.username, db)

  return {
    'access_token': access_token,
    'token_type': 'bearer',
    'user_id': user.id,
    'username': user.username,
    'token_expire': milliseconds
  }


def store_username(username: str, db: Session):
  try:
    check = db.query(NfcConfig).first()
    if check:
      db.query(NfcConfig).update({NfcConfig.lastlogin_user: username})
    else:
      data = NfcConfig()
      data.lastlogin_user = username
      db.add(data)
    db.commit()
    return 'ok'
  except:
    pass
