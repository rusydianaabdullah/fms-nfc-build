import uuid

from sqlalchemy import Boolean, Column, DateTime, ForeignKey, String, func
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.postgresql import JSONB
from sqlalchemy.sql.sqltypes import BigInteger, String

from db.database import Base


class Role(Base):
    __tablename__ = "roles"

    id = Column(BigInteger, primary_key=True, index=True)
    app = Column(String, unique=True, nullable=False)
    name = Column(String, unique=True, nullable=False)
    permissions = Column(JSONB, nullable=True)

    created_by = Column(String(), nullable=True)
    updated_by = Column(String(), nullable=True)
    deleted_by = Column(String(), nullable=True)

    created_at = Column(DateTime, default=func.now())
    updated_at = Column(DateTime, default=func.now(), onupdate=func.now())
    deleted_at = Column(DateTime, nullable=True)

    users = relationship("User", back_populates="role", cascade="all, delete-orphan")
    # users = relationship("User", backref="role", primaryjoin="User.role_id == Role.id", cascade="all, delete-orphan")

    fillable = [
        "app",
        "name",
        "permissions"
    ]
