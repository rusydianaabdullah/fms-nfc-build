from pydantic import BaseModel
from typing import List, Optional
import datetime


class Schema(BaseModel):
  name: str
  app: Optional[str] = None
  permissions: List = [
    {
      "id": 1,
      "name": "auth/users",
      "detail": {"browse": True, "read": True, "create": True, "update": True, "delete": True, "restore": True}
    },
    {
      "id": 2,
      "name": "auth/roles",
      "detail": {"browse": True, "read": True, "create": True, "update": True, "delete": True, "restore": True}
    },
    {
      "id": 3,
      "name": "auth/permissions",
      "detail": {"browse": True, "read": True, "create": True, "update": True, "delete": True, "restore": True}
    }
  ]


class Display(BaseModel):
  id: int
  app: Optional[str] = None
  name: Optional[str] = None
  permissions: List = []
  created_by: Optional[str]
  created_at: Optional[datetime.datetime]
  updated_by: Optional[str]
  updated_at: Optional[datetime.datetime]
  deleted_by: Optional[str]
  deleted_at: Optional[datetime.datetime]

