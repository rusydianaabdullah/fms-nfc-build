from fastapi import APIRouter, Depends
from typing import Optional
from sqlalchemy.orm import Session

from db.database import get_db
from app.auth.oauth2 import get_cred

from utils.std_repo import BrowseQueries, browse_query
from utils.exceptions import BadRequest400, NotFound404
from .repo import UserRepo as repo
from .schema import MeDisplay, FavMenu, UserBase, ChangePass
from .model import User as BaseModel

path = 'me'
router = APIRouter(prefix=f'/v1/{path}', tags=[path], dependencies=[])
module_permission = False

########### ME ###########
@router.get("", response_model=MeDisplay)
def user_profile(
    refresh_token: Optional[bool] = False,
    db: Session = Depends(get_db),
    username = Depends(get_cred)
):
    return repo.get_by_username(username=username, db=db, refresh_token=refresh_token)

@router.get("/notifications")
def me_notif(
    browse_queries: BrowseQueries = Depends(browse_query),
    db: Session = Depends(get_db),
    cred = Depends(get_cred)
):
    return repo.me_notif(browse_queries=browse_queries, db=db, cred=cred)

@router.post("/favmenu", response_model=MeDisplay)
def me_fav_menu(
        request: FavMenu,
        db: Session = Depends(get_db),
        cred = Depends(get_cred)
    ):
    return repo.me_favmenu(request, db, cred)

@router.put("/update", response_model=MeDisplay)
def me_update(
        request: UserBase,
        db: Session = Depends(get_db),
        cred = Depends(get_cred)
    ):
    return repo.me_update(request, db, cred)

@router.put("/change-password", response_model=MeDisplay)
def me_password(
        request: ChangePass,
        db: Session = Depends(get_db),
        cred = Depends(get_cred)
    ):
    return repo.me_password(request, db, cred)

# @router.post("/change-picture", response_model=MeDisplay)
# async def post_pict(
#     file: UploadFile = File(...),
#     db: Session = Depends(get_db),
#     cred = Depends(get_cred)
# ):
#     contents = await file.read()
#     filesize = len(contents)
#     max_size = 10 * 1024 * 1024
#     if filesize > max_size:
#         raise BadRequest400(f"Max filesize: {10} Mb!")
#     data = db.query(BaseModel.id).filter(BaseModel.username == cred).first()
#     if data is None:
#         raise NotFound404('invalid credential')
#     user_id = data.id

#     return repo.post_pict(id=user_id, db=db, cred=cred, file=contents, filename=file.filename)
