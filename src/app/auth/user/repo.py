import os
import requests
import json
from sqlalchemy.orm import Session
from sqlalchemy.exc import IntegrityError
from utils.hash import Hash
from utils.exceptions import BadRequest400, NotFound404
from utils.std_repo import BrowseQueries, BulkId, browse, get_by_id, soft_delete, data_query
from utils.helper import date

from app.auth import oauth2
from .schema import UserCreate, UserBase, UserWithCred, FavMenu, ChangePass, ResetPass
from .model import User as BaseModel
from ..notification.model import Notification
# from utils import digitalocean

TIME_DELTA = int(os.environ.get('JWT_EXPIRED_MINUTE', '30'))

class UserRepo:
    ####### ME #######
    def get_by_username(username: str, db: Session, refresh_token = False):
        model = db.query(BaseModel).filter(BaseModel.username == username).first()
        if not model:
            raise NotFound404(message=f'Data {BaseModel.__tablename__} not Found')
        if refresh_token:
            access_token = oauth2.create_access_token(data = {'sub': username})
            expire = date.utcnow() + date.timedelta(TIME_DELTA)
            milliseconds = date.timemillis_from_date(expire)
            model.jwt_token = access_token
            model.jwt_token_expire = milliseconds
        return model

    def me_notif(browse_queries: BrowseQueries, db: Session, cred: str):
        data = db.query(Notification).filter(Notification.username == cred)
        data = data_query(browse_queries=browse_queries, model=Notification, data=data)
        read = db.query(Notification).filter(Notification.is_read == True).count()
        unread = db.query(Notification).filter(Notification.is_read == False).count()
        all = read + unread
        res = {
            'read': read,
            'unread': unread,
            'all': all,
            'items': data
        }
        return res

    def me_favmenu(request: FavMenu, db: Session, cred: str):
        model = db.query(BaseModel.menu_favorites).filter(BaseModel.username == cred).first()
        if not model:
            raise NotFound404(message=f'Data {BaseModel.__tablename__} not Found')
        if request:
            try:
                db.query(BaseModel).filter(BaseModel.username == cred).update({BaseModel.menu_favorites: request.menu_favorites})
                db.commit()
                model = db.query(BaseModel).filter(BaseModel.username == cred).first()
            except Exception as e:
                db.rollback()
                raise BadRequest400(str(e))
        return model

    def me_update(request: UserBase, db: Session, cred: str):
        try:
            if db.query(BaseModel.id).filter(BaseModel.username == cred).first():
                updates = {'updated_by': cred}
                for index in BaseModel.fillable:
                    if hasattr(request, index):
                        updates.update({index: getattr(request, index)})
                data = db.query(BaseModel).filter(BaseModel.username == cred).update(updates)
                db.commit()
                data = db.query(BaseModel).filter(BaseModel.username == cred).first()
                return data
            else:
                raise NotFound404(None, id)
        except IntegrityError as e:
            db.rollback()
            raise BadRequest400(str(e))

    def me_password(request: ChangePass, db: Session, cred: str):
        try:
            if db.query(BaseModel.id).filter(BaseModel.username == cred).first():
                model = db.query(BaseModel.password).filter(BaseModel.username == cred).first()
                curr = Hash.bcrypt(request.current_password)
                print(model.password, request.current_password)
                print(curr)
                print(Hash.verify(model.password, request.current_password))
                if Hash.verify(model.password, request.current_password):
                    passw = Hash.bcrypt(request.password)
                    db.query(BaseModel).filter(BaseModel.username == cred).update({BaseModel.password: passw, BaseModel.updated_by: cred})
                    db.commit()
                    data = db.query(BaseModel).filter(BaseModel.username == cred).first()
                    return data
                raise BadRequest400('Current password did not match')
            else:
                raise NotFound404(None, id)
        except IntegrityError as e:
            db.rollback()
            raise BadRequest400(str(e))

    # def post_pict(id: int, db: Session, cred: str, file: bytes, filename: str):
        # CDN_ENDPOINT = os.environ.get('DO_CDN_ENDPOINT')
        # FOLDER = os.environ.get('DO_FOLDER')
        # BUCKET = os.environ.get('DO_BUCKET')
        # try:
        #     data = db.query(BaseModel).filter(BaseModel.id == id).first()
        #     if file:
        #         upload_file = f'{FOLDER}/profile_pict__{id}{data.username}__{filename}'
        #         client = digitalocean.get_spaces_client()
        #         uploaded = digitalocean.upload_bytes_array_to_space(
        #             client,
        #             BUCKET,
        #             file,
        #             upload_file,
        #             is_public=True
        #         )
        #         path = f'{CDN_ENDPOINT}/{BUCKET}/{upload_file}'
        #     else:
        #         path = None
        #         filename = None
        #     db.query(BaseModel).filter(BaseModel.id == id).update({
        #         BaseModel.profile_picture: path,
        #         BaseModel.updated_by: cred
        #     })
        #     db.commit()
        #     # data = db.query(BaseModel).filter(BaseModel.id == id).first()
        #     return data
        # except IntegrityError as e:
        #     db.rollback()
        #     raise BadRequest400(str(e))


    ####### CRUD #######
    def reset_pass(id: int, request: ResetPass, db: Session, cred: str):
        try:
            if db.query(BaseModel.id).filter(BaseModel.id == id).first():
                passw = Hash.bcrypt(request.password)
                db.query(BaseModel).filter(BaseModel.id == id).update({BaseModel.password: passw, BaseModel.updated_by: cred})
                db.commit()
                data = db.query(BaseModel).filter(BaseModel.id == id).first()
                return data
            else:
                raise NotFound404(None, id)
        except IntegrityError as e:
            db.rollback()
            raise BadRequest400(str(e))

    def create(request: UserCreate, db: Session, cred: str):
        data = BaseModel()
        for index in BaseModel.fillable:
            try: setattr(data, index, getattr(request, index))
            except: None
        setattr(data, 'created_by', cred)
        setattr(data, 'password', Hash.bcrypt(request.password))
        try:
            db.add(data)
            db.commit()
            db.refresh(data)
            return data
        except Exception as e:
            db.rollback()
            raise BadRequest400(str(e))

    def update(id: int, request: UserWithCred, db: Session, cred: str):
        try:
            if db.query(BaseModel.id).filter(BaseModel.id == id).first():
                updates = {'updated_by': cred}
                for index in BaseModel.fillable:
                    if hasattr(request, index):
                        updates.update({index: getattr(request, index)})
                data = db.query(BaseModel).filter(BaseModel.id == id).update(updates)
                db.commit()
                data = db.query(BaseModel).filter(BaseModel.id == id).first()
                return data
            else:
                raise NotFound404(None, id)
        except IntegrityError as e:
            db.rollback()
            raise BadRequest400(str(e))

    def get_index(browse_queries: BrowseQueries, db: Session):
        data = browse(browse_queries=browse_queries, db=db, model=BaseModel)
        return data

    def get_id(id: int, db: Session):
        data = get_by_id(id=id, db=db, model=BaseModel)
        return data

    def delete(request: BulkId, id: int, db: Session, cred: str):
        return soft_delete(request=request, id=id, db=db, model=BaseModel, cred=cred, type = 'delete')

    def restore(request: BulkId, id: int, db: Session, cred: str):
        return soft_delete(request=request, id=id, db=db, model=BaseModel, cred=cred, type = 'restore')


    def get_fr_api(db: Session, cred: str):
        headers = {"Authorization": f"Bearer {cred}"}
        api1    = requests.get('https://api.bmbcoal.com/v1/auth/users?limit=0&order=id:ASC&like[]=role_name:fms-bmb&like[]=groups:fms', headers=headers, stream=True)
        res1    = UserRepo.getUserFrApi(api1, db)
        api2    = requests.get('https://api.bmbcoal.com/v1/auth/users?limit=0&order=id:ASC&like[]=role_name:operator&like[]=groups:fms', headers=headers, stream=True)
        res2    = UserRepo.getUserFrApi(api2, db)
        return {'res1': res1, 'res2': res2}

    def getUserFrApi(res, db: Session):
        lists   = json.loads(res.content)
        if lists['data'] is None:
            return lists
        insert  = 0
        update  = 0
        for v in lists['data']:
            try:
                check = db.query(BaseModel).filter(BaseModel.username == v['username']).first()
                if check:
                    db.query(BaseModel).filter(BaseModel.username == v['username']).update({
                        # '_id': v['_id'],
                        # 'picture': v['picture'],
                        'name': v['name'],
                        'username': v['username'],
                        'email': v['email'],
                        'password': Hash.bcrypt(v['username']),
                        'role_id': 1,
                        'custom_permissions': json.dumps(v['custom_permissions'])
                    })
                    update = update + 1
                else:
                    row = BaseModel()
                    row.name = v['name']
                    row.username = v['username']
                    row.email = v['email']
                    row.password = Hash.bcrypt(v['username'])
                    row.role_id = 1
                    row.custom_permissions = json.dumps(v['custom_permissions'])
                    db.add(row)
                    insert = insert + 1
                db.commit()
            except Exception as e:
                raise BadRequest400(str(e))
                # db.rollback()
                pass
        return {'insert': insert, 'update': update}
