from pydantic import BaseModel, EmailStr, root_validator, validator
from datetime import datetime, date
from typing import Optional, List, Any, Dict
import json

# required fields on create
class UserCreate(BaseModel):
    password: str
    email: str
    username: str
    name: str
    role_id: Optional[int]


# basic and editable fields
class UserBase(BaseModel):
    name: str
    phone: Optional[str] = ""


class UserWithCred(UserBase):
    username: Optional[str]
    email: Optional[str]
    # password: Optional[str]
    role_id: Optional[int]
    class Config:
        require_by_default = False


# me fields
class Role(BaseModel):
    name: str
    permissions: Optional[List] = []
    class Config():
        # orm_mode = True
        from_attributes = True


class MeDisplay(UserBase):
    id: int
    email: Optional[str] = ""
    profile_picture: Optional[str] = ""
    menu_favorites: Optional[List] = []
    jwt_token: Optional[str] = None
    jwt_token_expire: Optional[int] = None
    role: Optional[Role] = None
    custom_permissions: Optional[List] = None
    class Config():
        from_attributes = True

    @validator("custom_permissions", pre=True)
    def parse_string_array(cls, value):
        if isinstance(value, str):  # If the input is a string
            return json.loads(value)  # Convert the string to a list
        return []  # Otherwise, return it as is

class FavMenu(BaseModel):
    menu_favorites: Optional[List] = [{
        'id': 1,
        'app': '',
        'name': 'test',
        'slug': 'test',
        'path': '/test'
    }]

class ChangePass(BaseModel):
    current_password: str
    password: str

class ResetPass(BaseModel):
    password: str

class UserDisplay(UserBase):
    id: int
    username: Optional[str] = ""
    email: Optional[str] = ""
    role_id: Optional[int]
    role: Optional[Role] = None
    created_by: Optional[str] = ""
    created_at: Optional[datetime] = ""
    updated_by: Optional[str] = ""
    updated_at: Optional[datetime] = ""
    deleted_by: Optional[str] = ""
    deleted_at: Optional[datetime] = ""
    class Config():
        from_attributes = True

class UserDisplayCore(UserBase):
    id: int
    email: str