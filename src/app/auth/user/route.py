# from typing import Dict, Any
from fastapi import APIRouter, Depends
from typing import Optional
from sqlalchemy.orm import Session

from db.database import get_db
from app.auth.oauth2 import get_cred, check_permission, get_token

from utils.std_repo import BrowseQueries, BulkId, browse_query
from .repo import UserRepo as repo
from .schema import UserDisplay as Display, UserCreate, UserWithCred, ResetPass

path = 'auth/users'
router = APIRouter(prefix=f'/v1/{path}', tags=[path], dependencies=[])
module_permission = False

# BROWSE
@router.get('') # response_model=Union[List[Display], Page[Display]]
def browse(
        browse_queries: BrowseQueries = Depends(browse_query),
        db: Session = Depends(get_db),
        cred = Depends(get_cred)
    ):
    if module_permission: check_permission(cred=cred, db=db, module=f'{path}-browse')
    return repo.get_index(browse_queries, db)

# READ
@router.get('/{id}', response_model=Display)
def browse_id(
        id: str,
        db: Session = Depends(get_db),
        cred = Depends(get_cred)
    ):
    if module_permission: check_permission(cred=cred, db=db, module=f'{path}-read')
    return repo.get_id(id, db)


@router.post('/get')
def get_fr_api(db: Session = Depends(get_db), cred = Depends(get_token)):
    return repo.get_fr_api(db, cred)

# CREATE
@router.post('', response_model=Display)
def create(
        request: UserCreate,
        db: Session = Depends(get_db),
        cred = Depends(get_cred)
    ):
    if module_permission: check_permission(cred=cred, db=db, module=f'{path}-create')
    cred = 'system'
    return repo.create(request, db, cred)

# UPDATE
@router.put('/{id}', response_model=Display)
def update(
        id: int,
        request: UserWithCred, # Dict[Any, Any]
        db: Session = Depends(get_db),
        cred = Depends(get_cred)
    ):
    if module_permission: check_permission(cred=cred, db=db, module=f'{path}-update')
    return repo.update(id, request, db, cred)

# RESET PASS
@router.put('/{id}/reset-password', response_model=Display)
def reset_pass(
        id: int,
        request: ResetPass,
        db: Session = Depends(get_db),
        cred = Depends(get_cred)
    ):
    if module_permission: check_permission(cred=cred, db=db, module=f'{path}-update')
    return repo.reset_pass(id, request, db, cred)

# DELETE & RESTORE
@router.delete('/delete')
def delete(
        request: BulkId,
        id: Optional[int] = None,
        db: Session = Depends(get_db),
        cred = Depends(get_cred)
    ):
    if module_permission: check_permission(cred=cred, db=db, module=f'{path}-delete')
    return repo.delete(request, id, db, cred)

@router.delete('/restore')
def restore(
        request: BulkId,
        id: Optional[int] = None,
        db: Session = Depends(get_db),
        cred = Depends(get_cred)
    ):
    if module_permission: check_permission(cred=cred, db=db, module=f'{path}-restore')
    return repo.restore(request, id, db, cred)
