from sqlalchemy import Column, DateTime, ForeignKey, String, func, Text
from sqlalchemy.orm import relationship
from sqlalchemy.sql.sqltypes import BigInteger, String
from sqlalchemy.dialects.postgresql import JSONB

from db.database import Base


class User(Base):
    __tablename__ = "users"

    id = Column(BigInteger, primary_key=True, index=True)
    # _id = Column(String, unique=True)
    username = Column(String(225), unique=True, nullable=False)
    email = Column(String(255), unique=True, nullable=False)
    password = Column(String(), nullable=False)
    name = Column(String(225), nullable=False)

    phone = Column(String(255), nullable=True)

    created_by = Column(String(), nullable=True)
    updated_by = Column(String(), nullable=True)
    deleted_by = Column(String(), nullable=True)

    created_at = Column(DateTime, default=func.now())
    updated_at = Column(DateTime, default=func.now(), onupdate=func.now())
    deleted_at = Column(DateTime, nullable=True)

    role_id = Column(BigInteger, ForeignKey("roles.id"))
    role = relationship("Role", back_populates="users") # primaryjoin="User.role_id == Role.id"

    custom_permissions = Column(Text, nullable=True)

    fillable = [
        "email",
        "username",
        "password",
        "name",
        "phone",
        "role_id",
        "custom_permissions"
    ]
