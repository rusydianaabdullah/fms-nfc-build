from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session

from db.database import get_db
from app.auth.oauth2 import get_cred, check_permission

from utils.std_repo import BrowseQueries, browse_query
from .repo import ErrorLogRepo as repo

path = 'error-logs'
router = APIRouter(prefix=f'/v1/{path}', tags=[path], dependencies=[])
permission = False

# BROWSE
@router.get('')
def browse(browse_queries: BrowseQueries = Depends(browse_query), db: Session = Depends(get_db), cred = Depends(get_cred)):
    if permission: check_permission(cred=cred, db=db, module=f'{path}-browse')
    return repo.get_index(browse_queries, db)
