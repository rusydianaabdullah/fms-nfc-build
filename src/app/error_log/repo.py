import requests
from fastapi.encoders import jsonable_encoder
from config import statics
from sqlalchemy.orm import Session
from utils.std_repo import BrowseQueries, browse
from utils.helper import date

from .model import ErrorLogs as BaseModel

class ErrorLogRepo:
  def post_to_server(db: Session, token: str, limit: int = None):
    try:
      data = db.query(BaseModel).filter(BaseModel.deleted_at == None).filter(BaseModel.flag_sync == False)
      data = data.order_by(BaseModel.id)
      if limit:
        data = data.limit(limit)
      data = data.all()
      ids = []
      jsons = [] # jsonable_encoder(data)
      for v in data:
        jsons.append({
          'id': v.id,
          'type': 'error',
          'location': v.location,
          'module': v.module,
          'module_id': v.module_id,
          'name': v.name,
          'remark': v.remark,
          'created_at': date.timemillis_from_date(v.created_at, 0),
        })
        ids.append(v.id)
      url     = statics['error_log']
      headers = {"Authorization": f"Bearer {token}"}
      request = requests.post(url, headers=headers, json={'data': jsons})
      if request.status_code == 200:
        db.query(BaseModel).filter(BaseModel.id.in_(ids)).update({BaseModel.flag_sync: True})
        db.commit()
        return 'success'
      else:
        print('error: ', request.content)
        return 'failed'
    except Exception as e:
      db.rollback()
      return {'message': f'failed: {str(e)}'}


  def get_index(browse_queries: BrowseQueries, db: Session):
    data = browse(browse_queries=browse_queries, db=db, model=BaseModel)
    return data

