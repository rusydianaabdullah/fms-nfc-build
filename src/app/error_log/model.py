from db.database import Base
from sqlalchemy import Boolean, Column, DateTime, func
from sqlalchemy.sql.sqltypes import BigInteger, String, Text

class ErrorLogs(Base):
  __tablename__ = 'error_logs'

  id = Column(BigInteger, primary_key=True, index=True)
  flag_sync = Column(Boolean, default=False)
  location = Column(String, nullable=True)
  module = Column(String, nullable=True)
  module_id = Column(BigInteger, nullable=True)
  name = Column(String, nullable=True)
  remark = Column(Text, nullable=True)
  created_at = Column(DateTime, default=func.now())
  updated_at = Column(DateTime, default=func.now(), onupdate=func.now())
  deleted_at = Column(DateTime, nullable=True)

  fillable = [
    'module',
    'module_id',
    'name',
    'remark'
  ]
