import json
import requests
from sqlalchemy import column, text
from sqlalchemy.orm import Session

from utils.mqtt import send_mqtt
from utils.exceptions import BadRequest400
from utils.helper import date
from utils.weighing import run_weighing
from .schema import WeighingReq
from ..truck.model import TruckTare, Truck
from ..nfc.model import DeliveryOrder, NfcConfig
from ..local_config.model import LocalConfig

class WeighingHelper:
  def check_minweight(request: WeighingReq, cfg: NfcConfig):
    weighing = run_weighing(False)
    if cfg:
      if weighing and cfg.min_weight > weighing:
        raise BadRequest400(f'berat < {cfg.min_weight}')

  def get_tare(request: WeighingReq, local_cfg: LocalConfig, db: Session):
    WC_CODE = local_cfg.wc_code
    tare = 0
    try:
      t = db.query(TruckTare.value).filter(TruckTare.truck_code == request.truck_code).filter(TruckTare.wc_code == WC_CODE).order_by(TruckTare.id_.desc()).value(column('value'))
      tare = int(t) if int(t) > 0 else 0
    except:
      pass
    return tare

  def get_abnormal(request: WeighingReq, rfid: str, db: Session):
    abnormal = 'TIDAK TAP KARTU'
    truck_code = request.truck_code
    try:
      abnrfid = ''
      if rfid == truck_code         : ''
      elif rfid.find('error') > -1  : abnrfid += f'RFID Error: {rfid}'
      elif rfid == ''               : abnrfid += 'RFID IS NOT DETECTED'
      elif rfid != truck_code       : abnrfid += f'DIFF RFID: {rfid}'
      else                          : abnrfid += f'RFID OTHERS: {rfid}'

      if abnrfid != '':
        send_mqtt(f'abnormal: {abnrfid}')
        abnormal += f' | {abnrfid}'
    except:
      pass
    return abnormal

  def get_vendortruck(request: WeighingReq, db: Session):
    vendor_code = ''
    try:
      truck = db.query(Truck.company_code).filter(Truck.code == request.truck_code).first()
      vendor_code = truck.company_code
    except:
      pass
    return vendor_code

  def gen_code(request: WeighingReq, prefix: str, shiftdate: dict, db: Session):
    try:
      shf   = shiftdate.get('shift')
      dt    = shiftdate.get('date')
      ct    = db.query(DeliveryOrder).filter(DeliveryOrder.deleted_at == None).filter(DeliveryOrder.date_0 == dt).filter(DeliveryOrder.shift_0 == shf).count()
      count = int(ct)+1
      code_date = date.code_date()
      code = f'{prefix}{code_date}{shf}{count:03}'
      return {'code': code, 'rit': count}
    except Exception as e:
      raise BadRequest400(str(e))

  def check_existing_do_local(request: WeighingReq, db: Session):
    try:
      code = request.code
      sqlquery = f"""select code from do_closed where code = '{code}'"""
      data = db.execute(text(sqlquery)).first()
      if data: return True
      return False
    except:
      return False

  def check_existing_do_realtime(request: WeighingReq, token: str):
    try:
      headers   = {"Authorization": f"Bearer {token}"}
      endpoint  = f"https://fms-api2.bmbcoal.com/v1/transaction/delivery-orders?limit=1&where=status:1&where=code:{request.code}"
      res       = requests.get(endpoint, headers=headers)
      lists     = json.loads(res.content)
      if lists: return True
      return False
    except:
      return False