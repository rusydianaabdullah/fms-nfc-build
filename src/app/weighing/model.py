import uuid

from db.database import Base
from sqlalchemy import Column, func
from sqlalchemy.sql.sqltypes import BigInteger, Integer, String, Double, DateTime, Boolean, Text, Date
from sqlalchemy.orm import relationship

class Company(Base):
  __tablename__ = 'companies'

  id = Column(BigInteger, primary_key=True, unique=True)
  code = Column(String, nullable=True)
  name = Column(String, nullable=True)
  type = Column(String, nullable=True)
  parent_id = Column(BigInteger, nullable=True)
  ownership = Column(String, nullable=True)
  block = Column(String, nullable=True)
  address = Column(Text, nullable=True)
  remark = Column(Text, nullable=True)
  created_at = Column(DateTime, default=func.now())
  updated_at = Column(DateTime, default=func.now(), onupdate=func.now())
  deleted_at = Column(DateTime, nullable=True)

class Location(Base):
  __tablename__ = 'locations'

  id = Column(BigInteger, primary_key=True, unique=True)
  code = Column(String, nullable=True)
  name = Column(String, nullable=True)
  company_code = Column(String, nullable=True)
  status = Column(Integer, nullable=True)
  type = Column(Integer, nullable=True)
  parent_id = Column(BigInteger, nullable=True)
  flag_show = Column(String, nullable=True)
  area = Column(String, nullable=True)
  remark = Column(Text, nullable=True)
  created_at = Column(DateTime, default=func.now())
  updated_at = Column(DateTime, default=func.now(), onupdate=func.now())
  deleted_at = Column(DateTime, nullable=True)

class DeliveryOrderTemp(Base):
  __tablename__ = 'delivery_order_temps'

  id_ = Column(BigInteger, primary_key=True, index=True)
  id = Column(BigInteger, unique=True, nullable=True)
  type = Column(Integer, nullable=True)
  code = Column(String, nullable=True)
  vendor_code = Column(String, nullable=True)
  driver = Column(String, nullable=True)
  truck_code = Column(String, nullable=True)
  no_lambung = Column(String, nullable=True)
  owner_code = Column(String, nullable=True)
  pit_code = Column(String, nullable=True)
  seam_code = Column(String, nullable=True)

  datetime = Column(BigInteger, nullable=True)
  date = Column(Date, nullable=True)
  rit = Column(Integer, nullable=True)
  shift = Column(String, nullable=True)
  user = Column(String, nullable=True)
  wc = Column(String, nullable=True)
  loc = Column(String, nullable=True)
  loc_1 = Column(String, nullable=True)
  bruto = Column(Double, nullable=True)
  net = Column(Double, nullable=True)
  rfid = Column(String, nullable=True)
  abnormal = Column(Text, nullable=True)

  created_at = Column(DateTime, default=func.now())
  updated_at = Column(DateTime, default=func.now(), onupdate=func.now())
  deleted_at = Column(DateTime, nullable=True)

  fillable = [
    '_id',
    'type',
    'code',
    'truck_code',
    'no_lambung',
    'owner_code',
    'pit_code',
    'seam_code',
    'datetime',
    'date',
    'rit',
    'shift',
    'user',
    'wc',
    'loc',
    'bruto',
    'net',
    'rfid',
  ]

  def as_dict(self):
    return {c.name: getattr(self, c.name) for c in self.__table__.columns}

class DoClosed(Base):
  __tablename__ = 'do_closed'

  id = Column(BigInteger, primary_key=True, unique=True)
  date = Column(String, nullable=True)
  code = Column(Date, nullable=True)
