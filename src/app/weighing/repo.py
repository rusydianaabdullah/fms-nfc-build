import requests
import json
from fastapi.encoders import jsonable_encoder
from config import getenv
from sqlalchemy import func, column
from sqlalchemy.orm import Session
from sqlalchemy.exc import IntegrityError
from starlette.background import BackgroundTasks
from datetime import datetime

from utils.exceptions import BadRequest400
from utils.std_repo import BrowseQueries, browse
from utils.helper import date
from utils.mqtt import send_mqtt, open_gate
from utils.rfid import run_rfid
from utils.cctv import run_cctv
from utils.weighing import run_weighing

from .helper import WeighingHelper
from .schema import WeighingReq, TareReq
from .model import Company, Location, DeliveryOrderTemp, DoClosed
from ..truck.repo import TruckRepo
from ..truck.model import TruckTare, WeighingCenter, Truck
from ..nfc.model import DeliveryOrder, NfcConfig
from ..nfc.repo import NfcRepo
from ..nfc.helper import NfcHelper
from ..local_config.model import LocalConfig
from ..cctv.repo import CctvRepo
from utils.logs import ErrorLog

class WeighingRepo:
  def truck_tare(truck_code: str, db: Session): # GET TARE
    try:
      local_cfg = db.query(LocalConfig).first()
      WC_CODE     = local_cfg.wc_code if local_cfg else getenv('WC_CODE')
      TYPE  = local_cfg.type if local_cfg else getenv('TYPE')
      HAULING  = local_cfg.hauling if local_cfg else getenv('HAULING')
      if TYPE == 'out' and HAULING == 1:
        return 0

      data = (
        db.query(TruckTare)
        .filter(TruckTare.truck_code == truck_code)
        .filter(TruckTare.wc_code == WC_CODE)
        .order_by(TruckTare.id_.desc())
        .first()
      )
      print(data)

      if data:
        return data.value
      else:
        return 0
    except IntegrityError as e:
      raise BadRequest400(str(e))


  def weighing(request: WeighingReq, db: Session, cred: str, token: str, bt: BackgroundTasks): ### WEIGHING TRANSACTIONS
    local_cfg = db.query(LocalConfig).first()
    cfg = db.query(NfcConfig).first()
    TYPE = local_cfg.type if local_cfg else getenv('TYPE')

    WeighingHelper.check_minweight(request, cfg)
    try:
      if (TYPE == 'out'):
        res = WeighingRepo.do_type_out(request=request, db=db, cred=cred, local_cfg=local_cfg, cfg=cfg, bt=bt)
        if res['status'] == 'failed': raise BadRequest400(res['message'])
        do_id = res['do_id']
        if do_id:
          send_mqtt(f'print:{do_id}')
          send_mqtt(f'do:{do_id}')
        open_gate()
        # bt.add_task(CctvRepo.create, db, res['code'])
        return res
      else:
        if WeighingHelper.check_existing_do_local(request, db):
          raise BadRequest400('DO Sudah Close!')
        res = WeighingRepo.do_type_in(request=request, db=db, cred=cred, local_cfg=local_cfg, cfg=cfg, bt=bt)
        if res['status'] == 'failed': raise BadRequest400(res['message'])
        do_id = res['do_id']
        if do_id:
          send_mqtt(f'print:{do_id}')
          send_mqtt(f'do:{do_id}')
        open_gate()
        # bt.add_task(CctvRepo.create, db, res['code'])
        bt.add_task(WeighingRepo.get_do_from_cloud, db, token, request.code)
        return res
    except Exception as e:
      msg = 'WeighingRepo weighing: ' + str(e)
      raise BadRequest400(msg)

  def og_trip(truck_code: str, db: Session):
    try:
      local_cfg = db.query(LocalConfig).first()
      if local_cfg and local_cfg.type == 'out':
        # on going trip validation
        cek = db.query(DeliveryOrderTemp).filter(DeliveryOrderTemp.truck_code == truck_code).count()
        print(cek, 'cek')
        return cek
        # end on going trip validation
      else: return 0
    except Exception as e:
      ErrorLog.create(db, str(e), 'og_trip')
      print(e)
      raise BadRequest400(str(e))

  def do_type_out(request: WeighingReq, db: Session, cred: str, local_cfg: LocalConfig, cfg: NfcConfig, bt: BackgroundTasks):
    LOC_CODE    = local_cfg.loc_code if local_cfg else getenv('LOC_CODE')
    WC_CODE     = local_cfg.wc_code if local_cfg else getenv('WC_CODE')
    HAULING     = int(local_cfg.hauling) if local_cfg else int(getenv('HAULING'))
    PIT_CODE    = local_cfg.pit_code if local_cfg else getenv('PIT_CODE')
    OWNER_CODE  = local_cfg.owner_code if local_cfg else getenv('OWNER_CODE')
    PREFIX      = local_cfg.prefix if local_cfg else getenv('PREFIX')
    CCTV        = run_cctv(False)
    RFID        = run_rfid(db, False)

    # weighing validation
    weighing    = 0
    if WC_CODE:
      weighing  = run_weighing()
      if weighing == None:
        raise BadRequest400('weighing gagal')
      elif weighing <= 5000:
        raise BadRequest400('weighing <= 5000')
    # end weighing validation
    shiftdate   = date.shift_date()
    shf         = shiftdate.get('shift')
    dt          = shiftdate.get('date')
    truck_name  = NfcHelper.get_truck(request.truck_code, db)
    tare        = WeighingHelper.get_tare(request, local_cfg, db)
    abnormal    = WeighingHelper.get_abnormal(request, RFID, db)
    # RFID validation
    SKIP_DIFF_RFID = getenv('SKIP_DIFF_RFID', '0')
    if SKIP_DIFF_RFID == '0' and abnormal.find('RFID') > -1:
      if cfg.diff_rfid:
        db.query(NfcConfig).update({NfcConfig.diff_rfid: False})
        db.commit()
      else:
        send_mqtt(f'popuprfid:{abnormal}')
        return { 'status': 'failed', 'message': abnormal }
    # end RFID validation
    if request.comments:
      abnormal += f"| {request.comments}"
    vendor_code = WeighingHelper.get_vendortruck(request, db)
    code_rit    = WeighingHelper.gen_code(request, PREFIX, shiftdate, db)
    code        = code_rit['code']
    rit         = code_rit['rit']
    destination = local_cfg.destination

    truck_name  = NfcHelper.get_truck(request.truck_code, db)
    truck_code  = request.truck_code
    try:
      do = DeliveryOrder(
        type        = HAULING,
        owner_code  = OWNER_CODE,
        pit_code    = PIT_CODE,
        code        = f'{code}-{truck_code}',
        vendor_code = vendor_code,
        truck_code  = truck_code,
        no_lambung  = truck_name,
        datetime_0  = date.timemillis(),
        date_0      = dt,
        shift_0     = shf,
        rit_0       = rit,
        user_0      = cred,
        wc_0        = WC_CODE,
        loc_0       = LOC_CODE,
        bruto_0     = weighing,
        net_0       = weighing - tare,
        rfid_0      = RFID,
        abnormal_0  = abnormal,
        cctv_0      = CCTV,
        loc_1       = destination
      )
      db.add(do)
      db.commit()
      db.refresh(do)

      res = {
        'status': 'success',
        'message': 'DO berhasil dibuat',
        'data': do,
        'do_id': do.id,
        'code': do.code,
      }

      print(f'bt:sync_do={do.id}')
      if (getenv('SYNC_AFTER_SAVE', '1') == '1'): bt.add_task(NfcRepo.sync_do, db, do.id)

      # if RFID == None or RFID == '': bt.add_task(NfcRepo.read_rfid, db, do, local_cfg)
      return res
    except Exception as e:
      db.rollback()
      ErrorLog.create(db, str(e), 'manual_do')
      print(e)
      raise BadRequest400(str(e))

  def do_type_in(request: WeighingReq, db: Session, cred: str, local_cfg: LocalConfig, cfg: NfcConfig, bt: BackgroundTasks):
    LOC_CODE  = local_cfg.loc_code if local_cfg else getenv('LOC_CODE')
    WC_CODE   = local_cfg.wc_code if local_cfg else getenv('WC_CODE')
    HAULING   = int(local_cfg.hauling) if local_cfg else int(getenv('HAULING'))
    CCTV      = run_cctv(False)
    RFID      = run_rfid(db, False)
    WB_PREFIX_LIST = getenv('WB_PREFIX_LIST').split(',') if getenv('WB_PREFIX_LIST') else ''

    weighing  = 0
    if WC_CODE:
      weighing  = run_weighing()
      if weighing == None:
        raise BadRequest400('weighing gagal')
      elif weighing <= 5000:
        raise BadRequest400('weighing <= 5000')
    shiftdate = date.shift_date()
    shf       = shiftdate.get('shift')
    dt        = shiftdate.get('date')
    tare        = WeighingHelper.get_tare(request, local_cfg, db)
    abnormal    = WeighingHelper.get_abnormal(request, RFID, db)

    SKIP_DIFF_RFID = getenv('SKIP_DIFF_RFID', '0')

    if SKIP_DIFF_RFID == '0' and abnormal.find('RFID') > -1:                              # DONT WRITE CARD! RFID VALIDATION !!!
      if cfg.diff_rfid:
        db.query(NfcConfig).update({NfcConfig.diff_rfid: False})
        db.commit()
      else:
        send_mqtt(f'popuprfid:{abnormal}')
        return { 'status': 'failed', 'message': abnormal }

    if request.comments:
      abnormal += f"| {request.comments}"
    vendor_code = WeighingHelper.get_vendortruck(request, db)
    truck_name  = NfcHelper.get_truck(request.truck_code, db)

    # manual code validation
    if request.manual_code:
      abnormal += f" | KETIK MANUAL"

      isCorrect   = True
      splitedCode = request.code.split('-')
      prefix      = splitedCode[0][0:5]
      dateStr     = splitedCode[0][5:11]
      shift       = splitedCode[0][11:12]
      rit         = splitedCode[0][12:15]
      truck       = splitedCode[1]
      # printCode = f'prefix:{prefix} | dateStr:{dateStr} | shift:{shift} | rit:{rit} | truck:{truck}'
      # validation
      if prefix not in WB_PREFIX_LIST: isCorrect = False
      if len(dateStr) < 6: isCorrect = False
      if shift not in ['1', '2']: isCorrect = False
      if len(rit) < 3: isCorrect = False
      if not db.query(Truck).filter(Truck.code == truck).first(): isCorrect = False
      # validation res
      if isCorrect == False: return { 'status': 'failed', 'message': 'Format kode yang diketik salah!, coba cek kembali SK nya!' }

    try:
      count = db.query(DeliveryOrder).filter(DeliveryOrder.date_1 == dt).filter(DeliveryOrder.shift_1 == shf).count()
      do = DeliveryOrder(
        type        = HAULING,
        vendor_code = vendor_code,
        truck_code  = request.truck_code,
        no_lambung  = truck_name,
        code        = request.code,
        datetime_1  = date.timemillis(),
        date_1      = dt,
        shift_1     = shf,
        rit_1       = count+1,
        user_1      = cred, # 'system',
        wc_1        = WC_CODE,
        loc_1       = LOC_CODE,
        bruto_1     = weighing,
        net_1       = weighing - tare,
        rfid_1      = RFID,
        abnormal_1  = abnormal,
        cctv_1      = CCTV
      )
      db.add(do)
      # Remove Delivery order temp base on code
      db.query(DeliveryOrderTemp).filter(DeliveryOrderTemp.code == request.code).update({DeliveryOrderTemp.deleted_at: func.now()})
      db.commit()
      db.refresh(do)

      res = {
        'status': 'success',
        'message': 'DO berhasil dibuat',
        'data': do,
        'do_id': do.id,
        'code': do.code,
      }

      print(f'bt:sync_do={do.id}')
      if (getenv('SYNC_AFTER_SAVE', '1') == '1'): bt.add_task(NfcRepo.sync_do, db, do.id)
      
      # if RFID == None or RFID == '': bt.add_task(NfcRepo.read_rfid, db, do, local_cfg)
      return res
    except Exception as e:
      db.rollback()
      ErrorLog.create(db, str(e), 'manual_do', request.code)
      print(e)
      raise BadRequest400(str(e))

  def save_tare(request: TareReq, db: Session, bt: BackgroundTasks): ### TARE TRANSACTIONS
    try:
      local_cfg = db.query(LocalConfig).first()
      cfg = db.query(NfcConfig).first()

      weigh = 0
      if (getenv('WB_TARA_SOURCE', 'be') == 'ui') :
        weigh = request.weight
      else : 
        weigh = run_weighing()

      # min_truck_tare = maksmimal berat truck ketika timbang kosong
      if int(weigh) > cfg.min_truck_tare:
        raise BadRequest400(f'Tidak bisa Save karena berat Tara truk ({weigh}) lebih dari {cfg.min_truck_tare} Kg')
      
      if int(weigh) == 0:
        raise BadRequest400(f'Gagal membaca berat Tara, klik ulang SAVE TARA')

      if local_cfg:
        wc_code = local_cfg.wc_code
        truck = db.query(Truck.code).filter(Truck.id == request.truck_id).first()
        weighing_center = db.query(WeighingCenter.id).filter(WeighingCenter.code == wc_code).first()

        if weighing_center is None :
          raise BadRequest400(f'Weighing center {wc_code} tidak ditemukan!')

        # cek tanggal tare terakhir
        lastTareDate = db.query(TruckTare.date).filter(TruckTare.truck_code == truck.code).filter(TruckTare.wc_code == wc_code).filter(TruckTare.deleted_at != None).order_by(TruckTare.date.desc()).first()
        if lastTareDate:
          daysDiff = date.date_diff_in_days(lastTareDate.date, date.timemillis())
          tareMaxDay = cfg.tare_max_day
          if daysDiff <= tareMaxDay:
            raise BadRequest400(f'Tare sudah dilakukan {tareMaxDay} hari kebelakang, tidak bisa tare ulang')

        truck_code = truck.code
        tare = TruckTare(
          flag_sync   = False,
          truck_id    = request.truck_id,
          wc_id       = weighing_center.id,
          truck_code  = truck_code,
          wc_code     = wc_code,
          value       = weigh,
          date        = date.timemillis(),
        )
        db.add(tare)
        db.commit()
        db.refresh(tare)
        bt.add_task(CctvRepo.create, db, f'tare_{truck_code}', f'tare_{truck_code}_{date.today()}', 'master/truck-tares', tare.id_)
        # TruckRepo.save_cctv(db, tare)
        return {
          'status': 'success',
          'message': f'{weigh}Kg | Tare'
        }
      else:
        raise BadRequest400('No Local Config')
    except Exception as e:
        raise BadRequest400(str(e))

  def get_from_cloud(db: Session, token: str): ### GET COMPANY & LOCATIONS FROM SERVER
    headers = {"Authorization": f"Bearer {token}"}
    company_api = requests.get('https://fms-api.bmbcoal.com/v1/master/companies?limit=0&order=id:asc', headers=headers, stream=True)
    companies   = WeighingRepo.get_companies(res=company_api, db=db)

    location_api= requests.get('https://fms-api.bmbcoal.com/v1/master/locations?limit=0&order=id:asc', headers=headers, stream=True)
    locations   = WeighingRepo.get_locations(res=location_api, db=db)
    return {'companies': companies, 'locations': locations}

  def get_companies(res, db: Session):
    lists   = json.loads(res.content)
    if lists['data'] is None:
      return lists
    insert  = 0
    update  = 0
    for v in lists['data']:
      print(v)
      try:
        check = db.query(Company).filter(Company.id == v['id']).first()
        if check:
          db.query(Company).filter(Company.id == v['id']).update({
            'id': v['id'],
            'code': v['code'],
            'name': v['name'],
            'type': v['type'],
            'parent_id': v['parent_id'],
            'ownership': v['ownership'],
            'block': v['block'],
            'address': v['address'],
            'remark': v['remark'],
          })
          update = update + 1
        else:
          row = Company()
          row.id = v['id']
          row.code = v['code']
          row.name = v['name']
          row.type = v['type']
          row.parent_id = v['parent_id']
          row.ownership = v['ownership']
          row.block = v['block']
          row.address = v['address']
          row.remark = v['remark']
          db.add(row)
          insert = insert + 1
        db.commit()
      except Exception as e:
        # raise BadRequest400(str(e))
        # db.rollback()
        pass
    return {'insert': insert, 'update': update}

  def get_locations(res, db: Session):
    lists   = json.loads(res.content)
    if lists['data'] is None:
      return lists
    insert  = 0
    update  = 0
    for v in lists['data']:
      try:
        check = db.query(Location).filter(Location.id == v['id']).first()
        if check:
          db.query(Location).filter(Location.id == v['id']).update({
            'id': v['id'],
            'area': v['area'],
            'code': v['code'],
            'company_code': v['company_code'],
            'flag_show': v['flag_show'],
            'name': v['name'],
            'status': v['status'],
            'type': v['type'],
            'parent_id': v['parent_id'],
            'remark': v['remark'],
          })
          update = update + 1
        else:
          row = Location()
          row.id = v['id']
          row.area = v['area']
          row.code = v['code']
          row.company_code = v['company_code']
          row.flag_show = v['flag_show']
          row.name = v['name']
          row.status = v['status']
          row.type = v['type']
          row.parent_id = v['parent_id']
          row.remark = v['remark']
          db.add(row)
          insert = insert + 1
        db.commit()
      except Exception as e:
        # raise BadRequest400(str(e))
        # db.rollback()
        pass
    return {'insert': insert, 'update': update}

  def get_do_from_cloud(db: Session, token: str, lastdo: str = None): ### GET DO TEMP
    insert  = 0
    delete  = 0
    local_cfg = db.query(LocalConfig).first()
    # if local_cfg and local_cfg.type == 'in':
    headers   = {"Authorization": f"Bearer {token}"}
    endpoint  = f"https://fms-api2.bmbcoal.com/v1/transaction/delivery-orders?limit=300&order=id:ASC&where=status:0&where=type:{local_cfg.hauling}"
    res       = requests.get(endpoint, headers=headers, stream=True)
    lists     = json.loads(res.content)
    if lists is None:
      return lists

    delete = db.query(DeliveryOrderTemp).delete()
    try:
      rows = []
      for v in lists:
        if lastdo and lastdo == v['code']:
          pass
        else:
          no_lambung = db.query(Truck.name).filter(Truck.code == v['truck_code']).value(column('name'))
          row = DeliveryOrderTemp(
            id = v['id'],
            type = v['type'],
            code = v['code'],
            truck_code = v['truck_code'],
            no_lambung = no_lambung,
            owner_code = v['owner_code'],
            pit_code = v['pit_code'],
            datetime = v['datetime_0'],
            date = v['date_0'],
            rit = v['rit_0'],
            shift = v['shift_0'],
            user = v['user_0'],
            wc = v['wc_0'],
            loc = v['loc_0'],
            loc_1 = v['loc_1'],
            bruto = v['bruto_0'],
            net = v['net_0'],
            rfid = v['rfid'],
          )
          insert = insert + 1
          rows.append(row)
      db.bulk_save_objects(rows)
      db.commit()
      
      # get last update do data
      parent_folder = 'src/cctv'
      x = datetime.now()
      now = x.strftime("%Y-%m-%d %H:%M:%S")
      f = open(f'{parent_folder}/last_do_temp_update.txt', "w")
      f.write(now)
      f.close()
    except Exception as e:
      db.rollback()
      raise BadRequest400(str(e))

    return {'insert': insert, 'delete': delete}

  def get_do_closed(db: Session, token: str):
    insert  = 0
    delete  = 0
    local_cfg = db.query(LocalConfig).first()
    if local_cfg and local_cfg.type == 'in':
      hauling   = local_cfg.hauling if local_cfg.hauling else '1'
      headers   = {"Authorization": f"Bearer {token}"}
      endpoint  = f"https://fms-api2.bmbcoal.com/v1/transaction/delivery-orders/status-closed?type={hauling}"
      res       = requests.get(endpoint, headers=headers, stream=True)
      lists     = json.loads(res.content)
      if lists is None:
        return lists

      delete = db.query(DoClosed).delete()
      try:
        rows = []
        for v in lists:
          row = DoClosed(
            date = v['date'],
            code = v['code']
          )
          insert = insert + 1
          rows.append(row)
        db.bulk_save_objects(rows)
        db.commit()
      except Exception as e:
        db.rollback()
        raise BadRequest400(str(e))
    return {'insert': insert, 'delete': delete}

  def get_index_do_temp(browse_queries: BrowseQueries, db: Session):
    if browse_queries.count:
      return db.query(DeliveryOrderTemp).count()

    data = db.query(DeliveryOrderTemp.code, DeliveryOrderTemp.truck_code, DeliveryOrderTemp.rfid, Truck.name, DeliveryOrderTemp.loc, DeliveryOrderTemp.loc_1, DeliveryOrderTemp.pit_code).join(Truck, Truck.code == DeliveryOrderTemp.truck_code).all()
    res = []
    for d in data:
      res.append({
        'code': d.code,
        'truck_code': d.truck_code,
        'rfid': json.loads(d.rfid) if d.rfid is not None  else {"out": None, "in": None},
        'truck_name': d.name,
        'loc_0': d.loc,
        'loc_1': d.loc_1,
        'pit_code': d.pit_code,
      })
    return res

  def get_index_company(browse_queries: BrowseQueries, db: Session): ### GET COMPANY
    data = browse(browse_queries=browse_queries, db=db, model=Company)
    return data

  def get_index_location(browse_queries: BrowseQueries, db: Session): ### GET LOCATIONS
    data = browse(browse_queries=browse_queries, db=db, model=Location)
    return data


  # def validate_rfid(rfid_code: str, wc_code: str, db: Session):
    #   try:
    #     truck = db.query(Truck.code).filter(Truck.rfid_code == rfid_code).value(column('code'))
    #     if truck:
    #       tare = db.query(TruckTare.value).filter(TruckTare.truck_code == truck_code).filter(TruckTare.wc_code == wc_code).order_by(TruckTare.id_.desc()).value(column('value'))
    #       return {"truck": truck, tare: tare}
    #     else:
    #       return true
    #   except IntegrityError as e:
    #     raise BadRequest400(str(e))