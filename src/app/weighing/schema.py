import datetime
from pydantic import BaseModel
from typing import List, Optional
from typing_extensions import Annotated
from fastapi.param_functions import Form

class WeighingReq(BaseModel):
  code: Optional[str] = None
  truck_code: Optional[str] = None
  comments: Optional[str] = None
  manual_code: Optional[bool] = False

class TareReq(BaseModel):
  truck_id: int
  weight: Optional[int] = 0
    