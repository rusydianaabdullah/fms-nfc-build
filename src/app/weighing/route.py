from fastapi import APIRouter, Query, Depends
from typing import Optional
from sqlalchemy.orm import Session
from starlette.background import BackgroundTasks

from db.database import get_db
from app.auth.oauth2 import get_cred, get_token

from utils.std_repo import BrowseQueries, BulkId, browse_query
from .repo import WeighingRepo as repo
from .schema import WeighingReq, TareReq

path = 'weighing'
router = APIRouter(prefix=f'/v1/{path}', tags=[path], dependencies=[])
permission = False

@router.get('/truck-tares')                                     # GET TARE VALUE BASED ON TRUCK & WC
def truck_tare(
        truck_code: str = Query(title="Truck Code"),
        db: Session = Depends(get_db)
    ):
    return repo.truck_tare(truck_code, db)

@router.post('')
def weighing(
        request: WeighingReq,
        bt: BackgroundTasks,
        db: Session = Depends(get_db),
        cred = Depends(get_cred),
        token = Depends(get_token)
    ):
    return repo.weighing(request, db, cred, token, bt)

@router.post('/tares')
def save_tare(
        request: TareReq,
        bt: BackgroundTasks,
        db: Session = Depends(get_db),
    ):
    return repo.save_tare(request, db, bt)

@router.get('/companies')
def get_index_company(
        browse_queries: BrowseQueries = Depends(browse_query),
        db: Session = Depends(get_db),
    ):
    return repo.get_index_company(browse_queries, db)

@router.get('/locations')
def get_index_location(
        browse_queries: BrowseQueries = Depends(browse_query),
        db: Session = Depends(get_db),
    ):
    return repo.get_index_location(browse_queries, db)

@router.post('/sync')
def get_from_cloud(
        db: Session = Depends(get_db),
        token = Depends(get_token)
    ):
    return repo.get_from_cloud(db, token)

@router.post('/delivery-orders')
def get_do_from_cloud(
        db: Session = Depends(get_db),
        token = Depends(get_token)
    ):
    return {
        'do_close': repo.get_do_closed(db, token),
        'do_temp': repo.get_do_from_cloud(db, token)
    }

@router.get('/delivery-order-temps')
def browse(
        browse_queries: BrowseQueries = Depends(browse_query),
        db: Session = Depends(get_db),
    ):
    return repo.get_index_do_temp(browse_queries, db)

@router.get('/og-trip')
def og_trip(
        truck_code: str = None,
        db: Session = Depends(get_db),
    ):
    return repo.og_trip(truck_code, db)



# @router.get('/validate-rfid')
# def validate_rfid(
#         rfid_code: str = Query(title="RFID Code"),
#         wc_code: str = Query(title="Weighing Center Code"),
#         db: Session = Depends(get_db)
#     ):
#     return repo.validate_rfid(truck_code, wc_code, db)
