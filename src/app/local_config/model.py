from db.database import Base
from sqlalchemy import Column, DateTime, String, func
from sqlalchemy.sql.sqltypes import BigInteger, String

class LocalConfig(Base):
  __tablename__ = 'local_config'

  id = Column(BigInteger, primary_key=True, index=True)
  type = Column(String, nullable=True)
  loc_code = Column(String, nullable=True)
  wc_code = Column(String, nullable=True)
  wc_code_1 = Column(String, nullable=True)
  pit_code = Column(String, nullable=True)
  hauling = Column(String, nullable=True)
  owner_code = Column(String, nullable=True)
  destination = Column(String, nullable=True)
  prefix = Column(String, nullable=True)
  # created_at = Column(DateTime, default=func.now())
  # updated_at = Column(DateTime, default=func.now(), onupdate=func.now())
  # deleted_at = Column(DateTime, nullable=True)

  fillable = [
    'type',
    'loc_code',
    'wc_code',
    'wc_code_1',
    'pit_code',
    'hauling',
    'owner_code',
    'destination',
    'prefix'
  ]