from pydantic import BaseModel
from typing import List, Optional
import datetime

class Schema(BaseModel):
  # id: Optional[int|None] = None
  type: Optional[str] = None
  loc_code: Optional[str] = None
  wc_code: Optional[str] = None
  wc_code_1: Optional[str] = None
  pit_code: Optional[str] = None
  hauling: Optional[str] = None
  owner_code: Optional[str] = None
  destination: Optional[str] = None
  prefix: Optional[str] = None
