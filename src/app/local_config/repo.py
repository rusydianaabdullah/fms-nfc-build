from sqlalchemy.orm import Session

from sqlalchemy.exc import IntegrityError
from utils.exceptions import BadRequest400, NotFound404
from utils.std_repo import BrowseQueries, get_by_id

from .schema import Schema
from .model import LocalConfig as BaseModel

class LocalConfigRepo:
  def get_index(db: Session):
    return db.query(BaseModel).first()


  def store(request: Schema, db: Session, cred: str):
    try:
      if db.query(BaseModel).first():
        data = LocalConfigRepo.update(request, db, cred)
      else:
        data = LocalConfigRepo.create(request, db, cred)
      return data
    except Exception as e:
      db.rollback()
      raise BadRequest400(str(e))

  @staticmethod
  def create(request: Schema, db: Session, cred: str):
    data = BaseModel()
    for index in BaseModel.fillable:
      try: setattr(data, index, getattr(request, index))
      except: None
    try:
      db.add(data)
      db.commit()
      db.refresh(data)
      return data
    except Exception as e:
      db.rollback()
      raise BadRequest400(str(e))

  @staticmethod
  def update(request: Schema, db: Session, cred: str):
    try:
      updates = {}
      for index in BaseModel.fillable:
        if hasattr(request, index):
          updates.update({index: getattr(request, index)})
      data = db.query(BaseModel).update(updates)
      db.commit()
      data = db.query(BaseModel).first()
      return data
    except IntegrityError as e:
      db.rollback()
      raise BadRequest400(str(e))
