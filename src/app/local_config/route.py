from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session

from db.database import get_db
from app.auth.oauth2 import get_cred, check_permission

from .repo import LocalConfigRepo as repo
from .schema import Schema

path = 'local-config'
router = APIRouter(prefix=f'/v1/{path}', tags=[path], dependencies=[])
permission = False

# BROWSE
@router.get('') # response_model=Union[List[Display], Page[Display]]
def browse(db: Session = Depends(get_db)):
    return repo.get_index(db)

# STORE
@router.post('', response_model=Schema) #response_model=Display
def store(
        request: Schema,
        db: Session = Depends(get_db),
        cred = Depends(get_cred)
    ):
    if permission: check_permission(cred=cred, db=db, module=f'{path}-create')
    return repo.store(request, db, cred)