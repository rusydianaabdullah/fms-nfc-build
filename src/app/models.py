# import all table models
from app.auth.user.model import User
from app.auth.role.model import Role
from app.auth.permission.model import Permission
from app.auth.notification.model import Notification
