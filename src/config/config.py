from dotenv import load_dotenv, dotenv_values
load_dotenv()
config  = dotenv_values(".env")
####### STATIC VARS #######
statics = {
    'do': 'https://fms-api2.bmbcoal.com/v1/transaction/delivery-orders',
    'cctv': 'https://fms-api.bmbcoal.com/v1/auth/master-files',
    'tare': 'https://fms-api.bmbcoal.com/v1/master/truck-tares?wc-app',
    'error_log': 'https://fms-api2.bmbcoal.com/v1/transaction/nfc-logs/bulk',
}

try:
    mode = config['MODE']
except:
    mode = 'development'

if mode == 'development':
    statics = {
        # 'do': 'https://fms-api2.metamine.id/v1/transaction/delivery-orders',
        'do': 'http://localhost:8094/v1/transaction/delivery-orders',
        'cctv': 'https://fms-api.metamine.id/v1/auth/master-files',
        'tare': 'https://fms-api.metamine.id/v1/master/truck-tares?wc-app',
        'error_log': 'http://localhost:8094/v1/transaction/nfc-logs/bulk',
    }

def getenv(index: str, default: str = None):
    try:
        mode = config[index]
    except:
        mode = default
    return mode


def is_dev() -> bool:
    try:
        mode = config["MODE"]
    except:
        mode = 'development'

    if mode == "development":
        return True
    elif mode == "production":
        return False

    raise Exception('env MODE must be "development" or "production"')
