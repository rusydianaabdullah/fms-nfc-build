from app import models
from db.database import Base, engine

Base.metadata.create_all(engine)


def drop_all():
    Base.metadata.drop_all(engine)
