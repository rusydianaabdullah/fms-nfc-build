import os
import uvicorn
from fastapi import FastAPI, Request, Depends
from fastapi.encoders import jsonable_encoder
from fastapi.responses import JSONResponse
from fastapi.exceptions import RequestValidationError
from fastapi.middleware.cors import CORSMiddleware
from utils.exceptions import BadRequest400, Unauthorized401, Forbidden403, NotFound404
from config import getenv

from app.auth import authentication as auth
from app.auth.user import route_me as me
from app.auth.user import route as user
from app.auth.permission import route as permission
from app.auth.role import route as role
# from app.auth.master_file import route as master_file
# from app.auth.notification import route as notification

from app.nfc import route as nfc
from app.cctv import route as cctv
from app.truck import route as truck
from app.local_config import route as local_config
from app.weighing import route as weighing
from app.dashboard import route as dashboard

# ROUTER
app = FastAPI()
app.include_router(nfc.router)
app.include_router(cctv.router)
app.include_router(truck.router)
app.include_router(local_config.router)
app.include_router(weighing.router)

app.include_router(dashboard.router)

app.include_router(auth.router)
app.include_router(me.router)
app.include_router(user.router)
app.include_router(permission.router)
app.include_router(role.router)
# app.include_router(master_file.router)
# app.include_router(notification.router)


@app.get("/")
def read_root():
    return {"Hello": "World"}

@app.get("/v1/ping")
def read_root():
    return {"Ping": "Version 1"}


# EXCEPTION
@app.exception_handler(BadRequest400)
def bad_request(request: Request, exc: BadRequest400):
    return JSONResponse(status_code=400, content={'message': exc.name})

@app.exception_handler(Unauthorized401)
def bad_request(request: Request, exc: Forbidden403):
    return JSONResponse(status_code=401, content={'message': exc.name})

@app.exception_handler(Forbidden403)
def bad_request(request: Request, exc: Forbidden403):
    return JSONResponse(status_code=403, content={'message': exc.name})

@app.exception_handler(NotFound404)
def bad_request(request: Request, exc: NotFound404):
    return JSONResponse(status_code=404, content={'message': exc.name})

@app.exception_handler(RequestValidationError)
async def validation_exception_handler(request: Request, exc: RequestValidationError):
    return JSONResponse(
        status_code=422,
        content=jsonable_encoder({"detail": exc.errors(), "body": exc.body}),
    )


app.add_middleware(
    CORSMiddleware,
    allow_origins = ['*'],
    allow_credentials = True,
    allow_methods = ['*'],
    allow_headers = ['*'],
)

if __name__ == "__main__":
    host = getenv('HOST', '127.0.0.1')
    port = int(getenv('PORT', '8000'))
    mode = True if getenv('MODE', 'development') == 'development' else False

    uvicorn.run(
        app="main:app",
        host=host,
        port=port,
        reload=mode,
    )
