from .helper import number
import serial
from config import getenv
from utils.mqtt import send_mqtt

ser = None
SERIAL_PORT_PATH    = getenv('SERIAL_PORT_PATH', 'COM4')
BAUD_RATE           = int(getenv('BAUD_RATE', '2400'))
RANDOMWEIGH         = getenv('RANDOMWEIGH', '0')

def run_weighing(mqtt = True):
    try:
        ser = serial.Serial(
                            port=SERIAL_PORT_PATH,
                            baudrate=BAUD_RATE,
                            parity=serial.PARITY_NONE,
                            stopbits=serial.STOPBITS_ONE,
                            bytesize=serial.EIGHTBITS,
                            timeout=0.2)
        ser.write(b'hello')
        val = 0
        valstr = ''
        for x in range(3):
            bs      = ser.read(12)
            valstr += str(bs, 'UTF-8')
        ser.close()
        # isnegative = False
        valsplit = valstr.split('+')
        # if valsplit is None:
        #     valsplit = valstr.split('-')
        #     isnegative = True
        if valsplit[1]:
            nval = valsplit[1]
            val  = int(nval[1:6])
        if mqtt: send_mqtt(f'weighing:{val}')
        # print(val)
        return val
    except Exception as e:
        print(e)
        if RANDOMWEIGH == '1':
            rand = number.randrange(fr= 15000, to= 50000)
            send_mqtt(f'weighing:{rand}')
            return rand
        else:
            return 0
