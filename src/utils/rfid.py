from utils.helper import date
from utils.mqtt import send_mqtt
from sqlalchemy.orm import Session
from app.truck.model import Truck
from config import getenv
import os

def run_rfid(db: Session, mqtt = True):
    # Handle SKIP RFID
    if (getenv('SKIP_RFID', 0) == '1') : return 'SKIP'
    
    parent_folder = 'src/cctv'
    file_name = 'rfid.txt'

    file_path = os.path.join(parent_folder, file_name)

    if not os.path.exists(parent_folder): return None # Check Folder
    if not os.path.exists(file_path): return None  # Check file

    f = open(file=f'{parent_folder}/{file_name}', mode="r", encoding= 'utf8')
    readfile = f.read()

    currentmillis = date.timemillis()
    rfid = ''
    timemillis = 0
    try:
        splitf = readfile.split('|')
        rfid = splitf[0]
        timemillis = int(splitf[1])
    except: ''
    if currentmillis - timemillis > 30000: rfid = ''

    if rfid and len(rfid) > 7:
        try:
            rfid_truck = db.query(Truck.code).filter(Truck.rfid_code == rfid).first()
            if rfid_truck: rfid = rfid_truck.code
        except: ''
    if mqtt:
        if rfid != '':
            send_mqtt(f'rfid:{rfid}')
        else:
            send_mqtt('rfid:Tidak ada RFID yang terdeteksi')
    return rfid
