from typing import Any, List, Optional

from fastapi import HTTPException, Query
from pydantic import BaseModel
from sqlalchemy import func
from sqlalchemy.orm.decl_api import DeclarativeMeta
from sqlalchemy.orm import Session
from sqlalchemy.sql import text
from fastapi_pagination import Params, paginate

from utils.exceptions import BadRequest400, NotFound404
from utils.helper import res

######### GET BY ID #########
def get_by_id(id: int, db: Session, model: DeclarativeMeta):
    data = db.query(model)
    data = data.filter(model.id == id)
    data = data.filter(model.deleted_at == None)
    data = data.first()
    if not data:
        raise NotFound404(None, id)
    return data


######### DELETE & RESTORE #########
class BulkId(BaseModel):
    id: Optional[List[int]] = []

def soft_delete(request: BulkId, id: int, db: Session, model: DeclarativeMeta, cred: str, type: str = 'delete'):
    if id:
        try:
            if type == 'delete':
                db.query(model).filter(model.id == id).update({model.deleted_at: func.now()})
            elif type == 'restore':
                db.query(model).filter(model.id == id).update({model.deleted_at: None})
            db.commit()
            return res.success(data=id)
        except Exception as e:
            db.rollback()
            raise BadRequest400(str(e))
    elif len(request.id) > 0:
        ids = request.id
        try:
            if type == 'delete':
                db.query(model).filter(model.id.in_(ids)).update({model.deleted_at: func.now()})
            elif type == 'restore':
                db.query(model).filter(model.id.in_(ids)).update({model.deleted_at: None})
            db.commit()
            return res.success(data=ids)
        except Exception as e:
            db.rollback()
            raise BadRequest400(str(e))
    else:
        raise BadRequest400('No Data Delete')


######### BROWSE #########
class BrowseQueries(BaseModel):
    s: Optional[str] = None
    where: Optional[List[str]] = []
    like: Optional[List[str]] = []
    between: Optional[List[str]] = []
    in_: Optional[List[str]] = []
    except_: Optional[List[str]] = []
    isnull: Optional[List[str]] = []
    is_not_null: Optional[List[str]] = []
    table: bool = False
    limit: int = 10
    page: int = (1,)
    trash: bool = False
    all: bool = False
    count: bool = False
    order: Optional[str] = 'id desc'

def browse_query(
    s: Optional[str] = Query(None, title="Quick Search"),
    where: Optional[List[str]] = Query(None, title = 'Exact = column:value'),
    like: Optional[List[str]] = Query(None, title = 'Like = column:value'),
    between: Optional[List[str]] = Query(None, title = 'Between = column:value1 to value2'),
    in_: Optional[List[str]] = Query(None, title = 'IN = column:value1,value2'),
    except_: Optional[List[str]] = Query(None, title = 'Except = column:value1'),
    isnull: Optional[List[str]] = Query(None, title = 'Is Null or NA = column'),
    is_not_null: Optional[List[str]] = Query(None, title = 'Is Not Null or NA = column'),
    table: bool = False,
    limit: int = 10,
    page: int = 1,
    trash: bool = False,
    all: bool = False,
    count: bool = False,
    order: Optional[str] = Query('id desc', title = 'column desc/asc')
):
    return BrowseQueries(
        s=s,
        where=where,
        like=like,
        between=between,
        in_=in_,
        except_=except_,
        isnull=isnull,
        is_not_null=is_not_null,
        table=table,
        limit=limit,
        page=page,
        trash=trash,
        all=all,
        order=order,
        count=count
    )


def browse(
    browse_queries: BrowseQueries,
    db: Session,
    model: DeclarativeMeta,
):
    data = db.query(model)
    return data_query(browse_queries=browse_queries, model=model, data=data)

def data_query(
    browse_queries: BrowseQueries,
    model: DeclarativeMeta,
    data: any
):
    def get_column(col) -> Any:
        try:
            return getattr(model, col)
        except AttributeError:
            raise BadRequest400(f"There is no column named '{col}'")

    # WHERE expected string -> column:value|column:value
    if browse_queries.where:
        try:
            wheres = browse_queries.where

            for where in wheres:
                column = where.split(":")[0]
                value = where.split(":")[1]

                data = data.filter(get_column(column) == value)

        except Exception as e:
            raise BadRequest400(str(e))

    # BETWEEN expected string -> column1:start,end|column2:start,end
    if browse_queries.between:
        try:
            items = browse_queries.between

            for item in items:
                column = item.split(":")[0]
                start, end = (
                    item.split(":")[1].split(" to ")[0],
                    item.split(":")[1].split(" to ")[1],
                )

                data = data.filter(get_column(column).between(start, end))

        except Exception as e:
            raise BadRequest400(str(e))

    # LIKE expected string -> column:value
    if browse_queries.like:
        try:
            likes = browse_queries.like

            for like in likes:
                column = like.split(":")[0]
                value = like.split(":")[1]

                data = data.filter(get_column(column).ilike(f"%{value}%"))
        except IndexError as e:
            raise BadRequest400("There is a mistake in 'Like' input")

    # IN expected string -> column:value
    if browse_queries.in_:
        try:
            items = browse_queries.in_

            for item in items:
                column = item.split(":")[0]
                value = item.split(":")[1]
                vals = value.split(",")

                data = data.filter(get_column(column).in_(vals))
        except Exception as e:
            raise BadRequest400(str(e))

    # Except expected string -> column:value|column:value
    if browse_queries.except_:
        try:
            items = browse_queries.except_

            for item in items:
                column = item.split(":")[0]
                value = item.split(":")[1]

                if value == 'none': value = None

                data = data.filter(get_column(column) != value)

        except Exception as e:
            raise BadRequest400(str(e))

    # IS Null -> column
    if browse_queries.isnull:
        try:
            items = browse_queries.isnull

            for item in items:
                column = item

                data = data.filter(get_column(column) == None)

        except Exception as e:
            raise BadRequest400(str(e))

    # IS Not Null -> column
    if browse_queries.is_not_null:
        try:
            items = browse_queries.is_not_null

            for item in items:
                column = item

                data = data.filter(get_column(column) != None)

        except Exception as e:
            raise BadRequest400(str(e))

    # TRASH, ALL, DEFAULT
    if browse_queries.trash:
        data = data.filter(get_column('deleted_at') != None)
    elif browse_queries.all:
        data = data
    else:
        data = data.filter(get_column('deleted_at') == None)

    # SORTING
    if browse_queries.order:
        sort = browse_queries.order.replace(':', ' ')
        data = data.order_by(text(sort))

    # PAGINATE?
    if browse_queries.count:
        return data.count()
    elif browse_queries.table:
        all = data.count()
        page = browse_queries.page
        size = all if browse_queries.limit == 0 else browse_queries.limit
        data = data.offset(size*(page-1)).limit(size)
        return {
            'total': all,
            'items': data.all()
        }
        # return paginate(
        #     data.all(),
        #     params=Params(page=page, size=size)
        # )
    elif browse_queries.limit:
        data = data.limit(browse_queries.limit)

    return data.all()
