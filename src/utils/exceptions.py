from typing import Union
# from fastapi import Request
from fastapi.encoders import jsonable_encoder
from fastapi.exceptions import RequestValidationError

class BadRequest400(Exception):
  def __init__(self, message: str = 'Bad Request!'):
    self.name = message
    # self.detail = str(Exception)

class Unauthorized401(Exception):
  def __init__(self, message: str = 'Unauthorize!'):
    self.name = message
    # self.detail = str(Exception)

class Forbidden403(Exception):
  def __init__(self, message: str = 'Forbidden'):
    self.name = message


class NotFound404(Exception):
  def __init__(self, message: str = None, id: Union[str, int, None] = None):
    if message:
      self.name = message
    elif id:
      self.name = f'Data with id {id} not found'
    else:
      self.name = 'Data Not Found'
