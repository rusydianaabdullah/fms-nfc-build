# import json
# import os
import time
from paho.mqtt import client as mqtt
from config import getenv
from utils.logs import ErrorLog

def send_mqtt(message):
    BROKER = getenv('BROKERMQTT')
    TOPIC = getenv('MQTT_SUBSCRIBE', 'subscribe')
    PORT = 1883
    timemillis = round(time.time() * 1000)
    client_id = f'pyfms-{timemillis}'

    try:
        client = mqtt.Client(client_id)
        client.connect(BROKER, PORT)
        client.loop_start()
        client.publish(TOPIC, message)
        # print(MQTT_MESSAGE)
        # status = result[0]
        # print(status)
        client.loop_stop()
    except Exception as e:
        msg = 'send_mqtt error: ' + str(e)
        ErrorLog.create(None, msg, 'mqtt', message)
        print(msg)

# sensor/lamp # sensor/gate1 # sensor/gate2
def open_gate():
    BROKER = getenv('BROKERMQTT')
    MQTTGATE2 = getenv('GATE2OPEN', '1')
    PORT = 1883
    timemillis = round(time.time() * 1000)
    client_id = f'pyfms-{timemillis}'
    try:
        client = mqtt.Client(client_id)
        client.connect(BROKER, PORT)
        client.loop_start()
        client.publish('sensor/gate1/open', 1)
        time.sleep(1)
        client.publish('sensor/gate1/open', 0)
        if MQTTGATE2 == '1':
            client.publish('sensor/gate2/open', 1)
            time.sleep(1)
            client.publish('sensor/gate2/open', 0)
        # client.publish('sensor/lamp/red', 0)
        # client.publish('sensor/lamp/green', 1)
        # time.sleep(10)
        # client.publish('sensor/lamp/green', 0)
        # client.publish('sensor/lamp/red', 1)
        client.loop_stop()
    except Exception as e:
        msg = 'mqtt open_gate error: ' + str(e)
        print(msg)

# sensor/lamp # sensor/gate1 # sensor/gate2
def close_gate():
    BROKER = getenv('BROKERMQTT')
    MQTTGATE2 = getenv('GATE2OPEN', '1')
    PORT = 1883
    timemillis = round(time.time() * 1000)
    client_id = f'pyfms-{timemillis}'
    try:
        client = mqtt.Client(client_id)
        client.connect(BROKER, PORT)
        client.loop_start()
        client.publish('sensor/gate1/close', 1)
        time.sleep(1)
        client.publish('sensor/gate1/close', 0)
        if MQTTGATE2 == '1':
            client.publish('sensor/gate2/close', 1)
            time.sleep(1)
            client.publish('sensor/gate2/close', 0)
        client.loop_stop()
    except Exception as e:
        msg = 'mqtt close_gate error: ' + str(e)
        print(msg)

# def lamp(color: str = 'green'):
#     broker = getenv('BROKERMQTT')
#     port = 1883
#     timemillis = round(time.time() * 1000)
#     client_id = f'pyfms-{timemillis}'
#     try:
#         client = mqtt.Client(client_id)
#         client.connect(broker, port)
#         client.loop_start()
#         client.publish(f'sensor/lamp/{color}', 1)
#         time.sleep(5)
#         client.publish(f'sensor/lamp/{color}', 0)
#         client.loop_stop()
#     except Exception as e:
#         print("error mqtt")
#         print(e)
