# from config import getenv
# import cv2
# import base64
# from utils.mqtt import send_mqtt

# URLCCTV = getenv('RTSP_URL')

# cctv = ""
# namaImg = ""

# def run_cctv(mqtt = True):
#     try:
#         cctv = None
#         # cap = cv2.VideoCapture(URLCCTV, cv2.CAP_FFMPEG)
#         cap = cv2.VideoCapture()
#         cap.setExceptionMode(True)
#         # cap.set(cv2.CAP_PROP_OPEN_TIMEOUT_MSEC, 10000)
#         cap.open(URLCCTV, cv2.CAP_FFMPEG)

#         try:
#             success, image = cap.read()
#             # print(success, image)
#             if success:
#                 retval, buffer = cv2.imencode('.jpg', image)
#                 jpg_as_text = base64.b64encode(buffer)
#                 cctv = jpg_as_text.decode('utf-8')
#                 if mqtt: send_mqtt(f'cctv:last-data')
#                 else: send_mqtt(f'cctv:Ok')
#             else:
#                 send_mqtt(f'cctv:error frame not found')
#         except cv2.error as e:
#             send_mqtt(f'cctv:error CV2 exception {str(e)}')
#         cap.release()
#         return cctv
#     except Exception as e:
#         send_mqtt(f'cctv:error stream not found')
#         # ErrorLog.create(None, str(e), 'cctv')
#         return None


# def run_cctv_old():
#     # global cctv
#     try:
#         return stream(URLCCTV)
#     except Exception as e:
#         print('cctv error', e)
#     # return run()

# def stream(sourceCam):
#     global namaImg
#     namaImg = "screenshots/capture.jpg"
#     # print(sourceCam)
#     # print(namaImg)
#     cap = cv2.VideoCapture(sourceCam, cv2.CAP_FFMPEG)

#     if(cap.isOpened()):
#         ret, frame = cap.read()
#         if ret == True:
#             buffer = cv2.imencode('.jpg', frame)
#             print('cctv', buffer)
#             cctv = base64.b64encode(buffer)
#             print('cctv', cctv)
#             # cctv = cctv.decode('utf-8')
#             # print('cctv2', cctv)
#             return cctv
#             # print("Capturing cam")
#             #cv2.moveWindow('Streaming', 0,0) #disable jika run di linux server
#             cv2.imwrite(namaImg,frame)
#             #cv2.imshow('Streaming', frame)  #disable jika run di linux server
#         else:
#             # print("release cam and start again")
#             cap.release()
#             cap = cv2.VideoCapture(sourceCam)
#     else:
#         print("error cam no open")
#         namaImg = "default.png"

#     cap.release()
#     cv2.destroyAllWindows()

# def run():
#     global cctv
#     try:
#         with open(namaImg, "rb") as img_file:
#             cctv = base64.b64encode(img_file.read())
#         cctv = cctv.decode('utf-8')
#         return cctv
#     except Exception as E:
#         return 'error: {}'.format(str(E))
