import time
import random
from datetime import datetime, timedelta
from app.auth import oauth2

class helper:
  def gen_token():
    expires_delta = timedelta(minutes=30)
    return oauth2.create_access_token(data = {'sub': 'system'}, expires_delta=expires_delta)

class res():
  def success(data: dict, message: str = 'success'):
    return {'message': message, 'data': data}

class number():
  def randrange(fr: int = 1, to: int = 100):
    return random.randrange(fr, to, 3)

class date():
  def today(format = "%Y-%m-%d"):
    x = datetime.now()
    return x.strftime(format)

  def code_date_y(days = 0):
    x = datetime.now()
    if days > 0:
      yesterday = x - timedelta(days = days)
      return yesterday.strftime("%y%m")
    else:
      return x.strftime("%y%m")

  def code_date():
    x = datetime.now()
    hour = int(x.strftime("%H"))
    if (hour < 7):
      yesterday = x - timedelta(days = 1)
      return yesterday.strftime("%y%m%d")
    else:
      return x.strftime("%y%m%d")

  def shift_date():
    x = datetime.now()
    hour = int(x.strftime("%H"))
    if (hour < 7):
      yesterday = x - timedelta(days = 1)
      return {
        'year': yesterday.strftime("%Y"),
        'date': yesterday.strftime("%Y-%m-%d"),
        'shift': '2'
      }
    elif (hour >= 19):
      return {
        'year': x.strftime("%Y"),
        'date': x.strftime("%Y-%m-%d"),
        'shift': '2'
      }
    else:
      return {
        'year': x.strftime("%Y"),
        'date': x.strftime("%Y-%m-%d"),
        'shift': '1'
      }

  def timemillis():
    return int(round(time.time() * 1000))

  def timemillis_from_date(date, timezone=7):
    return int(round(datetime.timestamp(date) * 1000)) + (timezone * 60 * 60000)

  def timedelta(expiry):
    return timedelta(minutes=expiry)

  def utcnow():
    return datetime.utcnow()

  def encodeminute(year='24'):
    date_ = datetime.strptime(f'{year}0101 00:00:00', "%y%m%d %H:%M:%S")
    base = date.timemillis_from_date(date=date_, timezone=8)
    now = date.timemillis()
    millis = now - base
    minute = millis/60000
    return round(minute)

  def decodeminute(year='24', time='0'):
    date_ = datetime.strptime(f'{year}0101 00:00:00', "%y%m%d %H:%M:%S")
    base = date.timemillis_from_date(date=date_, timezone=8)
    minute = round(int(time)*60000)
    millis = base + minute
    return round(millis)
  
  def date_diff_in_days(timestamp1, timestamp2):
    date1 = datetime.fromtimestamp(timestamp1 / 1000)
    date2 = datetime.fromtimestamp(timestamp2 / 1000)
    
    return (date2 - date1).days