
def _run():
    from db.database import Session
    from app.nfc.repo import NfcRepo as repo

    db = Session()
    try:
        token = gen_token()
        data = repo.do_post(db, token, 10)
        print(data)
    except Exception as e:
        print(str(e))
    finally:
        db.close()

def run():
    import asyncio
    from db.database import Session
    from app.nfc.repo import NfcRepo as repo

    db = Session()
    try:
        token = gen_token()
        loop = asyncio.get_event_loop()
        data = loop.run_until_complete(repo.do_post(db, token, 10))  # Jalankan coroutine
        print(data)
    except Exception as e:
        print(str(e))
    finally:
        db.close()


def gen_token():
    from utils.helper import date
    from app.auth import oauth2

    expires_delta = date.timedelta(30)
    return oauth2.create_access_token(data = {'sub': 'system'}, expires_delta=expires_delta)
