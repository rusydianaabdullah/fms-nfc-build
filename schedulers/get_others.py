
def run():
    from db.database import Session
    # from fastapi import Depends
    from app.weighing.repo import WeighingRepo
    from app.truck.repo import TruckRepo
    from app.auth.user.repo import UserRepo

    # db = Depends(get_db)
    db = Session()
    try:
        token = gen_token()
        company_locs = WeighingRepo.get_from_cloud(db, token)
        print('company_locs', company_locs)
        truck = TruckRepo.get_from_cloud(db, token)
        print('truck', truck)
        user = UserRepo.get_fr_api(db, token)
        print('user', user)
    except Exception as e:
        print(str(e))
    finally:
        db.close()

def gen_token():
    from utils.helper import date
    from app.auth import oauth2

    expires_delta = date.timedelta(30)
    return oauth2.create_access_token(data = {'sub': 'superuser'}, expires_delta=expires_delta)
