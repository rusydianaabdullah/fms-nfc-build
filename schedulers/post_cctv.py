
def run():
    from db.database import Session
    # from fastapi import Depends
    from app.cctv.repo import CctvRepo as repo

    # db = Depends(get_db)
    db = Session()
    try:
        token = gen_token()
        data = repo.cctv_post(db, token, 10)
        print(data)
    except Exception as e:
        print(str(e))
        pass
    finally:
        db.close()

def gen_token():
    from utils.helper import date
    from app.auth import oauth2

    expires_delta = date.timedelta(30)
    return oauth2.create_access_token(data = {'sub': 'system'}, expires_delta=expires_delta)
