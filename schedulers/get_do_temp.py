
def run():
    from db.database import Session
    from app.weighing.repo import WeighingRepo as repo

    # db = Depends(get_db)
    db = Session()
    try:
        token = gen_token()
        data = repo.get_do_from_cloud(db, token)
        print(data)
        close_do = repo.get_do_closed(db, token)
        print(close_do)
    except Exception as e:
        print(str(e))
    finally:
        db.close()

def gen_token():
    from utils.helper import date
    from app.auth import oauth2

    expires_delta = date.timedelta(30)
    return oauth2.create_access_token(data = {'sub': 'superuser'}, expires_delta=expires_delta)
