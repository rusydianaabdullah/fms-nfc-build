
def run():
    from db.database import Session
    from app.error_log.repo import ErrorLogRepo as repo

    db = Session()
    try:
        token = gen_token()
        data = repo.post_to_server(db, token, 5)
        print(data)
    except Exception as e:
        print(str(e))
    finally:
        db.close()

def gen_token():
    from utils.helper import date
    from app.auth import oauth2

    expires_delta = date.timedelta(30)
    return oauth2.create_access_token(data = {'sub': 'system'}, expires_delta=expires_delta)
