import websocket
import _thread
import time
import rel
import math
import sys
from typing import Literal, LiteralString
import os

def gen_token():
    from utils.helper import date
    from app.auth import oauth2

    expires_delta = date.timedelta(30)
    return oauth2.create_access_token(data = {'sub': 'system'}, expires_delta=expires_delta)

def on_message(ws, message):    
    print('ws-message:', message)
    match message:
        case "sync-truck":
            from db.database import Session
            from app.auth.oauth2 import get_cred, get_token as cred
            from app.truck.repo import TruckRepo

            db = Session() 
            cred = gen_token()
            res = TruckRepo.get_from_cloud(db, cred)
            print('ws-res:', res)
        case "sync-user":
            from db.database import Session
            from app.auth.oauth2 import get_cred, get_token as cred
            from app.auth.user.repo import UserRepo

            db = Session() 
            cred = gen_token()
            res = UserRepo.get_fr_api(db, cred)
            print('ws-res:', res)
        case "update-app":
            os.system(f'{os.path.abspath(os.path.join(os.path.dirname(__file__)))}\\..\\"- update.bat"')

def on_error(ws, error):
    print(error)

def on_close(ws, close_status_code, close_msg):
    print("### closed ###")

def on_open(ws):
    print("Opened connection")

def gen_access_token() -> LiteralString | Literal['']:
  result = ''
  timemillis = int(round(time.time() * 1000))
  day = 60000*24 # valid for one day
  num = round(timemillis/day)*7777777
  characters = 'Aa0Bb1Cc2Dd3Ee4Ff5Gg6Hh7Ii8Jj9KkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz'
  charactersLength = 62
  lst = [int(i) for i in str(num)]
  # arrlen = len(lst)
  i = 0
  for el in lst:
    iter = (el+i)*num
    key = int(str(iter)[-2:])
    to = math.floor(key/99 * charactersLength)
    fr = to - 1
    result += characters[fr:to]
    i += 1
  return result

def currentmillis():
  """Current timemillis."""
  return int(round(time.time() * 1000))

def run():
    # var
    # host = 'ws://127.0.0.1:3000'
    host = 'wss://service.metamine.id'
    prefix = 'ptscpe-fms-nfc'
    client_id = currentmillis()
    accessToken = gen_access_token()
    # websocket
    websocket.enableTrace(True)
    ws = websocket.WebSocketApp(f"{host}/ws/{prefix}/{client_id}?token={accessToken}",
                              on_open=on_open,
                              on_message=on_message,
                              on_error=on_error,
                              on_close=on_close)

    ws.run_forever(dispatcher=rel, reconnect=5)  # Set dispatcher to automatic reconnection, 5 second reconnect delay if connection closed unexpectedly
    rel.signal(2, rel.abort)  # Keyboard Interrupt
    rel.dispatch()

# if __name__ == "__main__":
#     run()