import os
import socket
import time
from datetime import datetime
from dotenv import load_dotenv, dotenv_values
load_dotenv()
config  = dotenv_values(".env")

# try:    RFID_CONNECTION = config['RFID_CONNECTION']
# except: RFID_CONNECTION = "2"

try:    RFID_IP = config['RFID_IP']
except: RFID_IP = None

try:    RFID_PORTTCP = int(config['RFID_PORTTCP'])
except: RFID_PORTTCP = 9090

EPC1 = "AA 02 10 00 02 01 01 71 AD"         # EPC antenna 1
EPC2 = "AA 02 10 00 02 02 01 7B AD"         # EPC antenna 2

cmdStop       = "AA 02 FF 00 00 A4 0F"
cmdInfoDevice = "AA 02 09 00 05 01 0B B8 02 00 1D EB"
parent_folder = 'src/cctv'

def read(iteration: int = 7):
  cmd(1, 0)
  iterate = ''
  try:
    for x in range(iteration):
      row: str = cmd(1,1)
      if row: iterate += row
  except:
    ''
  cmd(1, 0)
  return parsingdata(iterate)


def cmd(antena, stop):
  try:
    if (stop == 0):
      return send_cmd(cmdStop)
    else:
      if antena == 1: HEX_data = send_cmd(EPC1)
      else: HEX_data = send_cmd(EPC2)
      if HEX_data == 'error': return 'error'
      else: return HEX_data.hex().upper()
  except Exception as e:
    return 'error'


def send_cmd(cmd):
  try:
    message = bytes.fromhex(cmd)
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.settimeout(0.5)
    s.connect((RFID_IP, RFID_PORTTCP))
    s.sendall(message)
    data = s.recv(2560)
    s.close()
    return data
  except Exception as e:
    return 'error'


def parsingdata(header_check: str):
  value = ''
  ####### ITERATION BEGIN #######
  if header_check.find('EEEEEEEE') > 0:
    fr = header_check.find('EEEEEEEE') + 8
    to = header_check.find('FFFFFFFF')
    if to > fr:
      dataTag = header_check[fr:to]
      # print("EPC: " + dataTag)
      value = dataTag
  elif header_check.find('AA12000015000C') > 0:
    fr = header_check.find('AA12000015000C') + 14
    to = header_check.find('34000101')
    if to > fr:
      dataTag = header_check[fr:to]
      # print("EPC: " + dataTag)
      value = dataTag
  elif header_check.find('AA12000013000C') > 0:
    fr = header_check.find('AA12000013000C') + 14
    to = header_check.find('34000101')
    if to > fr:
      dataTag = header_check[fr:to]
      # print("EPC: " + dataTag)
      value = dataTag
  elif header_check.find('AA120000') > 0:
    fr = header_check.find('AA120000') + 14
    to = header_check.find('34000101')
    if to > fr:
      dataTag = header_check[fr:to]
      # print("EPC: " + dataTag)
      value = dataTag
    else:
      to = header_check.find('20000101')
      if to > fr:
        dataTag = header_check[fr:to]
        # print("EPC: " + dataTag)
        value = dataTag
  # else:
  #   value = parsingdataold(header_check)
  if len(value) > 30: return ''
  else: return value


def parsingdataold(header_check: str):
  value = ''
  parseLoop = True
  if header_check[0:69] == "6361743A2063616E2774206F70656E20272F776966692F776966695F6163636573735F706F696E74273A204E6F20737563682066696C65206F72206469726563746F72790A":
    header_check = header_check[69:]
  dataAfterRemoveHead = header_check
  if header_check[0:26] == "AA100000060404021000024F9D":      # header check dengan data   AA100000060404021000024F9DAA1200001C000CE280699500004001BE23992F3000010141074599527C00086DDC9C37
    if header_check == "AA100000060404021000024F9D":        # header check tanpa data
      value = ''
    else:
      dataAfterRemoveHead = header_check[26:]
  if header_check[0:16] == "AA021000010046F6":                    # header check dengan data   AA021000010046F6AA1200001C000CE2000020341301590610849B30000101510745995945000EFD898DE4AA1200001C000CE20000203413015306107BDC30000101690745995945000EFE7BCB31AA1200001C000CE32D77FCA12015060201271F30000101580745995945000F24BD10C1AA1200001C000CE2000020340F02812050F4A430000101620745995945000F25A5ECB3AA1200001C000CE32D77FCA12015060201154530000101530745995946000009792BF8AA120000180008ABCDEF01234567892000010173074599594600000A63A6B2AA1200001C000CE32D77FCA12015060201264F300001015507459959460000306BDD7F
    dataAfterRemoveHead = header_check[16:]
  # ITERATION FOR UNCOMPLETE DATA
  while parseLoop:
    parseLoop = False
    if dataAfterRemoveHead[0:69] == "6361743A2063616E2774206F70656E20272F776966692F776966695F6163636573735F706F696E74273A204E6F20737563682066696C65206F72206469726563746F72790A":
      dataAfterRemoveHead = dataAfterRemoveHead[69:]

    if dataAfterRemoveHead[0:12] == "AA1200001300":               # header tanpa (no tag) AA021000010046F6AA12000013000CE20000203413015306107BDC300001015812F3
      dataAfterRemoveHead = dataAfterRemoveHead[12:]
      lengthData = bytes.fromhex(dataAfterRemoveHead[0:2])
      lengthData = int.from_bytes(lengthData, byteorder='big', signed=False)
      lengthData = lengthData*2   #panjang karakter HEX
      dataAfterRemoveHead = dataAfterRemoveHead[2:]   #remove length data
      dataTag = dataAfterRemoveHead[0:lengthData]
      dataAfterRemoveHead = dataAfterRemoveHead[lengthData+14:]   #14 penutup data EPC
      value = dataTag

    if dataAfterRemoveHead[0:12] == "AA1200000F00":                #header tanpa (no tag) AA1200000F00081234567890ABCDEF200001017132E9AA1200000F00081234567890ABCDEF2000010173B2E6
      dataAfterRemoveHead = dataAfterRemoveHead[12:]
      lengthData = bytes.fromhex(dataAfterRemoveHead[0:2])
      lengthData = int.from_bytes(lengthData, byteorder='big', signed=False)
      lengthData = lengthData*2   #panjang karakter HEX
      dataAfterRemoveHead = dataAfterRemoveHead[2:]   #remove length data
      dataTag = dataAfterRemoveHead[0:lengthData]
      dataAfterRemoveHead = dataAfterRemoveHead[lengthData+14:]   #14 penutup data EPC
      value = dataTag
  return value

if RFID_IP:
  if not os.path.isdir(parent_folder): os.mkdir(parent_folder)
  if not os.path.isfile(f'{parent_folder}/rfid.txt'):
    f = open(f'{parent_folder}/rfid.txt', "w")
    f.write(f'|0|2024')
    f.close()
  while True:
    value = ''
    try:
      check = cmd(1, 0)
      if check == 'error': value = 'error connection'
      else: value = read(5)
    except Exception as e:
      msg = str(e)
      value =  f'error {msg}'
    if value != '':
      x = datetime.now()
      now = x.strftime("%Y-%m-%d %H:%M:%S")
      currentmillis = int(round(time.time() * 1000))
      f = open(f'{parent_folder}/rfid.txt', "w")
      f.write(f'{value}|{currentmillis}|{now}')
      f.close()
      print('rfid generated: ', value, now)
    else: print('rfid not detected: ', value)
    time.sleep(2)
