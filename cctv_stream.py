import cv2
from dotenv import load_dotenv, dotenv_values
load_dotenv()
config  = dotenv_values(".env")

try:
  rtsp_url = config['RTSP_URL']
except:
  rtsp_url = ""

if rtsp_url:
  cap = cv2.VideoCapture(rtsp_url)
  # output_url = "http://0.0.0.0:9000/manifest(format=m3u8-cmaf)"
  # width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
  # height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
  # fps = int(cap.get(cv2.CAP_PROP_FPS))
  # output_stream = cv2.VideoWriter(output_url,  cv2.CAP_FFMPEG,  0,  fps,  (width,  height))

  while True:
    ret,frame = cap.read()
    if  not  ret:
      break
    # output_stream.write(frame)
    cv2.namedWindow(rtsp_url, cv2.WINDOW_KEEPRATIO)
    cv2.imshow(rtsp_url, frame)
    k = cv2.waitKey(1)
    if k == ord('q'):
      break
    if cv2.getWindowProperty(rtsp_url, cv2.WND_PROP_VISIBLE) <1:
      break

  # output_stream.release()
  cap.release()
  cv2.destroyAllWindows()
